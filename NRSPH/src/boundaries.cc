#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

namespace NRSPH {
using namespace std;

extern "C" void NRSPH_boundaries(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // Global domain
  CCTK_REAL physical_min[3];
  CCTK_REAL physical_max[3];
  CCTK_REAL interior_min[3];
  CCTK_REAL interior_max[3];
  CCTK_REAL exterior_min[3];
  CCTK_REAL exterior_max[3];
  CCTK_REAL spacing[3];
  int ierr =
      GetDomainSpecification(3, physical_min, physical_max, interior_min,
                             interior_max, exterior_min, exterior_max, spacing);
  if (ierr < 0)
    CCTK_ERROR("internal error");
  const CCTK_REAL xmin = physical_min[0];
  const CCTK_REAL ymin = physical_min[1];
  const CCTK_REAL zmin = physical_min[2];
  const CCTK_REAL xmax = physical_max[0];
  const CCTK_REAL ymax = physical_max[1];
  const CCTK_REAL zmax = physical_max[2];

  // Remove ghost particles
  *nparticles_ghosted = *nparticles;

  auto copy_particle = [&](ptrdiff_t i) {
    ptrdiff_t j;
    //#pragma omp atomic capture
    {
      j = *nparticles_ghosted;
      ++*nparticles_ghosted;
    }
    if (j >= maxnparticles)
      //#pragma omp critical
      CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
    id[j] = id[i];
    mass[j] = mass[i];
    vol[j] = vol[i];
    posx[j] = posx[i];
    posy[j] = posy[i];
    posz[j] = posz[i];
    velx[j] = velx[i];
    vely[j] = vely[i];
    velz[j] = velz[i];
    uie[j] = uie[i];
    return j;
  };

  // Add ghost particles for reflective boundaries
  //#pragma omp parallel for
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    if (dimension >= 1) {
      if (posx[i] < xmin + hsml) {
        assert(posx[i] >= xmin - hsml);
        ptrdiff_t j = copy_particle(i);
        posx[j] = xmin - (posx[j] - xmin);
        velx[j] = -velx[j];
      }
      if (posx[i] > xmax - hsml) {
        assert(posx[i] <= xmax + hsml);
        ptrdiff_t j = copy_particle(i);
        posx[j] = xmax - (posx[j] - xmax);
        velx[j] = -velx[j];
      }
    }
    if (dimension >= 2) {
      if (posy[i] < ymin + hsml) {
        assert(posy[i] >= ymin - hsml);
        ptrdiff_t j = copy_particle(i);
        posy[j] = ymin - (posy[j] - ymin);
        vely[j] = -vely[j];
      }
      if (posy[i] > ymax - hsml) {
        assert(posy[i] <= ymax + hsml);
        ptrdiff_t j = copy_particle(i);
        posy[j] = ymax - (posy[j] - ymax);
        vely[j] = -vely[j];
      }
    }
    if (dimension >= 3) {
      if (posz[i] < zmin + hsml) {
        assert(posz[i] >= zmin - hsml);
        ptrdiff_t j = copy_particle(i);
        posz[j] = zmin - (posz[j] - zmin);
        velz[j] = -velz[j];
      }
      if (posz[i] > zmax - hsml) {
        assert(posz[i] <= zmax + hsml);
        ptrdiff_t j = copy_particle(i);
        posz[j] = zmax - (posz[j] - zmax);
        velz[j] = -velz[j];
      }
    }
  }
}
}
