#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>
#include <cstddef>

#include "interact.hh"
#include "kernel.hh"

namespace NRSPH {
using namespace std;

// pressure
inline CCTK_REAL eos_press(CCTK_REAL rho, CCTK_REAL uie) {
  DECLARE_CCTK_PARAMETERS;
  return (eos_gamma - 1.0) * rho * uie;
}

// sound speed
inline CCTK_REAL eos_cs(CCTK_REAL rho, CCTK_REAL uie) {
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL press = eos_press(rho, uie);
  return sqrt(eos_gamma * press / rho);
}

extern "C" void NRSPH_rhs(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

// Calculate pressure
#pragma omp parallel for simd
  for (ptrdiff_t i = 0; i < *nparticles_ghosted; ++i) {
    // p = (gamma-1) * u * rho
    CCTK_REAL rho_i = mass[i] / vol[i];
    press[i] = eos_press(rho_i, uie[i]);
  }

// Calculate RHS
#pragma omp parallel for schedule(dynamic, 1000)
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {

    volrhs[i] = 0.0;

    // d/dt x_i = v_i
    posxrhs[i] = velx[i];
    posyrhs[i] = vely[i];
    poszrhs[i] = velz[i];

    velxrhs[i] = 0.0;
    velyrhs[i] = 0.0;
    velzrhs[i] = 0.0;

    uierhs[i] = 0.0;

    const auto &restrict neighbours = interactions.particles_neighbours[i];
    const auto &restrict js = *neighbours.js;
    for (ptrdiff_t jind = neighbours.jindmin; jind < neighbours.jindmax;
         ++jind) {
      ptrdiff_t j = js[jind];

      // W_ij = W(x_i - x_j)
      // gradW_ij = (grad W)(x_i - x_j)

      CCTK_REAL xij = posx[i] - posx[j];
      CCTK_REAL yij = posy[i] - posy[j];
      CCTK_REAL zij = posz[i] - posz[j];
      // CCTK_REAL Wij = kernel(xij, yij, zij);
      CCTK_REAL gradWij_x, gradWij_y, gradWij_z;
      grad_kernel(xij, yij, zij, gradWij_x, gradWij_y, gradWij_z);

      // Geometric consistency

      // n_i = Sum_j W_ij
      // V_i = 1/n_i

      // d/dt n_i = Sum_j dW_ij/dx d(x_i - x_j)/dt
      //          = Sum_j (v_i - v_j) gradW_ij
      //          = - Sum_j (v_j - v_i) gradW_ij

      // d/dt V_i = -1/n_i^2 (d/dt n_i)
      //          = Sum_j V_i^2 (v_j - v_i) gradW_ij

      // V        = Sum_i V_i
      // d/dt V_i = Sum_i (d/dt V_i)
      //          = Sum_ij V_i^2 (v_j - v_i) gradW_ij
      //          != 0

      CCTK_REAL div_vji = (velx[j] - velx[i]) * gradWij_x +
                          (vely[j] - vely[i]) * gradWij_y +
                          (velz[j] - velz[i]) * gradWij_z;
      volrhs[i] += pow(vol[i], 2) * div_vji;

      // Artificial viscosity

      CCTK_REAL rho_i = mass[i] / vol[i];
      CCTK_REAL rho_j = mass[j] / vol[j];
      CCTK_REAL csij = 0.5 * (eos_cs(rho_i, uie[i]) + eos_cs(rho_j, uie[j]));

      CCTK_REAL dr2 = pow(xij, 2) + pow(yij, 2) + pow(zij, 2);
      CCTK_REAL dxdv = fmin(CCTK_REAL(0.0), xij * (velx[i] - velx[j]) +
                                                yij * (vely[i] - vely[j]) +
                                                zij * (velz[i] - velz[j]));

      // this term is similar to the divergence of the velocity, has
      // dimension of a velocity, and is scaled by the smoothing
      // length h (i.e. vanishes in the continuum limit)
      CCTK_REAL av_delta_vel = hsml * dxdv / (dr2 + pow(hsml, 2) * av_epsilon);

      CCTK_REAL av_press_ij =
          (-av_alpha * csij + av_beta * av_delta_vel) * av_delta_vel;

      // Euler equation (momentum conservation)

      // d/dt v = -1/rho grad p
      //        = -1/rho Int dV grad p

      // d/dt v_i = -V_i/m_i Sum_j V_j gradW_ij p_j
      //        0 = -V_i/m_i Sum_j V_j gradW_ij p_i
      // d/dt v_i = -1/m_i Sum_j V_i V_j (p_j + p_i) gradW_ij

      // P^a      = Sum_i m_i v_i^a
      // d/dt P^a = + Sum_i m_i (d/dt v_i^a)
      //          = - Sum_ij m_i/m_i V_i V_j (p_j + p_i) gradW_ij
      //          = 0

      CCTK_REAL vol_press_ij = vol[i] * vol[j] *
                               (press[j] + press[i] + av_press_ij) /
                               (mass[i] * mass[j]);
      velxrhs[i] -= mass[j] * vol_press_ij * gradWij_x;
      velyrhs[i] -= mass[j] * vol_press_ij * gradWij_y;
      velzrhs[i] -= mass[j] * vol_press_ij * gradWij_z;

      // Energy conservation

      // d/dt u = -p/rho div v
      //        = -1/rho div p v + 1/rho v grad p

      // d/dt u_i = -p_i/rho_i Sum_j V_j (v_j - v_i) gradW_ij
      //          = -p_i/m_i Sum_j V_i V_j (v_j - v_i) gradW_ij
      //          = -1/m_i Sum_j V_i V_j (p_j + p_i)/2 (v_j - v_i) gradW_ij

      // E      = Sum_i m_i/2 v_i^2 + Sum_i m_i u_i
      // d/dt E = Sum_i m_i v_i (d/dt v_i) + Sum_i m_i (d/dt u_i)
      //        = - Sum_ij m_i v_i / m_i V_i V_j (p_j + p_i) gradW_ij
      //          - Sum_ij m_i/m_i V_i V_j (p_j + p_i)/2 (v_j - v_i) gradW_ij
      //        = - Sum_ij V_i V_j (p_j + p_i) v_i gradW_ij
      //          - Sum_ij V_i V_j (p_j + p_i)/2 (v_j - v_i) gradW_ij
      //        = - Sum_ij V_i V_j (p_j + p_i)/2 (v_j + v_i) gradW_ij
      //        = 0

      CCTK_REAL vol_press_div_vji = 0.5 * vol_press_ij * div_vji;
      uierhs[i] -= mass[j] * vol_press_div_vji;
    }
  }
}
}
