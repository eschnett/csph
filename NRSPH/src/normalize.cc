#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>

namespace NRSPH {
using namespace std;

inline CCTK_REAL frem(CCTK_REAL x, CCTK_REAL y) {
  // r = x - n*y such that 0<=r<y (for y>0)
  CCTK_REAL n = floor(x / y);
  CCTK_REAL r = x - n * y;
  return r;
}

inline CCTK_REAL frem(CCTK_REAL x, CCTK_REAL y, CCTK_REAL offset) {
  return frem(x - offset, y) + offset;
}

extern "C" void NRSPH_normalize(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const auto &MoL_Intermediate_Step = *static_cast<const CCTK_INT *>(
      CCTK_VarDataPtr(cctkGH, 0, "MoL::MoL_Intermediate_Step"));
  if (cctk_iteration != 0 && MoL_Intermediate_Step != 0)
    return;

  // Global domain
  CCTK_REAL physical_min[3];
  CCTK_REAL physical_max[3];
  CCTK_REAL interior_min[3];
  CCTK_REAL interior_max[3];
  CCTK_REAL exterior_min[3];
  CCTK_REAL exterior_max[3];
  CCTK_REAL spacing[3];
  int ierr =
      GetDomainSpecification(3, physical_min, physical_max, interior_min,
                             interior_max, exterior_min, exterior_max, spacing);
  if (ierr < 0)
    CCTK_ERROR("internal error");
  const CCTK_REAL xmin = physical_min[0];
  const CCTK_REAL ymin = physical_min[1];
  const CCTK_REAL zmin = physical_min[2];
  const CCTK_REAL xmax = physical_max[0];
  const CCTK_REAL ymax = physical_max[1];
  const CCTK_REAL zmax = physical_max[2];

#if 0
// Normalize particle locations for periodic boundaries
// TODO: Enable this via an explicit parameter
#pragma omp parallel for simd
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    posx[i] = frem(posx[i], xmax - xmin, xmin);
    posy[i] = frem(posy[i], ymax - ymin, ymin);
    posz[i] = frem(posz[i], zmax - zmin, zmin);
  }
#endif

// Normalize particle locations for reflective boundaries
// TODO: Enable this via an explicit parameter
#pragma omp parallel for simd
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    if (dimension >= 1) {
      posx[i] = remainder(posx[i] - xmin, 2 * (xmax - xmin)) + xmin;
      if (posx[i] < xmin) {
        posx[i] = xmin - (posx[i] - xmin);
        velx[i] = -velx[i];
      }
      assert(posx[i] >= xmin && posx[i] <= xmax);
    }
    if (dimension >= 2) {
      posy[i] = remainder(posy[i] - ymin, 2 * (ymax - ymin)) + ymin;
      if (posy[i] < ymin) {
        posy[i] = ymin - (posy[i] - ymin);
        vely[i] = -vely[i];
      }
      assert(posy[i] >= ymin && posy[i] <= ymax);
    }
    if (dimension >= 3) {
      posz[i] = remainder(posz[i] - zmin, 2 * (zmax - zmin)) + zmin;
      if (posz[i] < zmin) {
        posz[i] = zmin - (posz[i] - zmin);
        velz[i] = -velz[i];
      }
      assert(posz[i] >= zmin && posz[i] <= zmax);
    }
  }
}
}
