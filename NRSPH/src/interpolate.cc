#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cmath>
#include <cstddef>

#include "kernel.hh"

namespace NRSPH {
using namespace std;

extern "C" void NRSPH_interpolate(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // Grid size
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];

  // Global domain
  const CCTK_REAL x0 = CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL y0 = CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL z0 = CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  // Local domain
  const CCTK_REAL x0loc = x0 + cctk_lbnd[0] * dx;
  const CCTK_REAL y0loc = y0 + cctk_lbnd[1] * dy;
  const CCTK_REAL z0loc = z0 + cctk_lbnd[2] * dz;

// Initialize grid quantities
#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) {
    for (ptrdiff_t gj = 0; gj < nj; ++gj) {
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
        ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
        grid_rho[gidx] = 0.0;
        grid_velx[gidx] = 0.0;
        grid_vely[gidx] = 0.0;
        grid_velz[gidx] = 0.0;
        grid_eps[gidx] = 0.0;
        grid_press[gidx] = 0.0;
      }
    }
  }

// Interpolate from particles to grid
#pragma omp parallel for
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    ptrdiff_t simin = max(
        ptrdiff_t(0), ptrdiff_t(lrint(floor((posx[i] - x0loc - hsml) / dx))));
    ptrdiff_t sjmin = max(
        ptrdiff_t(0), ptrdiff_t(lrint(floor((posy[i] - y0loc - hsml) / dy))));
    ptrdiff_t skmin = max(
        ptrdiff_t(0), ptrdiff_t(lrint(floor((posz[i] - z0loc - hsml) / dz))));
    ptrdiff_t simax =
        min(ni, ptrdiff_t(lrint(ceil((posx[i] - x0loc + hsml) / dx))) + 1);
    ptrdiff_t sjmax =
        min(nj, ptrdiff_t(lrint(ceil((posy[i] - y0loc + hsml) / dy))) + 1);
    ptrdiff_t skmax =
        min(nk, ptrdiff_t(lrint(ceil((posz[i] - z0loc + hsml) / dz))) + 1);
    for (ptrdiff_t sk = skmin; sk < skmax; ++sk) {
      for (ptrdiff_t sj = sjmin; sj < sjmax; ++sj) {
        for (ptrdiff_t si = simin; si < simax; ++si) {
          ptrdiff_t sidx = CCTK_GFINDEX3D(cctkGH, si, sj, sk);
          CCTK_REAL sx = x0loc + si * dx;
          CCTK_REAL sy = y0loc + sj * dy;
          CCTK_REAL sz = z0loc + sk * dz;

          CCTK_REAL xij = posx[i] - sx;
          CCTK_REAL yij = posy[i] - sy;
          CCTK_REAL zij = posz[i] - sz;
          CCTK_REAL Wij = kernel(xij, yij, zij);

          if (Wij != 0.0) {
#pragma omp atomic update
            grid_rho[sidx] += mass[i] * Wij;

#pragma omp atomic update
            grid_velx[sidx] += vol[i] * velx[i] * Wij;
#pragma omp atomic update
            grid_vely[sidx] += vol[i] * vely[i] * Wij;
#pragma omp atomic update
            grid_velz[sidx] += vol[i] * velz[i] * Wij;

#pragma omp atomic update
            grid_eps[sidx] += mass[i] * uie[i] * Wij;

#pragma omp atomic update
            grid_press[sidx] += vol[i] * press[i] * Wij;
          }
        }
      }
    }
  }
}
}
