#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cmath>
#include <cstddef>

#include "kernel.hh"

namespace NRSPH {
using namespace std;

extern "C" void NRSPH_distribute(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // Grid size
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];

  // Global domain
  const CCTK_REAL x0 = CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL y0 = CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL z0 = CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  // Local domain
  const CCTK_REAL x0loc = x0 + cctk_lbnd[0] * dx;
  const CCTK_REAL y0loc = y0 + cctk_lbnd[1] * dy;
  const CCTK_REAL z0loc = z0 + cctk_lbnd[2] * dz;

// Initialize grid quantities
#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) {
    for (ptrdiff_t gj = 0; gj < nj; ++gj) {
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
        ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
        grid_mass[gidx] = 0.0;
        grid_momx[gidx] = 0.0;
        grid_momy[gidx] = 0.0;
        grid_momz[gidx] = 0.0;
        grid_eint[gidx] = 0.0;
      }
    }
  }

  // Distribute particles onto grid
  if (ni < 2 || nj < 2 || nk < 2)
    CCTK_ERROR("Grid is too small for linear distribution");
#pragma omp parallel for
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    ptrdiff_t gi = lrint(floor((posx[i] - x0loc) / dx));
    ptrdiff_t gj = lrint(floor((posy[i] - y0loc) / dy));
    ptrdiff_t gk = lrint(floor((posz[i] - z0loc) / dz));
    if (CCTK_BUILTIN_EXPECT(gi < 0 || gi >= ni || gj < 0 || gj >= nj ||
                                gk < 0 || gk >= nk,
                            false)) {
#pragma omp critical
      {
        CCTK_VInfo(CCTK_THORNSTRING, "i=%td pos[%g,%g,%g] gi=%td gj=%td gk=%td",
                   i, double(posx[i]), double(posy[i]), double(posz[i]), gi, gj,
                   gk);
        CCTK_ERROR("Particle outside the grid; domain decomposition failed");
      }
    }
    // Shift position if too close to the upper boundary
    gi = min(ni - 2, gi);
    gj = min(nj - 2, gj);
    gk = min(nk - 2, gk);
    CCTK_REAL gx = x0loc + gi * dx;
    CCTK_REAL gy = y0loc + gj * dy;
    CCTK_REAL gz = z0loc + gk * dz;
    CCTK_REAL qx = (posx[i] - gx) / dx;
    CCTK_REAL qy = (posy[i] - gy) / dy;
    CCTK_REAL qz = (posz[i] - gz) / dz;

    // Linear interpolation
    const CCTK_REAL fi[2] = {1.0 - qx, qx};
    const CCTK_REAL fj[2] = {1.0 - qy, qy};
    const CCTK_REAL fk[2] = {1.0 - qz, qz};

    for (ptrdiff_t dk = 0; dk < 2; ++dk) {
      for (ptrdiff_t dj = 0; dj < 2; ++dj) {
        for (ptrdiff_t di = 0; di < 2; ++di) {
          ptrdiff_t sidx = CCTK_GFINDEX3D(cctkGH, gi + di, gj + dj, gk + dk);
          CCTK_REAL f = fi[di] * fj[dj] * fk[dj];

#pragma omp atomic update
          grid_mass[sidx] += f * mass[i];

#pragma omp atomic update
          grid_momx[sidx] += f * mass[i] * velx[i];
#pragma omp atomic update
          grid_momy[sidx] += f * mass[i] * vely[i];
#pragma omp atomic update
          grid_momz[sidx] += f * mass[i] * velz[i];

#pragma omp atomic update
          grid_eint[sidx] += f * mass[i] * uie[i];
        }
      }
    }
  }
}
}
