#ifndef INTERACT_H
#define INTERACT_H

#include <cstddef>
#include <vector>

namespace NRSPH {
using namespace std;

struct neighbours_t {
  // Store particle indices as 32-bit ints to save space
  // However, the interaction indices must be 64-bit ints (ptrdiff_t)
  vector<int> *js;
  ptrdiff_t jindmin, jindmax;
};

struct interactions_t {
  vector<neighbours_t> particles_neighbours; // [*nparticles]
  vector<vector<int> > threads_js;           // [omp_get_max_threads()]
};

extern interactions_t interactions;
}

#endif // #ifndef INTERACT_H
