#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cstddef>

namespace NRSPH {
using namespace std;

extern "C" void NRSPH_box(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (dimension != 3)
    CCTK_ERROR("Need dimension=3");

  const CCTK_REAL Mtot = box_mass;
  const CCTK_REAL xmin = box_xmin, ymin = box_ymin, zmin = box_zmin;
  const CCTK_REAL xmax = box_xmax, ymax = box_ymax, zmax = box_zmax;
  const CCTK_REAL Vtot = (xmax - xmin) * (ymax - ymin) * (zmax - zmin);
  const ptrdiff_t ni = box_ni, nj = box_nj, nk = box_nk;
  const ptrdiff_t np = ni * nj * nk;

  if (np > maxnparticles)
    CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
  *nparticles = np;

#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) {
    for (ptrdiff_t gj = 0; gj < nj; ++gj) {
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
        ptrdiff_t i = gi + ni * (gj + nj * gk);
        id[i] = i;
        mass[i] = Mtot / np;
        vol[i] = Vtot / np;
        posx[i] = xmin + (gi + 0.5) * (xmax - xmin) / ni;
        posy[i] = ymin + (gj + 0.5) * (ymax - ymin) / nj;
        posz[i] = zmin + (gk + 0.5) * (zmax - zmin) / nk;
        velx[i] = 0.0;
        vely[i] = 0.0;
        velz[i] = 0.0;
        uie[i] = 1.0;
      }
    }
  }
  CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
}
}
