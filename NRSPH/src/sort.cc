#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <vector>

namespace NRSPH {
using namespace std;

namespace {
template <typename T>
void reorder(T *restrict data, T *restrict tmpdata, ptrdiff_t ndata,
             const vector<int> &perm) {
#pragma omp for simd
  for (ptrdiff_t i = 0; i < ndata; ++i)
    tmpdata[i] = data[perm[i]];
#pragma omp for simd
  for (ptrdiff_t i = 0; i < ndata; ++i)
    data[i] = tmpdata[i];
}
}

extern "C" void NRSPH_sort(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const auto &MoL_Intermediate_Step = *static_cast<const CCTK_INT *>(
      CCTK_VarDataPtr(cctkGH, 0, "MoL::MoL_Intermediate_Step"));
  if (cctk_iteration != 0 && MoL_Intermediate_Step != 0)
    return;

  // Grid size
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];

  // Global domain
  const CCTK_REAL x0 = CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL y0 = CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL z0 = CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  // Local domain
  const CCTK_REAL x0loc = x0 + cctk_lbnd[0] * dx;
  const CCTK_REAL y0loc = y0 + cctk_lbnd[1] * dy;
  const CCTK_REAL z0loc = z0 + cctk_lbnd[2] * dz;

  // Calculate grid cell index
  vector<int> grididx(*nparticles);
#pragma omp parallel for
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    ptrdiff_t gi = lrint((posx[i] - x0loc) / dx);
    ptrdiff_t gj = lrint((posy[i] - y0loc) / dy);
    ptrdiff_t gk = lrint((posz[i] - z0loc) / dz);
    if (CCTK_BUILTIN_EXPECT(gi < 0 || gi >= ni || gj < 0 || gj >= nj ||
                                gk < 0 || gk >= nk,
                            false)) {
#pragma omp critical
      {
        CCTK_VInfo(CCTK_THORNSTRING, "i=%td pos[%g,%g,%g] gi=%td gj=%td gk=%td",
                   i, double(posx[i]), double(posy[i]), double(posz[i]), gi, gj,
                   gk);
        CCTK_ERROR("Particle outside the grid; domain decomposition failed");
      }
    }
    grididx[i] = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
  }

  vector<int> perm(*nparticles);
#pragma omp for simd
  for (ptrdiff_t i = 0; i < *nparticles; ++i)
    perm[i] = i;
  sort(perm.begin(), perm.end(), [&](ptrdiff_t permi, ptrdiff_t permj) -> bool {
    if (grididx[permi] == grididx[permj])
      return id[permi] < id[permj];
    return grididx[permi] < grididx[permj];
  });

  vector<CCTK_REAL> tmpdata1(*nparticles);
  CCTK_REAL *tmpdata = tmpdata1.data();
  reorder(id, tmpdata, *nparticles, perm);
  reorder(mass, tmpdata, *nparticles, perm);
  reorder(vol, tmpdata, *nparticles, perm);
  reorder(posx, tmpdata, *nparticles, perm);
  reorder(posy, tmpdata, *nparticles, perm);
  reorder(posz, tmpdata, *nparticles, perm);
  reorder(velx, tmpdata, *nparticles, perm);
  reorder(vely, tmpdata, *nparticles, perm);
  reorder(velz, tmpdata, *nparticles, perm);
  reorder(uie, tmpdata, *nparticles, perm);
}
}
