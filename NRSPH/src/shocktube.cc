#include "kernel.hh"

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cstddef>
#include <iostream>

namespace NRSPH {
using namespace std;

inline CCTK_REAL smooth_step(CCTK_REAL x) {
  if (x < 0.0) {
    if (x <= -1.0)
      return 0.0;
    if (x <= -0.5)
      return 2.0 / 3.0 *
             (1.0 + 4.0 * x + 6.0 * pow(x, 2) + 4.0 * pow(x, 3) + pow(x, 4));
    else
      return 1.0 / 6.0 * (3.0 + 8.0 * x - 16.0 * pow(x, 3) - 12.0 * pow(x, 4));
  } else {
    if (x >= 1.0)
      return 1.0;
    if (x <= 0.5)
      return 1.0 / 6.0 * (3.0 + 8.0 * x - 16.0 * pow(x, 3) + 12.0 * pow(x, 4));
    else
      return 1.0 / 3.0 * (1.0 + 8.0 * x - 12.0 * pow(x, 2) + 8.0 * pow(x, 3) -
                          2.0 * pow(x, 4));
  }
}

inline CCTK_REAL step(CCTK_REAL x, CCTK_REAL val_left, CCTK_REAL val_right) {
  DECLARE_CCTK_PARAMETERS;
  // return signbit(x) ? val_left : val_right;
  CCTK_REAL alpha = smooth_step(x / hsml);
  return (1.0 - alpha) * val_left + alpha * val_right;
}

extern "C" void NRSPH_shocktube(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (dimension != 1)
    CCTK_ERROR("Need dimension=1");

  const CCTK_REAL xmin = shocktube_xmin;
  const CCTK_REAL xmax = shocktube_xmax;
  const CCTK_REAL Vtot = xmax - xmin;
  const ptrdiff_t ni = shocktube_ni;
  const ptrdiff_t np = ni;

  if (np > maxnparticles)
    CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
  *nparticles = np;

#pragma omp parallel for simd
  for (ptrdiff_t gi = 0; gi < ni; ++gi) {
    ptrdiff_t i = gi;
    id[i] = i;
    posx[i] = xmin + (gi + 0.5) * (xmax - xmin) / ni;
    posy[i] = 0.0;
    posz[i] = 0.0;
    vol[i] = Vtot / np;
    mass[i] = step(posx[i], rho_left, rho_right) * vol[i];
    velx[i] = 0.0;
    vely[i] = 0.0;
    velz[i] = 0.0;
    uie[i] = step(posx[i], uie_left, uie_right);
  }
  CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
}
}
