#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

namespace NRSPH {

namespace {
void register_group(const char *evolved, const char *rhs) {
  int evolved_index = CCTK_GroupIndex(evolved);
  if (evolved_index < 0)
    CCTK_ERROR("Unknown group name");
  int rhs_index = CCTK_GroupIndex(rhs);
  if (rhs_index < 0)
    CCTK_ERROR("Unknown group name");
  int ierr = MoLRegisterEvolvedGroup(evolved_index, rhs_index);
  if (ierr)
    CCTK_ERROR("Could not register evolved group");
}
}

extern "C" void NRSPH_MoL(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  register_group("NRSPH::vol", "NRSPH::volrhs");
  register_group("NRSPH::pos", "NRSPH::posrhs");
  register_group("NRSPH::vel", "NRSPH::velrhs");
  register_group("NRSPH::uie", "NRSPH::uierhs");
}
}
