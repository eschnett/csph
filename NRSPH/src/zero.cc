#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

namespace NRSPH {
using namespace std;

extern "C" void NRSPH_zero(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

// Zero out ghost RHS
#pragma omp parallel for simd
  for (ptrdiff_t i = *nparticles; i < maxnparticles; ++i) {
    volrhs[i] = 0.0;

    posxrhs[i] = 0.0;
    posyrhs[i] = 0.0;
    poszrhs[i] = 0.0;

    velxrhs[i] = 0.0;
    velyrhs[i] = 0.0;
    velzrhs[i] = 0.0;

    uierhs[i] = 0.0;
  }

// Zero out inactive particles
#pragma omp parallel for simd
  for (ptrdiff_t i = *nparticles_ghosted; i < maxnparticles; ++i) {

    id[i] = -1.0;

    mass[i] = 0.0;

    vol[i] = 0.0;

    posx[i] = 0.0;
    posy[i] = 0.0;
    posz[i] = 0.0;

    velx[i] = 0.0;
    vely[i] = 0.0;
    velz[i] = 0.0;

    uie[i] = 0.0;

    press[i] = 0.0;

    volrhs[i] = 0.0;

    posxrhs[i] = 0.0;
    posyrhs[i] = 0.0;
    poszrhs[i] = 0.0;

    velxrhs[i] = 0.0;
    velyrhs[i] = 0.0;
    velzrhs[i] = 0.0;

    uierhs[i] = 0.0;
  }
}
}
