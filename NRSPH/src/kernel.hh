#ifndef KERNEL_HH
#define KERNEL_HH

#include <cctk.h>
#include <cctk_Parameters.h>

#include <cmath>

namespace NRSPH {
using namespace std;

// Conditions on the kernel:
//    symmetric:       W(x^i) = W(-x^i)
//    normalized:      Int W(x) = 1
//    positive:        W(x^i) >= 0
//    compact support: W(x^i) = 0 for |x|>=h
//    continuously differentiable everywhere

// Definition in Mathematica:
//    w[h_, r_] = (12/Pi) BSplineBasis[3, r/(2 h) + 1/2] / h^3

constexpr const int dim = 1;

template <int dim> struct kernel_normalization;
template <> struct kernel_normalization<1> {
  constexpr CCTK_REAL operator()() const { return 2.0; }
};
template <> struct kernel_normalization<2> {
  constexpr CCTK_REAL operator()() const { return 60.0 / (7.0 * M_PI); }
};
template <> struct kernel_normalization<3> {
  constexpr CCTK_REAL operator()() const { return 12.0 / M_PI; }
};

template <int dim>
inline CCTK_REAL kernel(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z) {
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL r2 = pow(x, 2) + pow(y, 2) + pow(z, 2);
  if (r2 >= pow(hsml, 2))
    return 0.0;
  CCTK_REAL r = sqrt(r2);
  CCTK_REAL q = r / hsml;
  CCTK_REAL w;
  if (q < 0.5)
    w = 8.0 / 12.0 * (1.0 - 6.0 * pow(q, 2) + 6.0 * pow(q, 3));
  else
    w = 16.0 / 12.0 * (1.0 - 3.0 * q + 3.0 * pow(q, 2) - pow(q, 3));
  w *= kernel_normalization<dim>()() / pow(hsml, dim);
  return w;
}

template <int dim>
inline void grad_kernel(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z,
                        CCTK_REAL &restrict wx, CCTK_REAL &restrict wy,
                        CCTK_REAL &restrict wz) {
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL r2 = pow(x, 2) + pow(y, 2) + pow(z, 2);
  if (r2 >= pow(hsml, 2)) {
    wx = wy = wz = 0.0;
    return;
  }
  CCTK_REAL r = sqrt(r2);
  CCTK_REAL q = r / hsml;
  CCTK_REAL wr;
  if (q < 0.5)
    wr = -4.0 * (2.0 * q - 3.0 * pow(q, 2));
  else
    wr = -4.0 * (1.0 - 2.0 * q + pow(q, 2));
  wr *= kernel_normalization<dim>()() / pow(hsml, dim + 1);
  wx = wr * x / r;
  wy = wr * y / r;
  wz = wr * z / r;
}

inline CCTK_REAL kernel(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z) {
  DECLARE_CCTK_PARAMETERS;
  switch (dimension) {
  case 1:
    return kernel<1>(x, y, z);
  case 2:
    return kernel<2>(x, y, z);
  case 3:
    return kernel<3>(x, y, z);
  default:
    CCTK_BUILTIN_TRAP();
  }
}

inline void grad_kernel(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z,
                        CCTK_REAL &restrict wx, CCTK_REAL &restrict wy,
                        CCTK_REAL &restrict wz) {
  DECLARE_CCTK_PARAMETERS;
  switch (dimension) {
  case 1:
    return grad_kernel<1>(x, y, z, wx, wy, wz);
  case 2:
    return grad_kernel<2>(x, y, z, wx, wy, wz);
  case 3:
    return grad_kernel<3>(x, y, z, wx, wy, wz);
  default:
    CCTK_BUILTIN_TRAP();
  }
}
}

#endif // #ifndef KERNEL_HH
