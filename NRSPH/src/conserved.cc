#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>
#include <cstddef>
#include <cstdio>

#include "kernel.hh"

namespace NRSPH {
using namespace std;

extern "C" void NRSPH_conserved(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_REAL Vtot = 0.0;
  CCTK_REAL Mtot = 0.0;
  CCTK_REAL CMx = 0.0, CMy = 0.0, CMz = 0.0;
  CCTK_REAL Ptotx = 0.0, Ptoty = 0.0, Ptotz = 0.0;
  CCTK_REAL Ltotx = 0.0, Ltoty = 0.0, Ltotz = 0.0;
  CCTK_REAL Ekin = 0.0, Eint = 0.0;

#pragma omp parallel for reduction(+ : Vtot, Mtot, CMx, CMy, CMz, Ptotx,       \
                                   Ptoty, Ptotz, Ltotx, Ltoty, Ltotz, Ekin,    \
                                   Eint)
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    Vtot += vol[i];
    Mtot += mass[i];
    CMx += mass[i] * posx[i];
    CMy += mass[i] * posy[i];
    CMz += mass[i] * posz[i];
    Ptotx += mass[i] * velx[i];
    Ptoty += mass[i] * vely[i];
    Ptotz += mass[i] * velz[i];
    // L = m r ∧ v
    Ltotx += mass[i] * (posy[i] * velz[i] - posz[i] * vely[i]);
    Ltoty += mass[i] * (posz[i] * velx[i] - posx[i] * velz[i]);
    Ltotz += mass[i] * (posx[i] * vely[i] - posy[i] * velx[i]);
    Ekin += 1.0 / 2.0 * mass[i] *
            (pow(velx[i], 2) + pow(vely[i], 2) + pow(velz[i], 2));
    Eint += mass[i] * uie[i];
  }

  CMx /= Mtot;
  CMy /= Mtot;
  CMz /= Mtot;
  Ltotx -= CMy * Ptotz - CMz * Ptoty;
  Ltoty -= CMz * Ptotx - CMx * Ptotz;
  Ltotz -= CMx * Ptoty - CMy * Ptotx;
  CCTK_REAL Etot = Ekin + Eint;

  if (verbose_every > 0 && cctk_iteration % verbose_every == 0) {
    static int last_iteration = -1;
    if (cctk_iteration > last_iteration) {
      last_iteration = cctk_iteration;
      CCTK_INFO("Conserved quantities:");
      printf("    particles:          %15.6g\n", double(*nparticles));
      printf("    volume:             %15.6g\n", double(Vtot));
      printf("    mass:               %15.6g\n", double(Mtot));
      printf("    centre of mass x:   %15.6g\n", double(CMx));
      printf("    centre of mass y:   %15.6g\n", double(CMy));
      printf("    centre of mass z:   %15.6g\n", double(CMz));
      printf("    momentum x:         %15.6g\n", double(Ptotx));
      printf("    momentum y:         %15.6g\n", double(Ptoty));
      printf("    momentum z:         %15.6g\n", double(Ptotz));
      printf("    angular momentum x: %15.6g\n", double(Ltotx));
      printf("    angular momentum y: %15.6g\n", double(Ltoty));
      printf("    angular momentum z: %15.6g\n", double(Ltotz));
      printf("    kinetic energy:     %15.6g\n", double(Ekin));
      printf("    internal energy:    %15.6g\n", double(Eint));
      printf("    total energy:       %15.6g\n", double(Etot));
    }
  }
}
}
