# Schedule definitions for thorn NRSPH

STORAGE: nparticles nparticles_ghosted
STORAGE: id mass
STORAGE: vol[2] pos[2] vel[2] uie[2]
STORAGE: press
STORAGE: volrhs posrhs velrhs uierhs

STORAGE: grid_rho grid_vel grid_eps grid_press
STORAGE: grid_mass grid_mom grid_eint



SCHEDULE NRSPH_MoL IN MoL_Register
{
  LANG: C
} "Register evolved groups"



if (CCTK_EQUALS(scenario, "box")) {
  SCHEDULE NRSPH_box AT initial
  {
    LANG: C
  } "Set up particles in a box formation"
} else if (CCTK_EQUALS(scenario, "shocktube")) {
  SCHEDULE NRSPH_shocktube AT initial
  {
    LANG: C
  } "Set up shock tube"
} else {
  CCTK_ERROR("internal error");
}  



SCHEDULE NRSPH_normalize IN MoL_PostStepModify
{
  LANG: C
} "Normalize particles"

SCHEDULE NRSPH_sort IN MoL_PostStepModify AFTER NRSPH_normalize
{
  LANG: C
} "Sort particles"



SCHEDULE NRSPH_boundaries IN MoL_PostStep
{
  LANG: C
} "Add boundary particles"

SCHEDULE NRSPH_interact IN MoL_PostStep AFTER NRSPH_boundaries
{
  LANG: C
} "Find interactions"

SCHEDULE NRSPH_rhs IN MoL_PostStep AFTER NRSPH_interact
{
  LANG: C
} "Calculate RHS"

SCHEDULE NRSPH_zero IN MoL_PostStep AFTER NRSPH_rhs
{
  LANG: C
} "Zero out unused particles to beautify Cactus output"



SCHEDULE NRSPH_conserved AT analysis
{
  LANG: C
} "Calculate conserved quantities"

SCHEDULE NRSPH_interpolate AT analysis
{
  LANG: C
} "Interpolate quantities to grid"

SCHEDULE NRSPH_distribute AT analysis
{
  LANG: C
} "Distribute conserved quantities onto grid"
