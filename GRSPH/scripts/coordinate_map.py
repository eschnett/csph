#!/bin/env python

import numpy as np
import matplotlib.pyplot as mpl
import sys

infilename = "star_profile.dat"

infile = open(infilename,"r")
indata = infile.readlines()
infile.close()

n = int(indata[-1].split()[0]) + 1

rho = np.zeros(n)
rad = np.zeros(n)
gmass = np.zeros(n)
bmass = np.zeros(n)
riso = np.zeros(n)
alp = np.zeros(n)
phi4 = np.zeros(n)
xi = np.zeros(n)

polyK = np.float(indata[0].split()[0])
polyG = np.float(indata[0].split()[1])

isurf = 0
for i in range(n):
    mystrings = (indata[i+2]).split()
    rad[i] = np.float(mystrings[1])
    rho[i] = np.float(mystrings[2])
    gmass[i] = np.float(mystrings[3])
    bmass[i] = np.float(mystrings[4])
    riso[i] = np.float(mystrings[5])
    alp[i] = np.float(mystrings[6])
    phi4[i] = np.float(mystrings[7])
    if isurf == 0 and rho[i] <= 1.0e-16:
        isurf = i

rho0 = bmass[isurf] / (4.0*np.pi/3.0) / rad[isurf]**3
print rho0

def xirhs(K,rho0,rhop,r0,xip):
    if xip < 1.0e-15:
        rhs = K * rho0/rhop * 10.0
    else:
        rhs = K * rho0/rhop * (r0/xip)**2 
    return rhs

def integrate(K,rho0,rho,rad,xi):
    for i in range(1,isurf+1):
        xi_temp1 = dr*xirhs(K,rho0,rho[i-1],rad[i-1],xi[i-1])
        xi_temp2 = dr*xirhs(K,rho0,(rho[i-1]+rho[i])*0.5,\
                            (rad[i-1]+rad[i])*0.5,\
                            xi[i-1]+0.5*xi_temp1)
        xi[i] = xi[i-1] + xi_temp2

    return xi

dr = rad[1]-rad[0]
xi[0] = 0

K1 = 1.0
xi = integrate(K1,rho0,rho,rad,xi)
xi1 = xi[isurf]


K2 = 0.01
xi = integrate(K2,rho0,rho,rad,xi)
xi2 = xi[isurf]

print xi1,xi2

dxidK = (xi2-xi1)/(K2-K1)
f = xi2 - rad[isurf]
K = K2 - f / dxidK

xiold = xi2
Kold = K2

it = 0
err = 1.0e-10
while it < 100 and np.abs(f)/rad[i-1] > err:
    xi = integrate(K,rho0,rho,rad,xi)
    f = xi[isurf] - rad[isurf]
    dxidK = (xi[isurf]-xiold)/(K-Kold)
    Kold = K
    xiold = xi[isurf]
    K = K - f / dxidK
    it += 1
    print it,K,xi[isurf],rad[isurf]

#mpl.xlim(0.0,0.002)
#mpl.ylim(0.0,0.002)
#mpl.plot(rad,xi)
#mpl.show()

outfilename = "star_profile_map.dat"
outfile = open(outfilename,"w")

# output
outline = "%18.9E %18.9E\n" % (polyK,polyG)
outfile.write(outline)

# output
outline = "# zone, radius, density, mgrav, mbary, isotropic radius, lapse, phi4, xi\n"
outfile.write(outline)
for i in range(n):
    outline = "%8d %18.9E %18.9E %18.9E %18.9E %18.9E %18.9E %18.9E %18.9E\n" % \
              (i,rad[i],rho[i],gmass[i],bmass[i],\
               riso[i],1.0,phi4[i],xi[i])
    outfile.write(outline)

outfile.close()


    

    



    
