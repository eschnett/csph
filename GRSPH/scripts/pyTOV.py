"""
Python TOV Solver
Copyright (c) 2016 Christian D. Ott & Stefan Ruehe
"""

import numpy as np
import sys

# global constants
ggrav = 1.0
clite = 1.0
msun = 1.0

# EOS parameters -- assume simple polytrope P = K rho^Gamma
polyK = 100 # K
polyG = 2.0 # Gamma
minrho = 1.0e-16
minpress = polyK * minrho**polyG

####################################################

def tov_RHS(r,data,neqns):
    rhs = np.zeros(neqns)
    mass = data[1]
    press = np.fmax(data[0],minpress)
    rho = (press/polyK)**(1.0/polyG)
    eps = press/(polyG-1.0)/rho
    u = rho * (1.0 + eps/clite**2)
    
    if(r < 1.0e-5 and mass < 1.0e-5*msun):
        rhs[:] = 0.0
    else:
        if (press > minpress):
            rhs[0] = -ggrav / r**2 * mass * rho
        else:
            rhs[0] = 0.0
        # gravitational mass
        rhs[1] = 4.0*np.pi*r**2 * rho
        # baryonic mass
        rhs[2] = 4.0*np.pi*r**2 * rho
        # isotropic radius
        rhs[3] = 0.0
        # newtonian potential
        if (press > minpress):
            rhs[4] = ggrav * mass / r**2
        else:
            rhs[4] = 0.0
            
    return rhs
        
def tov_RK4(old_data,r,dr,neqns):
    ktemp = np.zeros((neqns,4))

    # first step
    ktemp[:,0] = dr * tov_RHS(r,old_data,neqns)

    # second_step
    ktemp[:,1] = dr * \
        tov_RHS(r + 0.5*dr, old_data + 0.5*ktemp[:,0],neqns)

    # third_step
    ktemp[:,2] = dr * \
        tov_RHS(r + 0.5*dr, old_data + 0.5*ktemp[:,1],neqns)

    # fourth step
    ktemp[:,3] = dr * \
        tov_RHS(r + dr, old_data + ktemp[:,2],neqns)

    return old_data + \
        1.0/6.0 * (ktemp[:,0] + 2*ktemp[:,1] + 2*ktemp[:,2] +\
                   ktemp[:,3])

####################################################
def tov_integrate(nzones,rho_c,rmax):

    neqns = 5       # number of equations we integrate

    # set up grid
    rad = np.linspace(0.0,rmax,nzones)
    dr  = rad[1]-rad[0]

    # set up data arrays
    # tovint -- holds stuff we integrate for
    tovint = np.zeros( (nzones, neqns) )
    # 0 -- press
    # 1 -- mgrav
    # 2 -- mbary
    # 3 -- riso (isotropic radius)
    # 4 -- Newt potential

    # tovdata -- holds the data we want
    tovdata = np.zeros( (nzones, 7) )
    # 0 -- rho
    # 1 -- press
    # 2 -- eps
    # 3 -- mgrav
    # 4 -- mbary
    # 5 -- riso
    # 6 -- Newt potential
    
    # central values
    tovint[0,0] = polyK * rho_c ** polyG # central pressure
    tovdata[0,0] = rho_c
    tovdata[0,1] = tovint[0,0]
    tovdata[0,2] = tovint[0,0] / (polyG - 1.0) / rho_c

    # initialize surface info
    isurf = 0
    # loop counter
    i = 0

    while (isurf == 0 and i < nzones-1 ):
    
        tovint[i+1,:] = tov_RK4(tovint[i,:],rad[i],dr,neqns)

    
        # check if we have reached the surface
        if(tovint[i+1,0] <= minpress):
            tovint[i+1,0] = minpress
            if(isurf == 0):
                isurf = i+1
    
        # press and mass, iso radius
        tovdata[i+1,0] = ((tovint[i+1,0])/polyK)**(1.0/polyG)
        tovdata[i+1,1] = tovint[i+1,0]
        tovdata[i+1,2] = tovdata[i+1,1]/(tovdata[i+1,0])/ \
                         (polyG-1.0)
        tovdata[i+1,3] = tovint[i+1,1]
        tovdata[i+1,4] = tovint[i+1,2]
        tovdata[i+1,5] = tovint[i+1,3]
        tovdata[i+1,6] = tovint[i+1,4]
        
        print "%8d %15.6E %15.6E %15.6E" % \
            (i,rad[i+1],tovdata[i+1,0],tovdata[i+1,3])
    
        i += 1
    
    if(isurf == 0):
        print "Could not solve for (entire?) TOV!"
        print "Setting isurf to nzones-1"
        isurf = nzones-1

    # mass outside the star
    tovdata[isurf+1:,3] = tovdata[isurf,3]
    tovdata[isurf+1:,4] = tovdata[isurf,4]
        
    # potential outside the star:
    phishift = tovdata[isurf,6] - (-tovdata[isurf,4]/rad[isurf])
    tovdata[:,6] -= phishift
    tovdata[isurf+1:,6] = - tovdata[isurf,4]/rad[isurf+1:]
        
    return (rad,tovdata,isurf)


######################################
# main program

nzones = 10000
rho_c = 6.0e-4
rmax  = 50.0
(rad,tovdata,isurf) = tov_integrate(nzones,rho_c,rmax)

import matplotlib.pyplot as mpl
mpl.plot(rad,tovdata[:,6])
mpl.show()


outfilename = "star_profile.dat"
outfile = open(outfilename,"w")

# output
outline = "%18.9E %18.9E\n" % (polyK,polyG)
outfile.write(outline)

# output
outline = "# zone, radius, density, mgrav, mbary, isotropic radius, lapse, phi4\n"
outfile.write(outline)
for i in range(0,nzones):
    outline = "%8d %18.9E %18.9E %18.9E %18.9E %18.9E %18.9E %18.9E\n" % \
              (i,rad[i],tovdata[i,0],tovdata[i,3],tovdata[i,4],\
               tovdata[i,5],1.0,tovdata[i,6])
    outfile.write(outline)

outfile.close()

#import matplotlib.pyplot as mpl
#mpl.semilogy(rad,tovdata[:,0])
#mpl.show()


