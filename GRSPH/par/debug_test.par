ActiveThorns = "
    Boundary
    Carpet
    CarpetLib
    CarpetIOASCII
    CarpetIOBasic
    CarpetIOScalar
    CarpetReduce
    CartGrid3D
    CoordBase
    IOUtil
    MoL
    ADMBase
    GRSPH
    SymBase
    Time
"



# Grid setup

#Carpet::verbose = yes
#Carpet::veryverbose = yes

Carpet::domain_from_coordbase = yes
CartGrid3D::type = "coordbase"

CoordBase::domainsize = "minmax"
CoordBase::spacing = "numcells"

CoordBase::xmin = -2.0
CoordBase::ymin = -2.0
CoordBase::zmin = -2.0
CoordBase::xmax = +2.0
CoordBase::ymax = +2.0
CoordBase::zmax = +2.0

CoordBase::ncells_x = 40
CoordBase::ncells_y = 40
CoordBase::ncells_z = 40

CoordBase::boundary_size_x_lower = 2
CoordBase::boundary_size_y_lower = 2
CoordBase::boundary_size_z_lower = 2
CoordBase::boundary_size_x_upper = 2
CoordBase::boundary_size_y_upper = 2
CoordBase::boundary_size_z_upper = 2

driver::ghost_size_x = 2
driver::ghost_size_y = 2
driver::ghost_size_z = 2



# Particle setup

GRSPH::initial_data = sedovblast
GRSPH::eos_gamma = 1.66667
GRSPH::maxnparticles = 40000

GRSPH::hsml = 0.05
GRSPH::eta_smooth = 1.2
GRSPH::prec_smooth = 1.0e-3
GRSPH::neib_fac = 3.0
GRSPH::artificial_viscosity = "Price08"
GRSPH::varsmooth = yes


# Evolution

Time::dtfac = 0.01

cactus::terminate = "time"
cactus::cctk_final_time = 1.0

MoL::init_RHS_zero = no

MoL::ODE_Method = "RK2"
MoL::MoL_Intermediate_Steps = 2
MoL::MoL_Num_Scratch_Levels = 1


admbase::initial_lapse = "one"
admbase::initial_shift = "zero"
admbase::evolution_method = "static"
admbase::lapse_evolution_method = "static"
admbase::shift_evolution_method = "static"
admbase::lapse_timelevels = 1
admbase::shift_timelevels = 1
admbase::metric_timelevels = 1
admbase::initial_data = "Cartesian Minkowski"

# Output

IO::out_dir = $parfile

IOBasic::outInfo_every = 1
IOBasic::outInfo_reductions = "sum maximum"
IOBasic::outInfo_vars = "
    GRSPH::dens GRSPH::velx
"

IOScalar::one_file_per_group = yes
IOScalar::outScalar_every = 1
IOScalar::outScalar_vars = "
    GRSPH::id
    GRSPH::mass
    GRSPH::pos
    GRSPH::vel
    GRSPH::press
    GRSPH::grid_rho
    GRSPH::grid_vel
    GRSPH::grid_eps
    GRSPH::grid_mass
    GRSPH::grid_mom
    GRSPH::grid_eint
"

IOASCII::one_file_per_group = yes
IOASCII::out1D_every = 1
IOASCII::out1D_vars = "
    GRSPH::id
    GRSPH::mass
    GRSPH::pos
    GRSPH::vel
    GRSPH::rho
    GRSPH::eps
    GRSPH::press
    GRSPH::visc
    GRSPH::tau
    GRSPH::dens
    GRSPH::w
    GRSPH::smoothl
    GRSPH::dens0
    GRSPH::sxrhs
    GRSPH::syrhs
    GRSPH::szrhs
    GRSPH::taurhs
"

IOASCII::out2D_every = -1
IOASCII::out2D_vars = "
    GRSPH::grid_rho
    GRSPH::grid_vel
    GRSPH::grid_eps
    GRSPH::grid_mass
    GRSPH::grid_mom
    GRSPH::grid_eint
"

IOASCII::out3D_every = -1
IOASCII::out3D_vars = "
    GRSPH::grid_rho
    GRSPH::grid_vel
    GRSPH::grid_eps
    GRSPH::grid_mass
    GRSPH::grid_mom
    GRSPH::grid_eint
"
