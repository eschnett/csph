#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cmath>
#include <cstddef>

#include "kernel.hh"

namespace GRSPH {
using namespace std;

extern "C" void GRSPH_sources_grid(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // Grid size
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];

  // This is the routine in which we will eventually
  // compute the GR source terms to the right-hand sides
  // of the hydro equations. These will then be mapped
  // to the particles by another routine.

  // Right now, this routine simply computes the acceleration
  // term from the Newtonian potential using second-order
  // standard finite differences

  const CCTK_REAL half = 0.50;
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  const CCTK_REAL idx = 1.0/dx * half;
  const CCTK_REAL idy = 1.0/dy * half;
  const CCTK_REAL idz = 1.0/dz * half;

#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) {
    for (ptrdiff_t gj = 0; gj < nj; ++gj) {
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
        ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
	grid_NewtAccelx[gidx] = 0.0;
	grid_NewtAccely[gidx] = 0.0;
	grid_NewtAccelz[gidx] = 0.0;
      }
    }
  }

  // standard centered second-order FD
#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 1; gk < nk-1; ++gk) {
    for (ptrdiff_t gj = 1; gj < nj-1; ++gj) {
      for (ptrdiff_t gi = 1; gi < ni-1; ++gi) {

        ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
        ptrdiff_t gidx_xp = CCTK_GFINDEX3D(cctkGH, gi+1, gj, gk);
        ptrdiff_t gidx_xm = CCTK_GFINDEX3D(cctkGH, gi-1, gj, gk);
        ptrdiff_t gidx_yp = CCTK_GFINDEX3D(cctkGH, gi, gj+1, gk);
        ptrdiff_t gidx_ym = CCTK_GFINDEX3D(cctkGH, gi, gj-1, gk);
        ptrdiff_t gidx_zp = CCTK_GFINDEX3D(cctkGH, gi, gj, gk+1);
        ptrdiff_t gidx_zm = CCTK_GFINDEX3D(cctkGH, gi, gj, gk-1);
	
	grid_NewtAccelx[gidx] = 
	  -(grid_NewtPot[gidx_xp] - grid_NewtPot[gidx_xm]) * idx;

	grid_NewtAccely[gidx] = 
	  -(grid_NewtPot[gidx_yp] - grid_NewtPot[gidx_ym]) * idy;

	grid_NewtAccelz[gidx] = 
	  -(grid_NewtPot[gidx_zp] - grid_NewtPot[gidx_zm]) * idz;

      }
    }
  }





} // GRSPH_sources_grid

} // namespace GRSPH

