#ifndef EOS_HH
#define EOS_HH

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

namespace GRSPH {

static inline __attribute__((always_inline))
double GRSPH_press(const double rho, const double eps){

  DECLARE_CCTK_PARAMETERS;
  return (eos_gamma-1.0)*rho*eps;

}

static inline __attribute__((always_inline))
double GRSPH_dpdrho(const double rho, const double eps){

  DECLARE_CCTK_PARAMETERS;
  return (eos_gamma-1.0)*eps;

}

static inline __attribute__((always_inline))
double GRSPH_dpdeps(const double rho, const double eps){

  DECLARE_CCTK_PARAMETERS;
  return (eos_gamma-1.0)*rho;

}

static inline __attribute__((always_inline))
double GRSPH_cs2(const double rho, const double eps){

  // this is the speed of sound squared

  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL xpress  = (eos_gamma-1.0)*eps*rho;
  CCTK_REAL xdpdrho = (eos_gamma-1.0)*eps;
  CCTK_REAL xdpdeps = (eos_gamma-1.0)*rho;
  CCTK_REAL invrho = 1.0/rho;
  CCTK_REAL h = 1.0 + eps + xpress*invrho;
  CCTK_REAL cs2 = (xdpdrho 
		   + xpress * xdpdeps * invrho * invrho) / h;
  return cs2;

}



}
#endif // #ifndef EOS_HH 
