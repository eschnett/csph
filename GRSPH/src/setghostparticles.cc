#include <cctk.h>
#include <cctk_Parameters.h>
#include <cctk_Arguments.h>
#include <cmath>


namespace GRSPH {
  using namespace std;
                         
  extern "C" void GRSPH_setghostparticles(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    //Set ghost particles                                                                           
    *n_g_particles = *nparticles;

    auto copy_particle = [&](ptrdiff_t i) {
      ptrdiff_t j;
      //#pragma omp atomic capture                                                                                             
      {
	j = *n_g_particles;
	++*n_g_particles;
      }
      if (j >= maxnparticles)
	//#pragma omp critical                                                                                                 
	CCTK_ERROR("Too many particles; increase \"maxnparticles\"");

      posx[j] = posx[i];
      posy[j] = posy[i];
      posz[j] = posz[i];
      velx[j] = velx[i];
      vely[j] = vely[i];
      velz[j] = velz[i];
      
      id[j] = j;	  
      rho[j] = rho [i];
      dens[j] = dens [i];
      dens0[j] = dens0[i];
      cs2[j] = cs2[i];
      mass[j] = mass[i];
      eps[j] = eps[i];
      press[j] = press[i];
      smoothl[j] = smoothl[i];
      visca[j] = visca[i];
      
      alphap[j] = alphap[i];
      sdetgp[j] = sdetgp[i];
      gxxp[j] = gxxp[i];
      gxyp[j] = gxyp[i];
      gxzp[j] = gxzp[i];
      gyyp[j] = gyyp[i];
      gyzp[j] = gyzp[i];
      gzzp[j] = gzzp[i];
      betaxp[j] = betaxp[i];
      betayp[j] = betayp[i];
      betazp[j] = betazp[i];
      
      return j;
    };


    // Loop over all particles, find particles at boundary
    //#pragma omp parallel for 
    for (ptrdiff_t i = 0; i < *nparticles; ++i) {

      CCTK_REAL CopyWidth = 3.0 * hsml; 
      CCTK_REAL CopyFree  = hsml / 100.0; // Do not copy on boundary
      CCTK_REAL xmin = boundaryval[0]; bool xmin_bound = false;
      CCTK_REAL xmax = boundaryval[1]; bool xmax_bound = false;
      CCTK_REAL ymin = boundaryval[2]; bool ymin_bound = false;
      CCTK_REAL ymax = boundaryval[3]; bool ymax_bound = false;
      CCTK_REAL zmin = boundaryval[4]; 
      CCTK_REAL zmax = boundaryval[5]; 
	
      //xmin
      if (isfinite(xmin)
	  and posx[i] < xmin + CopyWidth
	  and posx[i] > xmin + CopyFree) { 
	assert(posx[i] >= xmin - CopyWidth);
        ptrdiff_t j = copy_particle(i);
        posx[j] = xmin - (posx[j] - xmin);
        velx[j] = -velx[j];
	xmin_bound = true;
      }

      //xmax
      if (isfinite(xmax)
	  and posx[i] > xmax - CopyWidth
	  and posx[i] < xmax - CopyFree) {
	assert(posx[i] <= xmax + CopyWidth);
        ptrdiff_t j = copy_particle(i);
        posx[j] = xmax - (posx[j] - xmax);
        velx[j] = -velx[j];
	xmax_bound = true;
      }

      //ymin
      if (isfinite(ymin)
	  and posy[i] < ymin + CopyWidth
	  and posy[i] > ymin + CopyFree) {
        assert(posy[i] >= ymin - CopyWidth);
        ptrdiff_t j = copy_particle(i);
        posy[j] = ymin - (posy[j] - ymin);
        vely[j] = -vely[j];
	ymin_bound = true;

	if (xmin_bound) {
	  ptrdiff_t k = copy_particle(i);
	  posx[k] = xmin - (posx[k] - xmin);
	  velx[k] = -velx[k];
	  posy[k] = ymin - (posy[k] - ymin);
	  vely[k] = -vely[k];
	}
	if (xmax_bound) {
	  ptrdiff_t l = copy_particle(i);
	  posx[l] = xmax - (posx[l] - xmax);
	  velx[l] = -velx[l];
	  posy[l] = ymin - (posy[l] - ymin);
	  vely[l] = -vely[l];
	}

      }

      //ymax
      if (isfinite(ymax)
	  and posy[i] > ymax - CopyWidth
	  and posy[i] < ymax - CopyFree) {
        assert(posy[i] <= ymax + CopyWidth);
        ptrdiff_t j = copy_particle(i);
        posy[j] = ymax - (posy[j] - ymax);
        vely[j] = -vely[j];
	ymax_bound = true;

	if (xmin_bound) {
	  ptrdiff_t k = copy_particle(i);
	  posx[k] = xmin - (posx[k] - xmin);
	  velx[k] = -velx[k];
	  posy[k] = ymax - (posy[k] - ymax);
	  vely[k] = -vely[k];
	}
	if (xmax_bound) {
	  ptrdiff_t l = copy_particle(i);
	  posx[l] = xmax - (posx[l] - xmax);
	  velx[l] = -velx[l];
	  posy[l] = ymax - (posy[l] - ymax);
	  vely[l] = -vely[l];
	}
       }

      //zmin
      if (isfinite(zmin)
	  and posz[i] < zmin + CopyWidth
	  and posz[i] > zmin + CopyFree) {
        assert(posz[i] >= zmin - CopyWidth);
        ptrdiff_t j = copy_particle(i);
        posz[j] = zmin - (posz[j] - zmin);
        velz[j] = -velz[j];


	if (xmin_bound) {
	  ptrdiff_t k = copy_particle(i);
	  posx[k] = xmin - (posx[k] - xmin);
	  velx[k] = -velx[k];
	  posz[k] = zmin - (posz[k] - zmin);
	  velz[k] = -velz[k];
	}
	if (xmax_bound) {
	  ptrdiff_t l = copy_particle(i);
	  posx[l] = xmax - (posx[l] - xmax);
	  velx[l] = -velx[l];
	  posz[l] = zmin - (posz[l] - zmin);
	  velz[l] = -velz[l];
	}
	if (ymin_bound) {
	  ptrdiff_t k = copy_particle(i);
	  posy[k] = ymin - (posy[k] - ymin);
	  vely[k] = -vely[k];
	  posz[k] = zmin - (posz[k] - zmin);
	  velz[k] = -velz[k];
	  if (xmin_bound) {
	    ptrdiff_t kk = copy_particle(i);
	    posx[kk] = xmin - (posx[kk] - xmin);
	    velx[kk] = -velx[kk];
	    posy[kk] = ymin - (posy[kk] - ymin);
	    vely[kk] = -vely[kk];
	    posz[kk] = zmin - (posz[kk] - zmin);
	    velz[kk] = -velz[kk];
	  }
	  if (xmax_bound) {
	    ptrdiff_t kl = copy_particle(i);
	    posx[kl] = xmax - (posx[kl] - xmax);
	    velx[kl] = -velx[kl];
	    posy[kl] = ymin - (posy[kl] - ymin);
	    vely[kl] = -vely[kl];
	    posz[kl] = zmin - (posz[kl] - zmin);
	    velz[kl] = -velz[kl];
	    }

	}
	if (ymax_bound) {
	  ptrdiff_t l = copy_particle(i);
	  posy[l] = ymax - (posy[l] - ymax);
	  vely[l] = -vely[l];
	  posz[l] = zmin - (posz[l] - zmin);
	  velz[l] = -velz[l];
	  if (xmin_bound) {
	    ptrdiff_t kk = copy_particle(i);
	    posx[kk] = xmin - (posx[kk] - xmin);
	    velx[kk] = -velx[kk];
	    posy[kk] = ymax - (posy[kk] - ymax);
	    vely[kk] = -vely[kk];
	    posz[kk] = zmin - (posz[kk] - zmin);
	    velz[kk] = -velz[kk];
	  }
	   if (xmax_bound) {
	    ptrdiff_t kl = copy_particle(i);
	    posx[kl] = xmax - (posx[kl] - xmax);
	    velx[kl] = -velx[kl];
	    posy[kl] = ymax - (posy[kl] - ymax);
	    vely[kl] = -vely[kl];
	    posz[kl] = zmin - (posz[kl] - zmin);
	    velz[kl] = -velz[kl];
	    }
	}
       }

      //zmax
      if (isfinite(zmax)
	  and posz[i] > zmax - CopyWidth
	  and posz[i] < zmax - CopyFree) {
        assert(posz[i] <= zmax + CopyWidth);
        ptrdiff_t j = copy_particle(i);
        posz[j] = zmax - (posz[j] - zmax);
        velz[j] = -velz[j];

	if (xmin_bound) {
	  ptrdiff_t k = copy_particle(i);
	  posx[k] = xmin - (posx[k] - xmin);
	  velx[k] = -velx[k];
	  posz[k] = zmax - (posz[k] - zmax);
	  velz[k] = -velz[k];
	}
	if (xmax_bound) {
	  ptrdiff_t l = copy_particle(i);
	  posx[l] = xmax - (posx[l] - xmax);
	  velx[l] = -velx[l];
	  posz[l] = zmax - (posz[l] - zmax);
	  velz[l] = -velz[l];
	}

	if (ymin_bound) {
	  ptrdiff_t k = copy_particle(i);
	  posy[k] = ymin - (posy[k] - ymin);
	  vely[k] = -vely[k];
	  posz[k] = zmax - (posz[k] - zmax);
	  velz[k] = -velz[k];
	  if (xmin_bound) {
	    ptrdiff_t kk = copy_particle(i);
	    posx[kk] = xmin - (posx[kk] - xmin);
	    velx[kk] = -velx[kk];
	    posy[kk] = ymin - (posy[kk] - ymin);
	    vely[kk] = -vely[kk];
	    posz[kk] = zmax - (posz[kk] - zmax);
	    velz[kk] = -velz[kk];
	  }
	  if (xmax_bound) {
	    ptrdiff_t kl = copy_particle(i);
	    posx[kl] = xmax - (posx[kl] - xmax);
	    velx[kl] = -velx[kl];
	    posy[kl] = ymin - (posy[kl] - ymin);
	    vely[kl] = -vely[kl];
	    posz[kl] = zmax - (posz[kl] - zmax);
	    velz[kl] = -velz[kl];
	    }
	}
	if (ymax_bound) {
	  ptrdiff_t l = copy_particle(i);
	  posy[l] = ymax - (posy[l] - ymax);
	  vely[l] = -vely[l];
	  posz[l] = zmax - (posz[l] - zmax);
	  velz[l] = -velz[l];
	  if (xmin_bound) {
	    ptrdiff_t kk = copy_particle(i);
	    posx[kk] = xmin - (posx[kk] - xmin);
	    velx[kk] = -velx[kk];
	    posy[kk] = ymax - (posy[kk] - ymax);
	    vely[kk] = -vely[kk];
	    posz[kk] = zmax - (posz[kk] - zmax);
	    velz[kk] = -velz[kk];
	  }
	  if (xmax_bound) {
	    ptrdiff_t kl = copy_particle(i);
	    posx[kl] = xmax - (posx[kl] - xmax);
	    velx[kl] = -velx[kl];
	    posy[kl] = ymax - (posy[kl] - ymax);
	    vely[kl] = -vely[kl];
	    posz[kl] = zmax - (posz[kl] - zmax);
	    velz[kl] = -velz[kl];
	  }
	}
      }
      
    } // end loop over particles
  } // end function
} //end namespace

