#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cstddef>
#include "eos.hh"
#include <math.h>

namespace GRSPH {
using namespace std;

  // function protype for shocktupe
  void GRSPH_setup_shocktube(CCTK_ARGUMENTS);
  void GRSPH_setup_shocktube_eqmass(CCTK_ARGUMENTS);
  void GRSPH_setup_shocktube_eqmassHR(CCTK_ARGUMENTS);
  void GRSPH_setup_shocktube_eqmass3D(CCTK_ARGUMENTS);
  void GRSPH_setup_kelvin_helmholtz(CCTK_ARGUMENTS);
  void GRSPH_setup_sedovblast2D(CCTK_ARGUMENTS);
  void GRSPH_setup_sedovblast3D(CCTK_ARGUMENTS);
  void GRSPH_star_init(CCTK_ARGUMENTS); // function defined in star_init.cc
 

extern "C" void GRSPH_init_to_zero(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  //set boundary values to NaN
  for (ptrdiff_t bi = 0; bi < 6; ++bi) {
    boundaryval[bi] = sqrt(-1);
  } 
  

#pragma omp parallel for 
  for (ptrdiff_t i = 0; i < maxnparticles; ++i) {
  
    	posx[i] = 0.0;
	posy[i] = 0.0;
	posz[i] = 0.0;
	mass[i] = 0.0;
	velx[i] = 0.0;
	vely[i] = 0.0;
	velz[i] = 0.0;
	eps[i] = 0.0;
	dens[i] = 0.0;
	dens0[i] = 0.0;
	sx[i] = 0.0;
	sy[i] = 0.0;
	sz[i] = 0.0;
	tau[i] = 0.0;
	alphap[i] = 0.0;
	sdetgp[i] = 0.0;
	betaxp[i] = 0.0;
	betayp[i] = 0.0;
	betazp[i] = 0.0;
	gxxp[i] = 0.0;
	gxyp[i] = 0.0;
	gxzp[i] = 0.0;
	gyyp[i] = 0.0;
	gyzp[i] = 0.0;
	gzzp[i] = 0.0;
	W[i] = 0.0;
	rho[i] = 0.0;
	press[i] = 0.0;
	smoothl[i] = 0.0;
	visca[i] = 0.0;
	visc[i] = 0.0;
	NewtAccelx[i] = 0.0;
	NewtAccely[i] = 0.0;
	NewtAccelz[i] = 0.0;
  }
  
  // initialize also 3D stuff ?
}

extern "C" void GRSPH_init(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;


  if(CCTK_EQUALS(initial_data,"shocktube")) {

    // setup shocktube in here
    GRSPH_setup_shocktube(CCTK_PASS_CTOC);
  }else if (CCTK_EQUALS(initial_data,"shocktube_eqmass")) {
    // setup shocktube with equal mass in here                 
    GRSPH_setup_shocktube_eqmass(CCTK_PASS_CTOC);
  }else if (CCTK_EQUALS(initial_data,"shocktube_eqmass3D")) {
    // 3D setup shocktube with equal mass in here               
    GRSPH_setup_shocktube_eqmass3D(CCTK_PASS_CTOC);
  }else if (CCTK_EQUALS(initial_data,"shocktube_eqmassHR")) {
    // setup high relativistic shocktube with equal mass in here      
    GRSPH_setup_shocktube_eqmassHR(CCTK_PASS_CTOC);
  }else if (CCTK_EQUALS(initial_data,"kelvin_helmholtz")) {
    // setup for kelvin helmholtz instabilities in here                       
     GRSPH_setup_kelvin_helmholtz(CCTK_PASS_CTOC);
  }else if (CCTK_EQUALS(initial_data,"sedovblast2D")) {
    // setup for 2D sedov blast in here                                     
    GRSPH_setup_sedovblast2D(CCTK_PASS_CTOC);
  }else if (CCTK_EQUALS(initial_data,"sedovblast3D")) {
    // setup for 3D sedov blast in here                                       
     GRSPH_setup_sedovblast3D(CCTK_PASS_CTOC);
  }else if (CCTK_EQUALS(initial_data,"star")) {
    // setup for star in here                                            
     GRSPH_star_init(CCTK_PASS_CTOC);
  } else {
    CCTK_WARN(0,"Your choice of initial data is not implemented");
  }

}
void GRSPH_setup_shocktube_eqmass3D(CCTK_ARGUMENTS) {

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const CCTK_REAL xmin = -0.5;
  const CCTK_REAL ymin = -0.5;
  const CCTK_REAL zmin = -0.5;

  //define kernel dimension (3D)                                                                                                                                   
  kerneldim[0] = 2.0;
  kerneldim[1] = 10.0/(7.0*M_PI);//1.0 / M_PI;                                                                                                                     
  //define boundary values                                                                                                                                         
  //boundaryval[0] = xmin - 0.005;
  //boundaryval[1] = -xmin + 0.005;
  //boundaryval[2] = ymin - 0.005;
  //boundaryval[3] = -ymin + 0.005;
  //boundaryval[4] = zmin - 0.005;
  //boundaryval[5] = -zmin + 0.005;

  *nparticles = 101*101;

  ptrdiff_t ipart = 0;
  for (CCTK_INT i = 0; i < 101; ++i) {
    for (CCTK_INT j = 0; j < 101; ++j) {
      //for (CCTK_INT k = 0; k < 101; ++k) {                                                                                                                       
      posx[ipart] = xmin +i*0.01;
      posy[ipart] = ymin +j*0.01;
      posz[ipart] = 0.0;//zmin +k*0.01;                                                                                                                          



      //press[ipart] = 0.1;                                                                                                                                      
      if (posx[ipart] < resolution_par) {
	mass[ipart] = 1.0 / (100.0*100.0);
	rho[ipart] = 1.0;
	press[ipart] = 1.0;
	eps[ipart] = press[ipart]/(eos_gamma - 1.0)/rho[ipart];

      } else {
	mass[ipart] = 0.125 / (100.0*100.0);
	rho[ipart] = 0.125;
	press[ipart] = 0.1;
	eps[ipart] = press[ipart]/(eos_gamma - 1.0)/rho[ipart];
      }
      press[ipart] = GRSPH_press(rho[ipart],eps[ipart]);
      id[ipart] = ipart;


    // all other settings                                                                                                      
    velx[ipart] = 0.0;
    vely[ipart] = 0.0;
    velz[ipart] = 0.0;
    alphap[ipart] = 1.0;
    sdetgp[ipart] = 1.0;
    gxxp[ipart] = 1.0;
    gxyp[ipart] = 0.0;
    gxzp[ipart] = 0.0;
    gyyp[ipart] = 1.0;
    gyzp[ipart] = 0.0;
    gzzp[ipart] = 1.0;
    betaxp[ipart] = 0.0;
    betayp[ipart] = 0.0;
    betazp[ipart] = 0.0;
    smoothl[ipart] = eta_smooth * pow(mass[i]/rho[i],1.0/2.0);
    visca[ipart] = 1.0;

    ++ipart;
    }
  }

  CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
}









void GRSPH_setup_kelvin_helmholtz(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const CCTK_REAL rho_high = 2.0;
  const CCTK_REAL rho_low = 1.0;

    ptrdiff_t i = 0;
    CCTK_REAL xpos = -0.5;
    CCTK_REAL ypos = -0.25;
    //define kernel dimension (2D)
    kerneldim[0] = 2.0;
    kerneldim[1] = 10.0 / (7.0 * M_PI);


    if (7555+400 > maxnparticles)
      CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
 

    // high density region between |y|<0.25
    while (xpos <= 0.5) {
      while (ypos <= 0.25) {
	mass[i] = 0.2e-6/1.2;
	posy[i] = ypos;
	posx[i] = xpos;

	velx[i] = 0.5;

	if (fabs(ypos-0.25) < 0.025) {
	  vely[i] = 0.025 * sin(-2.0 * M_PI * (xpos + 0.5) * 6.0);
	} else if (fabs(ypos+0.25) < 0.025) {
	  vely[i] = 0.025 * sin(2.0 * M_PI * (xpos + 0.5) * 6.0);
	}



	// all other settings
        press[i] = 2.5;
        rho[i] = rho_high;
        eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];

	press[i] = GRSPH_press(rho[i],eps[i]);
	id[i] = i;
	alphap[i] = 1.0;
	sdetgp[i] = 1.0;
	gxxp[i] = 1.0;
	gxyp[i] = 0.0;
	gxzp[i] = 0.0;
	gyyp[i] = 1.0;
	gyzp[i] = 0.0;
	gzzp[i] = 1.0;
	betaxp[i] = 0.0;
	betayp[i] = 0.0;
	betazp[i] = 0.0;
	smoothl[i] = hsml;//eta_smooth*sqrt(mass[i]/rho[i]);
	visca[i] = 1.0;


	ypos += 0.01; 		
	i++;  
      } // y loop

    xpos += 0.01;
      ypos = -0.25;

    } // x loop
  
    // low density region
    xpos = -0.5;
    ypos = 0.25;
    while (xpos <= 0.5) {
      while (ypos <= 0.5) {
	mass[i] =  0.2e-6/1.2;
	posy[i] = ypos;
	posx[i] = xpos;

	velx[i] = -0.5;
	
	// all other settings
        press[i] = 2.5;
        rho[i] = rho_low;
        eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];

	press[i] = GRSPH_press(rho[i],eps[i]);
	id[i] = i;
	alphap[i] = 1.0;
	sdetgp[i] = 1.0;
	gxxp[i] = 1.0;
	gxyp[i] = 0.0;
	gxzp[i] = 0.0;
	gyyp[i] = 1.0;
	gyzp[i] = 0.0;
	gzzp[i] = 1.0;
	betaxp[i] = 0.0;
	betayp[i] = 0.0;
	betazp[i] = 0.0;
	smoothl[i] = hsml*1.44;//eta_smooth*sqrt(mass[i]/rho[i]);
	visca[i] = 1.0;




	i++;
	//second particle
	mass[i] = 0.2e-6/1.2;
	posy[i] = ypos - 0.75;
	posx[i] = xpos;
	velx[i] = -0.5;


	// all other settings
        press[i] = 2.5;
        rho[i] = rho_low;
        eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];

	press[i] = GRSPH_press(rho[i],eps[i]);
	id[i] = i;
	alphap[i] = 1.0;
	sdetgp[i] = 1.0;
	gxxp[i] = 1.0;
	gxyp[i] = 0.0;
	gxzp[i] = 0.0;
	gyyp[i] = 1.0;
	gyzp[i] = 0.0;
	gzzp[i] = 1.0;
	betaxp[i] = 0.0;
	betayp[i] = 0.0;
	betazp[i] = 0.0;
	smoothl[i] = hsml*1.44;//eta_smooth*sqrt(mass[i]/rho[i]);
	visca[i] = 1.0;



	ypos += sqrt(2) * 0.01;	
	i++;
      } // y loop
      xpos += sqrt(2) * 0.01;
      ypos = 0.25;
    } // x loop
    i--;
    *nparticles = i;

  CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
}
}  // end namespace









// old initialization
#if 0

  const CCTK_REAL Mtot = initial_mass;
  const CCTK_REAL xmin = initial_xmin, ymin = initial_ymin, zmin = initial_zmin;
  const CCTK_REAL xmax = initial_xmax, ymax = initial_ymax, zmax = initial_zmax;
  const CCTK_REAL Vtot = (xmax - xmin) * (ymax - ymin) * (zmax - zmin);
  const ptrdiff_t ni = initial_ni, nj = initial_nj, nk = initial_nk;
  const ptrdiff_t np = ni * nj * nk;

  if (np > maxnparticles)
    CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
  *nparticles = np;

#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) {
    for (ptrdiff_t gj = 0; gj < nj; ++gj) {
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
        ptrdiff_t i = gi + ni * (gj + nj * gk);
        id[i] = i;
        mass[i] = Mtot / np;
        posx[i] = xmin + (gi + 0.5) * (xmax - xmin) / ni;
        posy[i] = ymin + (gj + 0.5) * (ymax - ymin) / nj;
        posz[i] = zmin + (gk + 0.5) * (zmax - zmin) / nk;
        velx[i] = 0.0;
        vely[i] = 0.0;
        velz[i] = 0.0;
	alphap[i] = 1.0;
	sdetgp[i] = 1.0;
	gxxp[i] = 1.0;
	gxyp[i] = 1.0;
	gxzp[i] = 1.0;
	gyyp[i] = 1.0;
	gyzp[i] = 1.0;
	gzzp[i] = 1.0;
	betaxp[i] = 1.0;
	betayp[i] = 1.0;
	betazp[i] = 1.0;
      }
    }
  }
  CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
#endif
