#ifndef DENS_UPDATE_HH
#define DENS_UPDATE_HH

#include <cctk.h>
#include <cctk_Parameters.h>
#include <cmath>

#include "kernel.hh"

namespace GRSPH {
using namespace std;

  static inline CCTK_REAL compute_dens(const int* js,
                                       const ptrdiff_t jindmin,
                                       const ptrdiff_t jindmax,
                                       const ptrdiff_t i,
                                       const CCTK_REAL* posx,
                                       const CCTK_REAL* posy,
                                       const CCTK_REAL* posz,
                                       const CCTK_REAL* mass,
                                       const CCTK_REAL  h,
				       const CCTK_REAL dim,
				       const CCTK_REAL coeff) {

    CCTK_REAL dens = 0.0;

    // loop over neighbors                                                                                              
    for (ptrdiff_t jind = jindmin; jind < jindmax; ++jind){
      const ptrdiff_t j = js[jind];
      const CCTK_REAL xij = posx[i] - posx[j];
      const CCTK_REAL yij = posy[i] - posy[j];
      const CCTK_REAL zij = posz[i] - posz[j];

      //Calculate first conserved density                                                                               
      dens += mass[j] * M4kernel(xij,yij,zij,h,dim,coeff);
    }
    // need to include particle itself                                                                                  
    dens += mass[i] * M4kernel(0.0,0.0,0.0,h,dim,coeff);

    return dens;
  }


}
#endif // #ifndef DENS_UPDATE_HH
