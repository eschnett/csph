#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

namespace GRSPH {

namespace {
void register_group(const char *evolved, const char *rhs) {
  int evolved_index = CCTK_GroupIndex(evolved);
  if (evolved_index < 0)
    CCTK_ERROR("Unknown group name");
  int rhs_index = CCTK_GroupIndex(rhs);
  if (rhs_index < 0)
    CCTK_ERROR("Unknown group name");
  int ierr = MoLRegisterEvolvedGroup(evolved_index, rhs_index);
  if (ierr)
    CCTK_ERROR("Could not register evolved group");
}
}

extern "C" void GRSPH_MoL(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  register_group("GRSPH::pos"  , "GRSPH::posrhs");
  register_group("GRSPH::scon" , "GRSPH::sconrhs");
  register_group("GRSPH::tau"  , "GRSPH::taurhs");
  register_group("GRSPH::visca", "GRSPH::viscarhs");

}
}
