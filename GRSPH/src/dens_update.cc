#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include <cmath>

#include "interact.hh"
#include "dens_update.hh"

namespace GRSPH {

using namespace std;

extern "C" void GRSPH_dens_update_fixed_smoothl(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;


#pragma omp for schedule(dynamic, 1000)
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    

    const auto &restrict neighbours = interactions.particles_neighbours[i];
    const auto &restrict js = *neighbours.js;


    dens[i] = compute_dens((int*)js.data(),neighbours.jindmin,neighbours.jindmax,
			   i,posx,posy,posz,mass,smoothl[i],kerneldim[0],kerneldim[1]);
    
  } // for 

} // GRSPH_dens_update_fixed_smoothl

extern "C" void GRSPH_dens_update_var_smoothl(CCTK_ARGUMENTS) {
  // compute the smoothing length based on description in
  // Cossins PhD thesis, arXiv:1007.1245v2, section 3.5
  //
  // NOTE: this routine is missing the grad h term Omega! 
  // This must still be implemented
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // convergence accuracy
  const CCTK_REAL prec = prec_smooth;
  const CCTK_REAL eta = eta_smooth;
  const CCTK_INT countmax = 100;



#pragma omp parallel for schedule(dynamic, 1000)
  for (ptrdiff_t i = 0; i < *nparticles; ++i){

    CCTK_REAL h = smoothl[i];
    CCTK_REAL tempdens = 0.0;
    CCTK_INT count = 0;
    bool done = false;



    const auto &restrict neighbours = interactions.particles_neighbours[i];
    const auto &restrict jjs = *neighbours.js;

      
    //    fprintf(stderr,"%4d %15.6E %15.6E %15.6E\n",i,dens[i],dens0[i],smoothl[i]);



#if 0      

    // Newton Raphson iteration to find smoothing length 
    while(!done && count < countmax) {

      count++;
      tempdens = compute_dens((int*)jjs.data(),neighbours.jindmin,neighbours.jindmax,
			      i,posx,posy,posz,mass,h,kerneldim[0],kerneldim[1]);

      // NOW the NR step
#if 0
      // Price calculation
      CCTK_REAL F = mass[i] * pow(eta/h,3.0) - tempdens;
      CCTK_REAL dFdh = - 3.0 * tempdens / h;
#endif
      // Siegler & Riffert 2000 calculation
      CCTK_REAL F = dens0[i] * pow(hsml/h,1.0) - tempdens;
      CCTK_REAL dFdh = - 1.0 * tempdens / h;
      CCTK_REAL dh = - F / dFdh;
      CCTK_REAL hnew = h + dh;
      CCTK_REAL reldiff = abs(hnew-h)/h;

      done = ( reldiff < prec );
      h = hnew;

    } // while
#endif

    h = eta_smooth * pow(mass[i]/dens[i], 1.0/kerneldim[0]) ;//hsml * pow(dens0[i]/dens[i],1.0);

    tempdens = compute_dens((int*)jjs.data(),neighbours.jindmin,neighbours.jindmax,
			    i,posx,posy,posz,mass,h,kerneldim[0],kerneldim[1]);

    if (count >= countmax) {
      CCTK_WARN(0,"Did not converge in smoothing length calculation!");
    }
    
    smoothl[i] = h;
    dens[i] = tempdens;

  } // for

} // GRSPH_dens_update_var_smoothl



} // namespace GRSPH
