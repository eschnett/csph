#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include "interact.hh"

#include <omp.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <sys/time.h>

namespace GRSPH {
using namespace std;

interactions_t interactions;

namespace {
double gettime() {
  timeval tv;
  gettimeofday(&tv, nullptr);
  return tv.tv_sec + tv.tv_usec / 1.0e+6;
}
}


extern "C" void GRSPH_interact(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"Start Interact");

  // Grid size
  const ptrdiff_t ai = cctk_ash[0];
  const ptrdiff_t aj = cctk_ash[1];
  const ptrdiff_t ak = cctk_ash[2];
  const ptrdiff_t nallocated = ai * aj * ak;
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];
  const ptrdiff_t ncells = ni * nj * nk;

  // Global domain
  const CCTK_REAL x0 = CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL y0 = CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL z0 = CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  // Local domain
  const CCTK_REAL xmin = x0 + cctk_lbnd[0] * dx;
  const CCTK_REAL ymin = y0 + cctk_lbnd[1] * dy;
  const CCTK_REAL zmin = z0 + cctk_lbnd[2] * dz;


  // Initialize interaction lists                                                                                      
  vector<int> grid(nallocated);
#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) {
    for (ptrdiff_t gj = 0; gj < nj; ++gj) {
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
        ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
        grid[gidx] = -1;
      }
    }
  }

  // Distribute particles onto grid                                                                                    
  vector<int> next(*n_g_particles);
#pragma omp parallel for
  for (ptrdiff_t i = 0; i < *n_g_particles; ++i) {
    ptrdiff_t gi = lrint((posx[i] - xmin) / dx);
    ptrdiff_t gj = lrint((posy[i] - ymin) / dy);
    ptrdiff_t gk = lrint((posz[i] - zmin) / dz);
    if (gi < 0 || gi >= ni || gj < 0 || gj >= nj || gk < 0 || gk >= nk) {
#pragma omp critical
      fprintf(stderr,"%d %18.9E %18.9E \n",i,posx[i],posy[i]);
      CCTK_ERROR("Particle outside the grid; domain decomposition failed");
    }
    ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
    // Insert particle into list                                                                                       
    // Note: This inserts particles in descending order                                                                
    ptrdiff_t j;
#pragma omp atomic capture
    {
      j = grid[gidx];
      grid[gidx] = i;
    }
    next[i] = j;
  }

#if 0
  // this code is only for convenience output
  ptrdiff_t max_nparticles_cell = 0;
  ptrdiff_t n_zeroparticles_cells = 0;
#pragma omp parallel for collapse(3) reduction(max : max_nparticles_cell) \
  reduction(+ : n_zeroparticles_cells)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) {
    for (ptrdiff_t gj = 0; gj < nj; ++gj) {
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
        ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
        ptrdiff_t np = 0;
        for (ptrdiff_t i = grid[gidx]; i >= 0; i = next[i])
          ++np;
        max_nparticles_cell = max(max_nparticles_cell, np);
        n_zeroparticles_cells += np == 0;
      }
    }
  }
#endif

  // Find interations
  interactions.particles_neighbours.resize(*n_g_particles);
  interactions.threads_js.resize(omp_get_max_threads());
  for (auto &js : interactions.threads_js)
    js.clear();


  ptrdiff_t ninteractions = 0;
  ptrdiff_t max_ninteractions_particle = 0;
  ptrdiff_t n_zerointeractions_particles = 0;

#if 0
  // Trivial (inefficient) implementation
  auto &js = interactions.threads_js.at(0);
  for (ptrdiff_t i = 0; i < *n_g_particles; ++i) {

    auto &neighbours = interactions.particles_neighbours[i];
    neighbours.js = &js;
    neighbours.jindmin = js.size();
    for (ptrdiff_t j = 0; j < *n_g_particles; ++j) {
      if (j != i) {
	CCTK_REAL dist = sqrt( (posx[i]-posx[j])*(posx[i]-posx[j]) +
			       (posy[i]-posy[j])*(posy[i]-posy[j]) +
			       (posz[i]-posz[j])*(posz[i]-posz[j]) );
	if (dist <= neib_fac * smoothl[i]) {
	  js.push_back(j);
	}
      }
    }
    neighbours.jindmax = js.size();
    max_ninteractions_particle = max(max_ninteractions_particle,
                                     neighbours.jindmax - neighbours.jindmin);
  }
  ninteractions = js.size();

#else

#pragma omp parallel reduction(max : max_ninteractions_particle)               \
    reduction(+ : ninteractions, n_zerointeractions_particles)
  {
    auto &js = interactions.threads_js.at(omp_get_thread_num());

// Loop over all particles
#pragma omp for schedule(dynamic, 1000)
    for (ptrdiff_t i = 0; i < *n_g_particles; ++i) {

      auto &neighbours = interactions.particles_neighbours[i];
      neighbours.js = &js;
      neighbours.jindmin = js.size();

      // Loop over all neighbouring grid cells
      ptrdiff_t simin =
          max(ptrdiff_t(0), ptrdiff_t(lrint((posx[i] - neib_fac * smoothl[i] - xmin) / dx)));
      ptrdiff_t sjmin =
          max(ptrdiff_t(0), ptrdiff_t(lrint((posy[i] - neib_fac * smoothl[i] - ymin) / dy)));
      ptrdiff_t skmin =
          max(ptrdiff_t(0), ptrdiff_t(lrint((posz[i] - neib_fac * smoothl[i] - zmin) / dz)));
      ptrdiff_t simax =
          min(ni, ptrdiff_t(lrint((posx[i] + neib_fac * smoothl[i] - xmin) / dx)) + 1);
      ptrdiff_t sjmax =
          min(nj, ptrdiff_t(lrint((posy[i] + neib_fac * smoothl[i] - ymin) / dy)) + 1);
      ptrdiff_t skmax =
          min(nk, ptrdiff_t(lrint((posz[i] + neib_fac * smoothl[i] - zmin) / dz)) + 1);

      for (ptrdiff_t sk = skmin; sk < skmax; ++sk) {
        for (ptrdiff_t sj = sjmin; sj < sjmax; ++sj) {
          for (ptrdiff_t si = simin; si < simax; ++si) {
            ptrdiff_t sidx = CCTK_GFINDEX3D(cctkGH, si, sj, sk);
            // Loop over all particles in the neighbouring cell
            for (ptrdiff_t j = grid[sidx]; j >= 0; j = next[j]) {
              // Skip self-interactions
	        if (j == i)
                continue;

              // Skip particles that are too far apart
              CCTK_REAL xij = posx[i] - posx[j];
              CCTK_REAL yij = posy[i] - posy[j];
              CCTK_REAL zij = posz[i] - posz[j];
              CCTK_REAL dr2 = pow(xij, 2) + pow(yij, 2) + pow(zij, 2);
              if (dr2 >= pow(neib_fac * smoothl[i], 2))
                continue;

              // Insert interaction
              js.push_back(j);
            }
          }
        }
      }

      neighbours.jindmax = js.size();
      ptrdiff_t niacs = neighbours.jindmax - neighbours.jindmin;
      ninteractions += niacs;
      max_ninteractions_particle = max(max_ninteractions_particle, niacs);
      n_zerointeractions_particles += niacs == 0;
    }
  }

#endif

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"End Interact");

}
} // namespace GRSPH
