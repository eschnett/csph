#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include <cmath>


namespace GRSPH {

  using namespace std;

extern "C" void GRSPH_CalcSDetG(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // Grid size
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];


  // compute sdetg
#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) 
    for (ptrdiff_t gj = 0; gj < nj; ++gj) 
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
	ptrdiff_t sdindex = CCTK_GFINDEX3D(cctkGH,gi,gj,gk);

	grid_sdetg[sdindex] = gxx[sdindex]*gyy[sdindex]*gzz[sdindex] 
	  + 2.0*gxy[sdindex]*gxz[sdindex]*gyz[sdindex] 
	  - gyy[sdindex]*gxz[sdindex]*gxz[sdindex]
	  - gxx[sdindex]*gyz[sdindex]*gyz[sdindex]
	  - gzz[sdindex]*gxy[sdindex]*gxy[sdindex];
			       
        grid_sdetg[sdindex] = sqrt(grid_sdetg[sdindex]);

      } // loop i,j,k

} // function GRSPH_CalcSDetG

} // namespace GRSPH
