#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cmath>
#include <cstddef>

#include "kernel.hh"

namespace GRSPH {
using namespace std;

static inline __attribute__((always_inline))
CCTK_REAL GRSPH_linterp_3D(const int* __restrict__ idx,
			   const CCTK_REAL delx,
			   const CCTK_REAL dely,
			   const CCTK_REAL delz,
			   const CCTK_REAL dxi,
			   const CCTK_REAL dyi,
			   const CCTK_REAL dzi,
			   const CCTK_REAL dxdyi,
			   const CCTK_REAL dxdzi,
			   const CCTK_REAL dydzi,
			   const CCTK_REAL dxdydzi,
			   const CCTK_REAL* __restrict__ var) {

  //helper vars
  CCTK_REAL fh[8], a[8];
  fh[0] = var[idx[0]];
  fh[1] = var[idx[1]];
  fh[2] = var[idx[2]];
  fh[3] = var[idx[3]];
  fh[4] = var[idx[4]];
  fh[5] = var[idx[5]];
  fh[6] = var[idx[6]];
  fh[7] = var[idx[7]];

  // set up coeffs of interpolation polynomical and evaluate function
  // values
  a[0] = fh[0];
  a[1] = dxi *   ( fh[1] - fh[0] );
  a[2] = dyi *   ( fh[2] - fh[0] );
  a[3] = dzi *   ( fh[3] - fh[0] );
  a[4] = dxdyi *  ( fh[4] - fh[1] - fh[2] + fh[0] );
  a[5] = dxdzi *  ( fh[5] - fh[1] - fh[3] + fh[0] );
  a[6] = dydzi *  ( fh[6] - fh[2] - fh[3] + fh[0] );
  a[7] = dxdydzi * ( fh[7] - fh[0] + fh[1] + fh[2] +
                         fh[3] - fh[4] - fh[5] - fh[6] );
  return a[0] + a[1] * delx
    + a[2] * dely
    + a[3] * delz
    + a[4] * delx * dely
    + a[5] * delx * delz
    + a[6] * dely * delz
    + a[7] * delx * dely * delz;

} // GRSPH_linterp_3D

extern "C" void GRSPH_interpolate_to_particles(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"Start Interpolate");

  // Grid size                                                                                                      
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];

  // Global domain                                                                                                  
  const CCTK_REAL x0 = CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL y0 = CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL z0 = CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi = 1.0/dx;
  const CCTK_REAL dyi = 1.0/dy;
  const CCTK_REAL dzi = 1.0/dz;
  const CCTK_REAL dxdyi = dxi*dyi;
  const CCTK_REAL dxdzi = dxi*dzi;
  const CCTK_REAL dydzi = dyi*dzi;
  const CCTK_REAL dxdydzi = dxi*dyi*dzi;

  // Local domain                                                                                                   
  const CCTK_REAL xmin = x0 + cctk_lbnd[0] * dx;
  const CCTK_REAL ymin = y0 + cctk_lbnd[1] * dy;
  const CCTK_REAL zmin = z0 + cctk_lbnd[2] * dz;
  
#pragma omp parallel for
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    ptrdiff_t gi = lrint((posx[i] - xmin) * dxi);
    ptrdiff_t gj = lrint((posy[i] - ymin) * dyi);
    ptrdiff_t gk = lrint((posz[i] - zmin) * dzi);
    if (gi < 1 || gi >= ni-1 || gj < 1 || gj >= nj-1 || gk < 1 || gk >= nk-1) {
      //fprintf(stderr,"%d %d %15.6E %15.6E %15.6E\n",cctk_iteration,(int)i,posx[i],posy[i],posz[i]);
      CCTK_ERROR("Particle outside the grid; domain decomposition failed");
    }

    ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);

    // 0th order interpolation at this point
    alphap[i]  = alp[gidx];
    sdetgp[i]  = grid_sdetg[gidx];
    gxxp[i]    = gxx[gidx];
    gxyp[i]    = gxy[gidx];
    gxzp[i]    = gxz[gidx];
    gyyp[i]    = gyy[gidx];
    gyzp[i]    = gyz[gidx];
    gzzp[i]    = gzz[gidx];
    betaxp[i]  = betax[gidx];
    betayp[i]  = betay[gidx];
    betazp[i]  = betaz[gidx];

    if(do_gravity_source) {
      // Interpolate Newtonian potential to particles
      // using trilinear interpolation.
      int idx[8];
      gi = 1 + (int) ((posx[i] - xmin) * dxi);
      gj = 1 + (int) ((posy[i] - ymin) * dyi);
      gk = 1 + (int) ((posz[i] - zmin) * dzi);

      idx[0] = CCTK_GFINDEX3D(cctkGH, gi  , gj  , gk  );
      idx[1] = CCTK_GFINDEX3D(cctkGH, gi-1, gj  , gk  );
      idx[2] = CCTK_GFINDEX3D(cctkGH, gi  , gj-1, gk  );
      idx[3] = CCTK_GFINDEX3D(cctkGH, gi  , gj  , gk-1);
      idx[4] = CCTK_GFINDEX3D(cctkGH, gi-1, gj-1, gk  );
      idx[5] = CCTK_GFINDEX3D(cctkGH, gi-1, gj  , gk-1);
      idx[6] = CCTK_GFINDEX3D(cctkGH, gi  , gj-1, gk-1);
      idx[7] = CCTK_GFINDEX3D(cctkGH, gi-1 ,gj-1, gk-1);

      CCTK_REAL delx = x[idx[0]] - posx[i];
      CCTK_REAL dely = y[idx[0]] - posy[i];
      CCTK_REAL delz = z[idx[0]] - posz[i];
      
      NewtAccelx[i] = GRSPH_linterp_3D(idx,delx,dely,delz,
				       dxi,dyi,dzi,dxdyi,dxdzi,dydzi,dxdydzi,
				       grid_NewtAccelx);
      
      NewtAccely[i] = GRSPH_linterp_3D(idx,delx,dely,delz,
				       dxi,dyi,dzi,dxdyi,dxdzi,dydzi,dxdydzi,
				       grid_NewtAccely);
      
      NewtAccelz[i] = GRSPH_linterp_3D(idx,delx,dely,delz,
				       dxi,dyi,dzi,dxdyi,dxdzi,dydzi,dxdydzi,
				       grid_NewtAccelz);
#if 0
    if (i == 1000) {
      fprintf(stderr,"%15.6E %15.6E %15.6E %15.6E\n",posx[i],posy[i],posz[i],
	      sqrt(posx[i]*posx[i] + posy[i]*posy[i] + posz[i]*posz[i]));
      fprintf(stderr,"%d %d %d %d\n", i,gi,gj,gk);
      fprintf(stderr,"%15.6E %15.6E %15.6E\n",x[idx[0]],y[idx[0]],z[idx[0]]);
      fprintf(stderr,"%15.6E %15.6E %15.6E\n",x[idx[1]],y[idx[2]],z[idx[3]]);
      fprintf(stderr,"%15.6E %15.6E %15.6E\n",NewtAccelx[i],
	      NewtAccely[i],NewtAccelz[i]);
    }
#endif

    }

  }
#if 0
  fprintf(stderr,"debug abort in interpolate_to_particles.cc\n");
  abort();
#endif

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"End Interpolate");


}
}
