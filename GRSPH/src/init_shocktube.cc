#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cstddef>
#include "eos.hh"
#include <math.h>

namespace GRSPH {
  using namespace std;
  void GRSPH_setup_shocktube(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    const CCTK_REAL rho_left = 1.0;
    const CCTK_REAL rho_right = 0.125;

    const CCTK_REAL xmin = initial_xmin;
    const CCTK_REAL xmax = initial_xmax;
    const ptrdiff_t np = 1000;

    //define kernel dimension (1D)                                                
    kerneldim[0] = 1.0;
    kerneldim[1] = 2.0 / 3.0;
    //define boundary values                                                      
    boundaryval[0] = -0.5005;
    boundaryval[1] = 0.4995;


    if (np+200 > maxnparticles)
      CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
    *nparticles = np;

#pragma omp parallel for
    for (ptrdiff_t i = 0; i < np; ++i) {

      // high density and low density region                                    
      if (i < 500) {
        mass[i] = rho_left/7.643512e+07;
        press[i] = 1.0;
	rho[i] = rho_left;
        eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];
      } else {
        mass[i]  = rho_right/7.643512e+07;
        press[i] = 0.1;
        rho[i] = rho_right;
        eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];
      }
      posx[i] = xmin + (i * 0.001);


      press[i] = GRSPH_press(rho[i],eps[i]);
      id[i] = i;
      // 1D line of particles                                                   

      // fprintf(stderr,"init %ld %15.6E %15.6E %15.6E\n",i,posx[i],mass[i],eps\
      [i]);                                                                           
    posy[i] = 0.0;
    posz[i] = 0.0;
    // all other settings                                                     
    velx[i] = 0.0;
    vely[i] = 0.0;
    velz[i] = 0.0;
    alphap[i] = 1.0;
    sdetgp[i] = 1.0;
    gxxp[i] = 1.0;
    gxyp[i] = 0.0;
    gxzp[i] = 0.0;
    gyyp[i] = 1.0;
    gyzp[i] = 0.0;
    gzzp[i] = 1.0;
    betaxp[i] = 0.0;
    betayp[i] = 0.0;
    betazp[i] = 0.0;
    smoothl[i] = hsml;
    visca[i] = 1.0;
  }

  // update the smoothing length                                              
  *n_g_particles = *nparticles;
  GRSPH_initdens(CCTK_PASS_CTOC);
  // mass correction factor (specific to this test problem)                   
  ptrdiff_t i = 100;
  CCTK_REAL mass_corr_fac = rho_left/dens[i];

  //    fprintf(stderr,"ZZ: %15.6E %15.6E %15.6E\n",mass_corr_fac,dens[i],den\
  s[i]*mass_corr_fac);                                                            

for (ptrdiff_t i = 0; i < *nparticles; ++i) {
  mass[i] = mass[i] * mass_corr_fac;
  smoothl[i] = hsml;
 }

CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
}



void GRSPH_setup_shocktube_eqmass(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const CCTK_REAL rho_left = 1.0;
  const CCTK_REAL rho_right = 0.125;

  const CCTK_REAL xmin = initial_xmin;
  const CCTK_REAL xmax = initial_xmax;
  const ptrdiff_t np = 900;

  //define kernel dimension (1D)                                              
  kerneldim[0] = 1.0;
  kerneldim[1] = 2.0 / 3.0;
  //define boundary values                                                    
  boundaryval[0] = -0.5 - (0.25*0.00125);
  boundaryval[1] = 0.5 - 0.0025;

  if (np+200 > maxnparticles)
    CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
  *nparticles = np;

#pragma omp parallel for
  for (ptrdiff_t i = 0; i < np; ++i) {

    // high density and low density region                                    
    if (i < 800) {
      posx[i] = xmin + (i * 0.5 * 0.00125);
      press[i] = 1.0;
      rho[i] = rho_left;
      eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];
    } else {
      posx[i] = (i-800) * 0.005;
      press[i] = 0.1;
      rho[i] = rho_right;
      eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];
    }

    mass[i] =0.5 * 0.12445e-02;

    press[i] = GRSPH_press(rho[i],eps[i]);
    id[i] = i;
    // 1D line of particles                                                   

    // fprintf(stderr,"init %ld %15.6E %15.6E %15.6E\n",i,posx[i],mass[i],eps\
    [i]);                                                                           
  posy[i] = 0.0;
  posz[i] = 0.0;
  // all other settings                                                     
  velx[i] = 0.0;
  vely[i] = 0.0;
  velz[i] = 0.0;
  alphap[i] = 1.0;
  sdetgp[i] = 1.0;
  gxxp[i] = 1.0;
  gxyp[i] = 0.0;
  gxzp[i] = 0.0;
  gyyp[i] = 1.0;
  gyzp[i] = 0.0;
  gzzp[i] = 1.0;
  betaxp[i] = 0.0;
  betayp[i] = 0.0;
  betazp[i] = 0.0;
  smoothl[i] = eta_smooth*(mass[i]/rho[i]);
  visca[i] = 1.0;
}

CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
}


void GRSPH_setup_shocktube_eqmassHR(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const CCTK_REAL rho_left = 1.0;
  const CCTK_REAL rho_right = 0.125;

  const CCTK_REAL xmin = initial_xmin;
  const CCTK_REAL xmax = initial_xmax;
  const ptrdiff_t np = 900;

  //define kernel dimension (1D)                                              
  kerneldim[0] = 1.0;
  kerneldim[1] = 2.0 / 3.0;
  //define boundary values                                                    
  boundaryval[0] = -0.5 - (0.25*0.00125);
  boundaryval[1] = 0.5 - 0.0025;

  if (np+200 > maxnparticles)
    CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
  *nparticles = np;

#pragma omp parallel for
  for (ptrdiff_t i = 0; i < np; ++i) {

    // high density and low density region                                    
    if (i < 800) {
      posx[i] = xmin + (i * 0.5 * 0.00125);
      press[i] = 10.0;
      rho[i] = rho_left;
      eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];
    } else {
      posx[i] = (i-800) * 0.005;
      press[i] = 0.01;
      rho[i] = rho_right;
      eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];
    }

    mass[i] =0.5 * 0.12445e-02;

    press[i] = GRSPH_press(rho[i],eps[i]);
    id[i] = i;
    // 1D line of particles                                                   

    // fprintf(stderr,"init %ld %15.6E %15.6E %15.6E\n",i,posx[i],mass[i],eps\
    [i]);                                                                           
  posy[i] = 0.0;
  posz[i] = 0.0;
  // all other settings                                                     
  velx[i] = 0.0;
  vely[i] = 0.0;
  velz[i] = 0.0;
  alphap[i] = 1.0;
  sdetgp[i] = 1.0;
  gxxp[i] = 1.0;
  gxyp[i] = 0.0;
  gxzp[i] = 0.0;
  gyyp[i] = 1.0;
  gyzp[i] = 0.0;
  gzzp[i] = 1.0;
  betaxp[i] = 0.0;
  betayp[i] = 0.0;
  betazp[i] = 0.0;
  smoothl[i] = eta_smooth*(mass[i]/rho[i]);
  visca[i] = 1.0;
}

CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
}


} // end namespace
