#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <vector>

#include "interact.hh"

namespace GRSPH {
using namespace std;


extern "C" void GRSPH_timestep_diagnostic(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // In this routine, we evaluate various timestep conditions.
  // We do this once per full timestep.
  //
  // Most of these calculations are based on intermediate data saved
  // during the RHS calculation.

  if(cctk_iteration < 1) return;
  
  // get actual timestep
  const CCTK_REAL dt_cactus = CCTK_DELTA_TIME;
  const CCTK_REAL tiny = 1.0e-10;

  // simplest: dt1 = 0.25 * min( h_i / c_s_i)
  // 0.25 is from Monaghan 1992
  CCTK_REAL dt1 = 10.0e100;  // Courant dt_cour
  CCTK_REAL dt2 = 10.0e100;  // viscous dt_visc

  // helper arrays
  CCTK_REAL *vcordx,*vcordy,*vcordz;
  vcordx = (CCTK_REAL*)malloc(*n_g_particles*sizeof(CCTK_REAL));
  vcordy = (CCTK_REAL*)malloc(*n_g_particles*sizeof(CCTK_REAL));
  vcordz = (CCTK_REAL*)malloc(*n_g_particles*sizeof(CCTK_REAL));

#pragma omp parallel 
  { 
#pragma omp for  
  for (ptrdiff_t i = 0; i < *n_g_particles; ++i) {

    vcordx[i] = alphap[i]*velx[i] - betaxp[i];
    vcordy[i] = alphap[i]*vely[i] - betayp[i];
    vcordz[i] = alphap[i]*velz[i] - betazp[i];
    
  } // end loop
#pragma omp for reduction(min : dt1), reduction(min : dt2)  
  for (ptrdiff_t i = 0; i < *n_g_particles; ++i) {

    const CCTK_REAL cs = sqrt(cs2[i]);

    dt1 = fmin(dt1, smoothl[i] / (cs+tiny) );

    // for dt2 = dt_visc, estimate using visc prescription of Price 08:
    // In this case, we'll just look at (v_i - v_j)*(r_i-r_j)/sqrt( (r_i - r_j)**2)
    // as a velocity.
    CCTK_REAL max_mu = 0.0;
    const auto &restrict neighbours = interactions.particles_neighbours[i];
    const auto &restrict js = *neighbours.js;
    for (ptrdiff_t jind = neighbours.jindmin; jind < neighbours.jindmax;
         ++jind) {
      const ptrdiff_t j = js[jind];

      const CCTK_REAL xij = posx[i] - posx[j];
      const CCTK_REAL yij = posy[i] - posy[j];
      const CCTK_REAL zij = posz[i] - posz[j];

      CCTK_REAL lowxij = gxxp[i]*xij + gxyp[i]*yij + gxzp[i]*zij;
      CCTK_REAL lowyij = gxyp[i]*xij + gyyp[i]*yij + gyzp[i]*zij;
      CCTK_REAL lowzij = gxzp[i]*xij + gyzp[i]*yij + gzzp[i]*zij;

      const CCTK_REAL vr_ij = lowxij * (vcordx[i]-vcordx[j])
        + lowyij * (vcordy[i]-vcordy[j])
        + lowzij * (vcordz[i]-vcordz[j]);

      const CCTK_REAL sqrtrij2= sqrt(lowxij*xij + lowyij*yij + lowzij*zij);

      max_mu = fmax(max_mu, 2.0 * vr_ij / sqrtrij2);
    }
    // using Monaghan 1992, section 10.3
    dt2 = fmin(dt2, smoothl[i] / (visca[i]*cs + 0.6 * max_mu + tiny));
      
  } // end loop
} // end pragma omp parallel

  // factor 0.25 from Monaghan 1992
  dt1 = dt1*0.25;
  dt2 = dt2*0.25;

  if(verbose) 
    CCTK_VInfo (CCTK_THORNSTRING,"dt_cactus = %13.6E, dt1 = %13.6E, dt2 = %13.6E",
		dt_cactus,dt1,dt2);

  if (dt1 <= dt_cactus || dt2 <= dt_cactus) {
    CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING,
	       "Timestep is too big! dt_cactus = %15.6E, dt1 = %15.6E, dt2 = %15.6E",
	       dt_cactus,dt1,dt2);
  }

  return;
} // end GRSPH_timestep_diagnostic


} // end namespace
