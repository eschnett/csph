#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <limits>


namespace GRSPH {
  using namespace std;

  void GRSPH_star_init(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    //define kernel dimension (3D)
    kerneldim[0] = 3.0;
    kerneldim[1] = 1.0 / M_PI;

    if(access( Profile_File, F_OK ) == -1) {
      CCTK_VWarn(0,__LINE__, __FILE__, CCTK_THORNSTRING,
		 "Input star profile file not present! %s",Profile_File);
    }

    // reading data
    FILE* infile = fopen(Profile_File,"r");
    char buf[1000];

    // get file length
    int i = 0;
    while (fgets(buf,1000, infile)!=NULL) {
      //    printf("%s",buf);
      i++;
    }
    fclose(infile);

    int n = i-2;
    //printf("%d\n",i);

    // allocate, reopen, read
    infile = fopen(Profile_File,"r");
    double srho[n],rad[n],mgrav[n],mbary[n],riso[n],salp[n],phi4[n];
    double K,gamma;
    // get K and  gamma
    fscanf(infile,"%lg %lg\n",&K,&gamma);
    //printf("%15.6E %15.6E\n",K,gamma);
    // ignore a line
    fgets(buf,1000, infile);
    //    fprintf(stderr,"%s\n",buf);
    for(int j=0;j<n;j++) {
      double dbuf;
      int ibuf;
      fscanf(infile,"%d %lg %lg %lg %lg %lg %lg %lg %lg\n",
	     &ibuf,&rad[j],&srho[j],&mgrav[j],&mbary[j],
	     &riso[j],&salp[j],&phi4[j],&dbuf);
      //      printf("%d %15.6E %15.6E \n",j,srho[j],rad[j]);
    }
    fclose(infile);

    // searching end of star data
    ptrdiff_t n_loop = 0;
    while (srho[n_loop] > 0.0) {
      n_loop++;
    }
    ptrdiff_t data_end = n_loop;

    // constant values
    const CCTK_REAL mass_data = mgrav[data_end-1];
    if (abs(log2(resolution_par)-(int)log2(resolution_par))
	>  numeric_limits<double>::epsilon())
      CCTK_ERROR("resolution_par is not power of two");


    const CCTK_REAL dr = rad[data_end-1] / resolution_par;
    const CCTK_REAL rho0 = 3.0 * mass_data / (4.0 * M_PI * pow(rad[data_end-1],3.0)) ;
    
    // set initital unit sphere , cubic lattice
    const CCTK_REAL xmin = -rad[data_end-1];
    const CCTK_REAL ymin = -rad[data_end-1];
    const CCTK_REAL zmin = -rad[data_end-1];
    CCTK_INT pline = 2 * (int)resolution_par + 1;

    ptrdiff_t ipart = -1;
    for (CCTK_INT i = 0; i < pline; ++i) {
      for (CCTK_INT j = 0; j < pline; ++j) {
	for (CCTK_INT k = 0; k < pline; ++k) {

	  if ( sqrt ( pow(xmin + i * dr, 2.0)
		      + pow(ymin + j * dr, 2.0)
		      + pow(zmin + k * dr, 2.0) ) 
	       <= rad[data_end-1] ) {
	  ipart++;
	  if (ipart > maxnparticles)
	    CCTK_ERROR("Too many particles; increase \"maxnparticles\"");

	  posx[ipart] = xmin + i * dr;
	  posy[ipart] = ymin + j * dr;
	  posz[ipart] = zmin + k * dr;

	  id[ipart] = i;
	  rho[ipart] = rho0;
	  
	  // all other settings
	  velx[ipart] = 0.0;
	  vely[ipart] = 0.0;
	  velz[ipart] = 0.0;
	  alphap[ipart] = 1.0;
	  sdetgp[ipart] = 1.0;
	  gxxp[ipart] = 1.0;
	  gxyp[ipart] = 0.0;
	  gxzp[ipart] = 0.0;
	  gyyp[ipart] = 1.0;
	  gyzp[ipart] = 0.0;
	  gzzp[ipart] = 1.0;
	  betaxp[ipart] = 0.0;
	  betayp[ipart] = 0.0;
	  betazp[ipart] = 0.0;
	  visca[ipart] = 1.0;
	  }

	}
      }
    }
    // particle mass
    *nparticles = ipart;
    CCTK_VInfo(CCTK_THORNSTRING, "Created %d particles", *nparticles);
    if (*nparticles > maxnparticles)
      CCTK_ERROR("Too many particles; increase \"maxnparticles\"");
    CCTK_REAL mass_p = mass_data / *nparticles;

    // calculating mapping
    const int N_pshell = data_end;
    const CCTK_REAL dr_shell = rad[1];
    CCTK_REAL *r_pshell, *rho_pshell;
    r_pshell = (CCTK_REAL*)malloc(N_pshell*sizeof(CCTK_REAL));
    rho_pshell = (CCTK_REAL*)malloc(N_pshell*sizeof(CCTK_REAL));
    r_pshell[0] = 0.0;
    rho_pshell[0] = srho[0];

    for (int n_shell = 1; n_shell < N_pshell; ++n_shell) {
      CCTK_REAL r_n_1 = (n_shell-1)*dr_shell;
      if (n_shell ==1) {
	// new radius of central particle with central density
	CCTK_REAL radc0 = pow(3.0 * mass_p / (4.0 * M_PI * srho[0]) , 1.0/3.0); 
	// radius of central particle with density0
	CCTK_REAL rad00 = pow(3.0 * mass_p / (4.0 * M_PI * rho0) , 1.0/3.0); 
	r_pshell[n_shell] = pow(rad00 / radc0 , 2.0) * dr_shell * rho0 / rho_pshell[n_shell-1];

	// interpolating rho_n
	int i_data = (int)(r_pshell[n_shell] / rad[1]);

	CCTK_REAL m_shell = (srho[i_data+1] - srho[i_data]) / rad[1]; 
	rho_pshell[n_shell] = m_shell * ( r_pshell[n_shell] - rad[i_data] ) 
	  + srho[i_data];
      }
      else {
	// calculating mapping
	CCTK_REAL dr_n = pow( r_n_1 / r_pshell[n_shell-1] ,2.0)
	  * rho0 * dr_shell / rho_pshell[n_shell-1];     
	r_pshell[n_shell] = r_pshell[n_shell-1] + dr_n;
	// interpolating rho_n
	int i_data = (int)(r_pshell[n_shell] / rad[1]);

	CCTK_REAL m_shell = (srho[i_data+1] - srho[i_data]) / rad[1]; 
	rho_pshell[n_shell] = m_shell * ( r_pshell[n_shell] - rad[i_data] ) 
	  + srho[i_data];
      } 
    }

    // mapping onto unit sphere
    for (ptrdiff_t i = 0; i < *nparticles; ++i) {
      CCTK_REAL r_part = sqrt ( posx[i]*posx[i]
				+ posy[i]*posy[i]
				+ posz[i]*posz[i]);
      CCTK_REAL r_part_new;

      // interpolating
      int i_data = (int)(r_part / rad[1]);
      //      printf("%d %d %15.6E %15.6E %15.6E %15.6E\n",i,i_data,posx[i],posy[i],posz[i],r_part);

      if (i_data >= N_pshell-1) {
        r_part_new = r_pshell[N_pshell-1];
	posx[i] = posx[i] * r_part_new / r_part;
	posy[i] = posy[i] * r_part_new / r_part;
	posz[i] = posz[i] * r_part_new / r_part;
        rho[i] = rho_pshell[N_pshell-1];
      }
      else if (r_part < numeric_limits<double>::epsilon()) { 
	rho[i] = srho[0];
      }
      else {
        CCTK_REAL m_shell = (r_pshell[i_data+1] - r_pshell[i_data]) / rad[1];
        r_part_new = r_pshell[i_data] + m_shell * (r_part - rad[i_data]);

	posx[i] = posx[i] * r_part_new / r_part;
	posy[i] = posy[i] * r_part_new / r_part;
	posz[i] = posz[i] * r_part_new / r_part;
	
        i_data = (int)(r_part_new / rad[1]);
        m_shell = (rho_pshell[i_data+1] - rho_pshell[i_data]) / rad[1];
        rho[i] = m_shell * ( r_part_new - rad[i_data] )
          + rho_pshell[i_data];
      }
      mass[i] = mass_p;      
      press[i] = K * pow(rho[i],gamma);
      eps[i] = press[i]/(eos_gamma - 1.0)/rho[i];
      smoothl[i] = eta_smooth* pow(mass[i]/rho[i], 1.0/3.0);


      // debug output
#if 0
      if( r_part_new > 4.51 && r_part_new < 4.512 ) {
	fprintf(stderr,"%8ld %15.6E %15.6E %15.6E %15.6E\n",
		i,r_part_new,rho[i],eps[i],press[i]);
      }
#endif

    }

    // now set up the Newtonian potential
    
    // Grid size                                                                                    
    const ptrdiff_t ni = cctk_lsh[0];
    const ptrdiff_t nj = cctk_lsh[1];
    const ptrdiff_t nk = cctk_lsh[2];

    // Initialize grid quantities                                                                     
    // first initialize the Newtonian variables
#pragma omp parallel for simd collapse(3)
    for (ptrdiff_t gk = 0; gk < nk; ++gk) {
      for (ptrdiff_t gj = 0; gj < nj; ++gj) {
	for (ptrdiff_t gi = 0; gi < ni; ++gi) {
	  ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
	  grid_NewtPot[gidx] = 0.0;
	  grid_NewtAccelx[gidx] = 0.0;
	  grid_NewtAccely[gidx] = 0.0;
	  grid_NewtAccelz[gidx] = 0.0;
	}
      }
    }

    // now interpolate from profile data
    // for testing, we are storing the Newtonian potential
    // in the phi4 profile variable
    CCTK_REAL inv_dr = 1.0/(rad[1]-rad[0]);

#pragma omp parallel for simd collapse(3)
    for (ptrdiff_t gk = 0; gk < nk; ++gk) {
      for (ptrdiff_t gj = 0; gj < nj; ++gj) {
        for (ptrdiff_t gi = 0; gi < ni; ++gi) {
	  ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);

	  int i_data = (int)(r[gidx] * inv_dr); 

	  //	  fprintf(stderr,"%d %15.6E %15.6E %15.6E %15.6E\n",i_data,inv_dr,r[gidx],rad[0],rad[1]);
	  CCTK_REAL m = (phi4[i_data+1] - phi4[i_data]) * inv_dr;
	  grid_NewtPot[gidx] = m*(r[gidx]-rad[i_data]) + phi4[i_data];

	}
      }
    }

    CCTK_VInfo(CCTK_THORNSTRING, "Done setting up star initial data!");

  } // function GRSPH_star_init
} // namespace GRSPH     
