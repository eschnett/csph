#ifndef CALCULATE_SHOCKTRIGGERCOEFFICIENT_HH
#define CALCULATE_SHOCKTRIGGERCOEFFICIENT_HH

#include <cctk.h>
#include <cctk_Parameters.h>
#include <cmath>

namespace GRSPH {

  static inline CCTK_REAL GRSPH_calculate_gradDivVel (const int* js,
						      const ptrdiff_t jindmin,
						      const ptrdiff_t jindmax,
						      const ptrdiff_t i,
						      const CCTK_REAL* posx,
						      const CCTK_REAL* posy,
						      const CCTK_REAL* posz,
						      const CCTK_REAL* mass,
						      const CCTK_REAL* div_vel,
						      const CCTK_REAL* gradh,
						      const CCTK_REAL* dens,
						      const CCTK_REAL  smoothl_i,
						      const CCTK_REAL* kerneldim,
						      const CCTK_REAL gxxp_i,
						      const CCTK_REAL gxyp_i,
						      const CCTK_REAL gxzp_i,
						      const CCTK_REAL gyyp_i,
						      const CCTK_REAL gyzp_i,
						      const CCTK_REAL gzzp_i) {
    CCTK_REAL graddivx = 0.0;CCTK_REAL graddivy = 0.0;CCTK_REAL graddivz = 0.0;
    // loop over neighbors
    for (ptrdiff_t jind = jindmin; jind < jindmax; ++jind){
      const ptrdiff_t j = js[jind];
      // Calculating Grad_Kernel function for smoothl(i)                                    
      CCTK_REAL gradWij_x, gradWij_y, gradWij_z;
      grad_M4kernel(posx[i] - posx[j],
		    posy[i] - posy[j],
		    posz[i] - posz[j],
		    gradWij_x, gradWij_y, gradWij_z,
		    smoothl_i,kerneldim[0],kerneldim[1]);
      graddivx += mass[j]/(gradh[i]*dens[j]) * div_vel[j]  * gradWij_x;
      graddivy += mass[j]/(gradh[i]*dens[j]) * div_vel[j]  * gradWij_y;
      graddivz += mass[j]/(gradh[i]*dens[j]) * div_vel[j]  * gradWij_z;

    }
    // Calculating absolute of grad*div*vel                                    
    
    CCTK_REAL lowgdx = gxxp_i*graddivx + gxyp_i*graddivy + gxzp_i*graddivz;
    CCTK_REAL lowgdy = gxyp_i*graddivx + gyyp_i*graddivy + gyzp_i*graddivz;
    CCTK_REAL lowgdz = gxzp_i*graddivx + gyzp_i*graddivy + gzzp_i*graddivz;
    return sqrt(lowgdx * graddivx +
		lowgdy * graddivy +
		lowgdz * graddivz);
  }


  static inline CCTK_REAL GRSPH_calculate_ShockCoeff (const CCTK_REAL gradDivVel,
						      const CCTK_REAL cs2_i,
						      const CCTK_REAL div_vel,
						      const CCTK_REAL smoothl_i) {
    //---------------------Shock triggers-----------------------------
    // Calculating shock triggers for artificial viscosity.                                 
    // Triggers are choosen by AV-template                                                  
    // avkey = 2: time dependent                                                            
    // avkey = 3: Read & Hayfield                                                           
    // avkey = 1 and = 0 do not need any trigger                                            
    // using shock-trigger by Read&Hayfield (2012)                                       
    // triggers on spatial change to detect convergence                                  
    // before it actually occurs, combining with                                   
    // time-dependent viscosity coefficient.                                      
    // no noise-trigger implemented yet                                          
    const CCTK_REAL sqcs2i = sqrt(cs2_i);
    const CCTK_REAL MaxCoeff = 1.5;
    const CCTK_REAL tau_coeff = 0.05;
    const CCTK_REAL Limiter = 1.0;
    CCTK_REAL Coeff_shock;
    if (div_vel < 0.0) {
      CCTK_REAL PreCoeff = Limiter * smoothl_i
	* smoothl_i * gradDivVel;
      Coeff_shock = MaxCoeff * PreCoeff /
	(PreCoeff + smoothl_i * abs(div_vel)
	 + tau_coeff * sqcs2i);
    }
    else {
      Coeff_shock = 0.0;
    }
    return Coeff_shock;

  }
}
#endif
