#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include <cmath>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#include "eos.hh" 
#include "prim2con_con2prim.hh"

using namespace std;

namespace GRSPH {

extern "C" void GRSPH_prim2con(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel for  
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {

    // call EOS for pressure
    press[i] = GRSPH_press(rho[i],eps[i]);

    // first lower velocity
    double vlowx = gxxp[i]*velx[i] + gxyp[i]*vely[i] + gxzp[i]*velz[i];
    double vlowy = gxyp[i]*velx[i] + gyyp[i]*vely[i] + gyzp[i]*velz[i];
    double vlowz = gxzp[i]*velx[i] + gyzp[i]*vely[i] + gzzp[i]*velz[i];
    
    // enthalpy
    double h = 1.0 + eps[i] + press[i]/rho[i];

    // Lorentz factor
    W[i] = 1.0 / sqrt(1.0 - vlowx*velx[i] - vlowy*vely[i] - vlowz*velz[i]);
    
    // conserved density
    dens[i] = sdetgp[i] * W[i] * rho[i];
    // conserved generalized momenta PER PARTICLE
    sx[i] = sdetgp[i] * rho[i] * h * W[i]*W[i] * vlowx / dens[i];
    sy[i] = sdetgp[i] * rho[i] * h * W[i]*W[i] * vlowy / dens[i];
    sz[i] = sdetgp[i] * rho[i] * h * W[i]*W[i] * vlowz / dens[i];
    // conserved generalized specific. energy PER PARTICLE
    tau[i] = sdetgp[i] * (rho[i] * h * W[i]*W[i] - press[i]) - dens[i];
    tau[i] *= 1.0/dens[i];

#if 1
    // example code how to find nans
    if( isnan(tau[i]) || isinf(tau[i]) ) {
      fprintf(stderr,"%d %15.6E %15.6E %15.6E %15.6E\n", i, posx[i],posy[i],posz[i],
	      tau[i]);
      abort();
    }
#endif

  }

}


static inline void con2prim_pt(const ptrdiff_t iparticle,
			       const CCTK_REAL posx,
			       const CCTK_REAL posy,
			       const CCTK_REAL posz,
			       const CCTK_REAL dens,
			       const CCTK_REAL sx,
			       const CCTK_REAL sy,
			       const CCTK_REAL sz,
			       const CCTK_REAL tau,
			       const CCTK_REAL uxx,
			       const CCTK_REAL uxy,
			       const CCTK_REAL uxz,
			       const CCTK_REAL uyy,
			       const CCTK_REAL uyz,
			       const CCTK_REAL uzz,
			       const CCTK_REAL sdetg,
			       CCTK_REAL &velx,
			       CCTK_REAL &vely,
			       CCTK_REAL &velz,
			       CCTK_REAL &rho,
			       CCTK_REAL &eps,
			       CCTK_REAL &press,
			       CCTK_REAL &W) {

  // minimum pressure constant; could have this as a parameter
  const CCTK_REAL pmin = 1.0e-40;
  // iteration relative tolerance
  const CCTK_REAL ptol = 1.0e-12;
  // do at least this many iterations
  const CCTK_INT  c2p_min_it = 1;
  // do not do more than this many iterations
  const CCTK_INT  c2p_max_it = 100;
  
  const CCTK_REAL invsdetg = 1.0/sdetg;
  const CCTK_REAL udens = dens*invsdetg;
  const CCTK_REAL usx = sx*udens;
  const CCTK_REAL usy = sy*udens;
  const CCTK_REAL usz = sz*udens;
  const CCTK_REAL utau = tau*udens;
  const CCTK_REAL s2 = usx*usx*uxx + usy*usy*uyy 
    + usz*usz*uzz + 2.0*usx*usy*uxy 
    + 2.0*usx*usz*uxz + 2.0*usy*usz*uyz;
  
  // helper vars
  CCTK_REAL f,df,pnew,stemp;
  CCTK_INT  count;
  

  // initial guess for pressure
  CCTK_REAL xpress = GRSPH_press(rho,eps);

  // This is the lowest admissible pressure, otherwise we are off
  // the physical regime Sometimes, we may end up with bad initial
  // guesses. In that case we need to start off with something
  // reasonable at least
  CCTK_REAL plow = MAX(pmin,sqrt(s2)-utau-udens);

  // Start out with some reasonable initial guess;
  CCTK_REAL pold = MAX(plow+1.0e-10,xpress);
    
  // Check that the variables have a chance of being physical
  CCTK_REAL utau_p_udens = utau+pold+udens;

  if (utau_p_udens*utau_p_udens - s2 <= 0.0) {
    CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING,
	       "C2P failure, particle: %d",(int)iparticle);
  }

  
  stemp = sqrt( utau_p_udens*utau_p_udens - s2 );
  rho = udens * stemp / (utau_p_udens);
  W = (utau_p_udens) / stemp;
  eps = (stemp - pold * W - udens) / udens;

#if 0
  // useful for debugging
  fprintf(stderr,"%18.9E %18.9E %18.9E %18.9E\n",utau,udens,utau_p_udens*utau_p_udens,s2);
  fprintf(stderr,"%18.9E %18.9E %18.9E %18.9E %18.9E\n",stemp,rho,W,eps,pold);
  abort();
#endif

  // calculate the function
  xpress = GRSPH_press(rho,eps);
  f = pold - xpress;
  
  count = 0;
  pnew = pold;

  // Newton-Raphson loop    
  while ( fabs(pnew-pold)/fabs(pnew) > ptol || count <= c2p_min_it) {

    count++;
    if(count > c2p_max_it) {
      CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING,
		 "C2P failure, particle: %d",(int)iparticle);
    }
    
    CCTK_REAL dpdrho = GRSPH_dpdrho(rho,eps);
    CCTK_REAL dpdeps = GRSPH_dpdeps(rho,eps);
    
    utau_p_udens = utau+udens+pnew;
    stemp = utau_p_udens*utau_p_udens - s2;
    CCTK_REAL drhobydpress = udens * s2 / 
      (sqrt(stemp)*(utau_p_udens*utau_p_udens));
    CCTK_REAL depsbydpress = pnew * s2 / 
      (rho * (utau_p_udens) * stemp);

    df = 1.0 - dpdrho * drhobydpress - 
      dpdeps * depsbydpress;
    
    pold = pnew;
    
    pnew = pold - f/df;
    
    pnew = MAX(pmin,pnew);
    
    utau_p_udens = utau+pnew+udens;
    stemp = utau_p_udens*utau_p_udens - s2;

    // check if we are still in the physical range
    if (stemp <= 0.0) {
      CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING,
		 "C2P failure, particle: %d",(int)iparticle);
    }
    
    // recalculate primitive variables

      
    rho = udens * sqrt(stemp) / utau_p_udens;
    W   = utau_p_udens / sqrt(stemp);
    eps = (sqrt(stemp) - pnew * W - udens)/udens;
    
    xpress = GRSPH_press(rho,eps);
    
    f = pnew - xpress;
  } // end while ...

  // calculate velocity
  press = pnew;
  
  CCTK_REAL xtmp = 1.0 / ((rho + rho*eps + press) * W * W);
  CCTK_REAL vlowx = usx * xtmp;
  CCTK_REAL vlowy = usy * xtmp;
  CCTK_REAL vlowz = usz * xtmp;
  velx = uxx * vlowx + uxy * vlowy + uxz * vlowz;
  vely = uxy * vlowx + uyy * vlowy + uyz * vlowz;
  velz = uxz * vlowx + uyz * vlowy + uzz * vlowz;

} // end con2prim_pt


extern "C" void GRSPH_con2prim(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  //  fprintf(stderr, "****************CP: %d\n", cctk_iteration);
  // if (cctk_iteration < 1) return;

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"Start Con2Prim");
 

#pragma omp parallel for  
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    
    // do something
    CCTK_REAL uxx,uxy,uxz,uyy,uyz,uzz;
    upper_metric(gxxp[i],gxyp[i],gxzp[i],gyyp[i],
		 gyzp[i],gzzp[i],sdetgp[i],&uxx,
		 &uxy,&uxz,&uyy,&uyz,&uzz);

    con2prim_pt(i,posx[i],posy[i],posz[i],dens[i],
		sx[i],sy[i],sz[i],tau[i],
		uxx,uxy,uxz,uyy,uyz,uzz,sdetgp[i],
		velx[i],vely[i],velz[i],rho[i],eps[i],
		press[i],W[i]);

  }

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"End Con2Prim");
  
}

extern "C" void GRSPH_con2prim_test(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // this routine calls the con2prim solver for
  // user specified test values
  // it is called at BASEGRID, but its call is normally
  // commented out in schedule.ccl

  int i = 0;
  gxxp[i] = 1.5;
  gxyp[i] = 0.0;
  gxzp[i] = 0.0;
  gyyp[i] = 1.5;
  gyzp[i] = 0.0;
  gzzp[i] = 1.5;
  sdetgp[i] = calc_sdetg(gxxp[i],gxyp[i],gxzp[i],
			 gyyp[i],gyzp[i],gzzp[i]);
	
  fprintf(stderr,"%15.6E\n",sdetgp[i]);

  rho[i] = 1.0e-4;
  const CCTK_REAL K = 100.0;
  press[i] = K*pow(rho[i],eos_gamma);
  eps[i] = press[i] / (eos_gamma-1.0) / rho[i];
  velx[i] = 0.45;
  vely[i] = 0.45;
  velz[i] = 0.45;

  // first lower velocity
  double vlowx = gxxp[i]*velx[i] + gxyp[i]*vely[i] + gxzp[i]*velz[i];
  double vlowy = gxyp[i]*velx[i] + gyyp[i]*vely[i] + gyzp[i]*velz[i];
  double vlowz = gxzp[i]*velx[i] + gyzp[i]*vely[i] + gzzp[i]*velz[i];
    
  // enthalpy
  double h = 1.0 + eps[i] + press[i]/rho[i];

  // Lorentz factor
  W[i] = 1.0 / sqrt(1.0 - vlowx*velx[i] - vlowy*vely[i] - vlowz*velz[i]);
    
  // conserved density
  dens[i] = sdetgp[i] * W[i] * rho[i];
  // conserved generalized momenta
  sx[i] = sdetgp[i] * rho[i] * h * W[i]*W[i] * vlowx;
  sy[i] = sdetgp[i] * rho[i] * h * W[i]*W[i] * vlowy;
  sz[i] = sdetgp[i] * rho[i] * h * W[i]*W[i] * vlowz;
  // conserved generalized spec. internal energy
  tau[i] = sdetgp[i] * (rho[i] * h * W[i]*W[i] - press[i]) - dens[i];

  fprintf(stderr,"%18.9E %18.9E %18.9E %18.9E\n",rho[i],eps[i],press[i],W[i]);

  // now perturb the vars:
  rho[i] = rho[i]*0.2;
  eps[i] = eps[i]*0.2;

  // do something
  CCTK_REAL uxx,uxy,uxz,uyy,uyz,uzz;
  upper_metric(gxxp[i],gxyp[i],gxzp[i],gyyp[i],
	       gyzp[i],gzzp[i],sdetgp[i],&uxx,
	       &uxy,&uxz,&uyy,&uyz,&uzz);

  con2prim_pt(i,posx[i],posy[i],posz[i],dens[i],
	      sx[i],sy[i],sz[i],tau[i],
	      uxx,uxy,uxz,uyy,uyz,uzz,sdetgp[i],
	      velx[i],vely[i],velz[i],rho[i],eps[i],
	      press[i],W[i]);

  fprintf(stderr,"%18.9E %18.9E %18.9E %18.9E\n",rho[i],eps[i],press[i],W[i]);

  CCTK_WARN(0,"Done testing c2p!)");

}

				 



}
