//-----------------------------------------------------------------
// Initial setup for hydrodynamical sedovblast blast test
// 
// Set high thermal energy (defined in par file, resolution_par)
// in small core of a hexagonal lattice with homogenous density.
// Core energy is modified by a distribution function to avoid
// C2P errors because of missing energy gradient between inner 
// particles.
// Use particle_res,particle_dist and pos_min for setup the lattice
// resoultion and size.
//
// 3D has cubic lattice yet.
//
//-----------------------------------------------------------------


#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include "eos.hh"
#include "kernel.hh"

namespace GRSPH {
  using namespace std;

  void GRSPH_setup_sedovblast2D(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    const CCTK_REAL pos_min = -1.0;
    const CCTK_REAL particle_dist = 0.0025;
    const CCTK_INT  particle_res = 400;
    const CCTK_REAL core_size = 0.004;
   
    //define kernel dimension (2D)                                             
    kerneldim[0] = 2.0;
    kerneldim[1] = 10.0/(7.0*M_PI);

    //define boundary values                                                    
    boundaryval[0] =  pos_min;
    boundaryval[1] = -pos_min - particle_dist ;
    boundaryval[2] =  pos_min;
    boundaryval[3] = -pos_min - particle_dist ;

    *nparticles = (particle_res) * (2 * particle_res);
    CCTK_REAL Center_eps = 0.0;
    if(*nparticles > maxnparticles) {
      CCTK_ERROR("maxnparticles is too low");
    }

    ptrdiff_t ipart = 0;
    for (CCTK_INT i = 0; i < particle_res; ++i) {
      for (CCTK_INT j = 0; j < (2 * particle_res); ++j) {
	
	// particle lattice setup
	posx[ipart] = pos_min + i * 2.0 * particle_dist;
	posy[ipart] = pos_min + j * 1.0 * particle_dist;
	posz[ipart] = 0.0;
	// additional for hexagonal lattice
	if (j%2 > 0)
	  posx[ipart] += particle_dist;
	
	// sedov blast initial energy
	// inner core, high energy
	if (sqrt(posx[ipart] * posx[ipart]
		 + posy[ipart] * posy[ipart])
	    <= core_size ) {
	  eps[ipart] = 100.0 * pow(6.0*particle_dist,kerneldim[0])
	    * M4kernel(posx[ipart],posy[ipart],posz[ipart],
		       core_size,kerneldim[0],kerneldim[1]);
	  Center_eps += eps[ipart];
	// outside of the core, low energy 
	} else {
	  eps[ipart] = 0.00025;
	}
	
	// all other settings	  
	id[ipart] = ipart;
	mass[ipart] = 4.0 / (particle_res * 2 * particle_res);
	rho[ipart] = 1.0;
	press[ipart] = GRSPH_press(rho[ipart],eps[ipart]);
	
	velx[ipart] = 0.0;
	vely[ipart] = 0.0;
	velz[ipart] = 0.0;
	alphap[ipart] = 1.0;
	sdetgp[ipart] = 1.0;
	gxxp[ipart] = 1.0;
	gxyp[ipart] = 0.0;
	gxzp[ipart] = 0.0;
	gyyp[ipart] = 1.0;
	gyzp[ipart] = 0.0;
	gzzp[ipart] = 1.0;
	betaxp[ipart] = 0.0;
	betayp[ipart] = 0.0;
	betazp[ipart] = 0.0;
	smoothl[ipart] = eta_smooth * pow(mass[ipart]/rho[ipart],1.0/2.0);
	visca[ipart] = 1.0;
	
	ipart++;
      }
    }

    CCTK_REAL epsresize = resolution_par / Center_eps;
    Center_eps = 0.0;
    ipart = 0;
    for (CCTK_INT i = 0; i < particle_res; ++i) {
      for (CCTK_INT j = 0; j < 2*particle_res; ++j) {
	if (sqrt(posx[ipart] * posx[ipart]
		 + posy[ipart] * posy[ipart])
	    <= core_size ) {
	  eps[ipart] *= epsresize;
	  press[ipart] = GRSPH_press(rho[ipart],eps[ipart]);
	  Center_eps += eps[ipart];
	}
	ipart++;
      }
    }

CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
CCTK_VInfo(CCTK_THORNSTRING, "Set center energy: %g", double(Center_eps));


  } // function GRSPH_setup_sedovblast2D                    

  void GRSPH_setup_sedovblast3D(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    const CCTK_REAL pos_min = -1.0;
    const CCTK_REAL particle_dist = 0.02;
    const CCTK_INT  particle_res = 50;
    const CCTK_REAL core_size = 0.0875;
   
    //define kernel dimension (3D)                                             
    kerneldim[0] = 3.0;
    kerneldim[1] = 1.0 / M_PI;

    //define boundary values                                                    
    boundaryval[0] =  pos_min;
    boundaryval[1] = -pos_min - particle_dist ;
    boundaryval[2] =  pos_min;
    boundaryval[3] = -pos_min - particle_dist ;
    boundaryval[4] =  pos_min;
    boundaryval[5] = -pos_min - particle_dist;

    *nparticles = pow(particle_res,3);
    CCTK_REAL Center_eps = 0.0;
    if(*nparticles > maxnparticles) {
      CCTK_ERROR("maxnparticles is too low");
    }

    ptrdiff_t ipart = 0;
    for (CCTK_INT i = 0; i < particle_res; ++i) {
      for (CCTK_INT j = 0; j < particle_res; ++j) {
	for (CCTK_INT k = 0; k < particle_res; ++k) {

	  // particle lattice setup
	  posx[ipart] = pos_min + i * 2.0 * particle_dist;
	  posy[ipart] = pos_min + j * 2.0 * particle_dist;
	  posz[ipart] = pos_min + k * 2.0 * particle_dist;
	  // additional for hexagonal lattice
	  //if (j%2 > 0)
	  //  posx[ipart] += particle_dist;

	  // sedov blast initial energy
	  // inner core, high energy
	  if (sqrt(posx[ipart] * posx[ipart]
		   + posy[ipart] * posy[ipart]
		   + posz[ipart] * posz[ipart])
	      <= core_size ) {
	    eps[ipart] = 100.0 * pow(6.0*particle_dist,kerneldim[0])
	      * M4kernel(posx[ipart],posy[ipart],posz[ipart],
			 core_size,kerneldim[0],kerneldim[1]);
	    Center_eps += eps[ipart];
	    // outside of the core, low energy 
	  } else {
	    eps[ipart] = 0.00025;
	  }

	  // all other settings	  
	  id[ipart] = ipart;
	  mass[ipart] = 1.0 / pow(particle_res,3.0);
	  rho[ipart] = 1.0;
	  press[ipart] = GRSPH_press(rho[ipart],eps[ipart]);
	  
	  velx[ipart] = 0.0;
	  vely[ipart] = 0.0;
	  velz[ipart] = 0.0;
	  alphap[ipart] = 1.0;
	  sdetgp[ipart] = 1.0;
	  gxxp[ipart] = 1.0;
	  gxyp[ipart] = 0.0;
	  gxzp[ipart] = 0.0;
	  gyyp[ipart] = 1.0;
	  gyzp[ipart] = 0.0;
	  gzzp[ipart] = 1.0;
	  betaxp[ipart] = 0.0;
	  betayp[ipart] = 0.0;
	  betazp[ipart] = 0.0;
	  smoothl[ipart] = eta_smooth * pow(mass[ipart]/rho[ipart],1.0/3.0);
	  visca[ipart] = 1.0;

	  ipart++;
	}
      }
    }
    CCTK_REAL epsresize = resolution_par / Center_eps;
    Center_eps = 0.0;
    ipart = 0;
    for (CCTK_INT i = 0; i < particle_res; ++i) {
      for (CCTK_INT j = 0; j < particle_res; ++j) {
	for (CCTK_INT k = 0; k < particle_res; ++k) {
	  
	  if (sqrt(posx[ipart] * posx[ipart]
		   + posy[ipart] * posy[ipart]
		   + posz[ipart] * posz[ipart])
	      <= core_size ) {
	    eps[ipart] *= epsresize;
	    press[ipart] = GRSPH_press(rho[ipart],eps[ipart]);
	    Center_eps += eps[ipart];
	  }
	  ipart++;
	}
      }
    }
    
CCTK_VInfo(CCTK_THORNSTRING, "Created %g particles", double(*nparticles));
CCTK_VInfo(CCTK_THORNSTRING, "Set center energy: %g", double(Center_eps));


  } // function GRSPH_setup_sedovblast3D    
}// namespace GRSPH                                                           

