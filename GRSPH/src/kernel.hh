#ifndef KERNEL_HH
#define KERNEL_HH

#include <cctk.h>
#include <cctk_Parameters.h>

#include <cmath>

namespace GRSPH {
using namespace std;

// Conditions on the kernel:
//    symmetric:       W(x^i) = W(-x^i)
//    normalized:      Int W(x) = 1
//    compact support: W(x^i) = 0 for |x|>=h

// Definition in Mathematica:
//    w[h_, r_] = (12/Pi) BSplineBasis[3, r/(2 h) + 1/2] / h^3
  inline CCTK_REAL kernel(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL smoothl_i) {
    DECLARE_CCTK_PARAMETERS;
    CCTK_REAL r2 = pow(x, 2) + pow(y, 2) + pow(z, 2);
    if (r2 >= pow(smoothl_i, 2))
      return 0.0;
    CCTK_REAL r = sqrt(r2);
    CCTK_REAL q = r / smoothl_i;
    CCTK_REAL w;
    if (q < 0.5)
      w = 8.0 * (1.0 - 6.0 * pow(q, 2) + 6.0 * pow(q, 3));
    else
      w = 16.0 * (1.0 - 3.0 * q + 3.0 * pow(q, 2) - pow(q, 3));
    w /= M_PI * pow(smoothl_i, 3);
  return w;
  }
  inline void grad_kernel(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL &wx,
			  CCTK_REAL &wy, CCTK_REAL &wz, CCTK_REAL smoothl_i) {
    DECLARE_CCTK_PARAMETERS;
    CCTK_REAL r2 = pow(x, 2) + pow(y, 2) + pow(z, 2);
    if (r2 >= pow(smoothl_i, 2)) {
      wx = wy = wz = 0.0;
      return;
    }
    CCTK_REAL r = sqrt(r2);
    CCTK_REAL q = r / smoothl_i;
    CCTK_REAL wr;
    if (q < 0.5)
      wr = -48.0 * (2.0 * q - 3.0 * pow(q, 2));
    else
      wr = -48.0 * (1.0 - 2.0 * q + pow(q, 2));
    wr /= M_PI * pow(smoothl_i, 4);
    wx = wr * x / r;
    wy = wr * y / r;
    wz = wr * z / r;
  }

// B-spline kernel function by Monaghan and Lattanzio, 1985
// and Monaghan, 1985, 2005
// 3-D M4 (cubic) spline:
//   compact support: W(x^i) = 0 for |x|> 2h
  inline CCTK_REAL M4kernel(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL smoothl_i, CCTK_REAL dim, CCTK_REAL coeff) {
    DECLARE_CCTK_PARAMETERS;
    CCTK_REAL r2 = x*x + y*y + z*z;
    if (r2 >= pow(2.0 * smoothl_i, 2))
      return 0.0;
    CCTK_REAL r = sqrt(r2);
    CCTK_REAL q = r / smoothl_i;
    CCTK_REAL w;
    if (q < 1.0)
      w = 0.25 * pow(2 - q ,3)-pow(1 -q ,3);
    else
      w = 0.25 * pow(2 - q ,3);
    w = w * coeff / pow(smoothl_i,dim);
    return w;
  }
  // Gradient of M4 kernel function
  inline void grad_M4kernel(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL &wx,
			    CCTK_REAL &wy, CCTK_REAL &wz, CCTK_REAL smoothl_i, CCTK_REAL dim, CCTK_REAL coeff) {


    DECLARE_CCTK_PARAMETERS;
    CCTK_REAL r2 = x*x + y*y + z*z;
    if (r2 >= pow(2.0 * smoothl_i, 2)) {
      wx = wy = wz = 0.0;
      return;
    }
    CCTK_REAL r = sqrt(r2);
    CCTK_REAL q = r / smoothl_i;
    CCTK_REAL wr;
    if (q < 1.0)
      wr = 9.0/4.0 * q*q - 3.0 * q;
    else
      wr = -3.0/4.0 * (2.0 - q) * (2.0 - q) ;
    wr = wr * coeff / pow(smoothl_i,dim + 1.0);
    wx = wr * x / r;
    wy = wr * y / r;
    wz = wr * z / r;
  }



}



/*
// B-spline kernel function by Monaghan and Lattanzio, 1985                                        
// and Monaghan, 1985, 2005                                                                        
// 1-D M4 (cubic) spline:                                                                           
//   compact support: W(x^i) = 0 for |x|> 2h                                                        
  inline CCTK_REAL M4kernel1D(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL smoothl_i) {         
    DECLARE_CCTK_PARAMETERS;                                                                        
    CCTK_REAL r2 = x*x + y*y + z*z;                                                                 
    if (r2 >= pow(2.0 * smoothl_i, 2))                                                              
      return 0.0;                                                                                   
    CCTK_REAL r = sqrt(r2);                                                                         
    CCTK_REAL q = r / smoothl_i;                                                                    
    CCTK_REAL w;                                                                                    
    if (q < 1.0)                                                                                    
      w = 0.25 * pow(2 - q ,3)-pow(1 -q ,3);                                                        
    else                                                                                            
      w = 0.25 * pow(2 - q ,3);                                                                     
    w /=  3.0 * smoothl_i / 2.0;                                                                    
    return w;                                                                                       
  }                                                                                                 
  // Gradient of M4 kernel function                                                                 
  inline void grad_M4kernel1D(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL &wx,                 
                            CCTK_REAL &wy, CCTK_REAL &wz, CCTK_REAL smoothl_i) {                    
    DECLARE_CCTK_PARAMETERS;                                                                        
    CCTK_REAL r2 = x*x + y*y + z*z;                                                                 
    if (r2 >= pow(2.0 * smoothl_i, 2)) {                                                            
      wx = wy = wz = 0.0;                                                                           
      return;                                                                                       
    }                                                                                               
    CCTK_REAL r = sqrt(r2);                                                                          
    CCTK_REAL q = r / smoothl_i;                                                                    
    CCTK_REAL wr;                                                                                   
    if (q < 1.0)                                                                                    
      wr = 9.0/4.0 * q*q - 3.0 * q;                                                                
    else                                                                                            
      wr = -3.0/4.0 * (2.0 - q) * (2.0 - q) ;                                                       
    wr /= 3.0 * smoothl_i * smoothl_i / 2.0;                                                        
    wx = wr * x / r;                                                                                
    wy = wr * y / r;                                                                                
    wz = wr * z / r;
 */



#endif // #ifndef KERNEL_HH


