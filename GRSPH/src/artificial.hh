#ifndef ARTIFICIAL_HH
#define ARTIFICIAL_HH

#include <cctk.h>
#include <cctk_Parameters.h>

#include <cmath>

namespace GRSPH {

  //----------------Artificial Viscosity-------------------
  //               by Rosswog, 2010/2014
  //  (SPH Methods in the Modelling of Compact Objects)
  //  av_rela is a artificial viscosity term for special
  //  relativistic problems.
  //  Using dissipative terms (Chow and Monaghan,1997)
  //  and an on the connection-line projected Lorentz factor 
  //  for the calculation of conserved momentum and energy.
  //  Also using signal velocity calculated by local
  //  eigenvalues of Euler equations (Marti and Mueller,2003)
  static inline CCTK_REAL av_rela_mom(const CCTK_REAL rho_a,
				      const CCTK_REAL rho_b,
				      const CCTK_REAL press_a,
				      const CCTK_REAL press_b,
				      const CCTK_REAL eps_a,
				      const CCTK_REAL eps_b,
				      const CCTK_REAL cs_a,
				      const CCTK_REAL cs_b,
				      const CCTK_REAL smooth_a,
				      const CCTK_REAL smooth_b,
				      const CCTK_REAL xij,
				      const CCTK_REAL yij,
				      const CCTK_REAL zij,
				      const CCTK_REAL lowxij,
				      const CCTK_REAL lowyij,
				      const CCTK_REAL lowzij,
				      const CCTK_REAL vr_ij,
				      const CCTK_REAL detg_a,
				      const CCTK_REAL detg_b,
				      const CCTK_REAL visca_a,
				      const CCTK_REAL visca_b) {
    CCTK_REAL momvisc = 0.0;
    return momvisc;
  }
  static inline CCTK_REAL av_rela_tau(const CCTK_REAL rho_a,
				      const CCTK_REAL rho_b,
				      const CCTK_REAL press_a,
				      const CCTK_REAL press_b,
				      const CCTK_REAL eps_a,
				      const CCTK_REAL eps_b,
				      const CCTK_REAL cs_a,
				      const CCTK_REAL cs_b,
				      const CCTK_REAL smooth_a,
				      const CCTK_REAL smooth_b,
				      const CCTK_REAL xij,
				      const CCTK_REAL yij,
				      const CCTK_REAL zij,
				      const CCTK_REAL lowxij,
				      const CCTK_REAL lowyij,
				      const CCTK_REAL lowzij,
				      const CCTK_REAL vr_ij,
				      const CCTK_REAL detg_a,
				      const CCTK_REAL detg_b,
				      const CCTK_REAL velx_a,
				      const CCTK_REAL vely_a,
				      const CCTK_REAL velz_a,
				      const CCTK_REAL velx_b,
				      const CCTK_REAL vely_b,
				      const CCTK_REAL velz_b,
				      const CCTK_REAL MeanWij_x,
				      const CCTK_REAL MeanWij_y,
				      const CCTK_REAL MeanWij_z,
				      const CCTK_REAL visca_a,
				      const CCTK_REAL visca_b) {
    
    CCTK_REAL tauvisc = 0.0;
    return tauvisc;
  }









  // std: Artificial viscosity terms for momentum and energy
  //      by Monaghan and Gingold, 1983
  //      (Shock simulation by the particle method SPH) 
  static inline CCTK_REAL av_std_mom(const CCTK_REAL rho_a,
				     const CCTK_REAL rho_b,
				     const CCTK_REAL press_a,
				     const CCTK_REAL press_b,
				     const CCTK_REAL eps_a,
				     const CCTK_REAL eps_b,
				     const CCTK_REAL cs_a,
				     const CCTK_REAL cs_b,
				     const CCTK_REAL smooth_a,
				     const CCTK_REAL smooth_b,
				     const CCTK_REAL xij,
				     const CCTK_REAL yij,
				     const CCTK_REAL zij,
				     const CCTK_REAL vr_ij,
				     const CCTK_REAL detg_a,
				     const CCTK_REAL detg_b) {
    
    DECLARE_CCTK_PARAMETERS;       // see Siegler,Riffert (2000)
    const CCTK_REAL alpha = 1.0;   //fixed viscosity coefficient
    const CCTK_REAL beta = 2.0;    // beta = 2* alpha
    const CCTK_REAL epsilon = 0.1; 

    CCTK_REAL visc_a = 0.0;
    CCTK_REAL visc_b = 0.0;
    CCTK_REAL momvisc = 0.0;
    const CCTK_REAL h_a = 1.0 + eps_a + press_a/rho_a;
    const CCTK_REAL h_b = 1.0 + eps_b + press_b/rho_b;
    const CCTK_REAL hab2 = pow( 0.5*(smooth_a+smooth_b), 2 );
    const CCTK_REAL rab2 = xij*xij + yij*yij + zij*zij;
    const CCTK_REAL divv = vr_ij / (rab2 + epsilon * hab2);
    
    if (divv < 0.0) {
      visc_a = rho_a * h_a * (-alpha*cs_a*smooth_a*divv + 
			      beta*smooth_a*smooth_a*divv*divv);
      visc_b = rho_b * h_b * (-alpha*cs_b*smooth_b*divv + 
			      beta*smooth_b*smooth_b*divv*divv);

      momvisc = detg_a * visc_a / (rho_a*rho_a) + detg_b * visc_b / (rho_b*rho_b);
    } else {
      momvisc = 0.0;
    }

    return momvisc;
  }
  static inline CCTK_REAL av_std_tau(const CCTK_REAL rho_a,
				     const CCTK_REAL rho_b,
				     const CCTK_REAL press_a,
				     const CCTK_REAL press_b,
				     const CCTK_REAL eps_a,
				     const CCTK_REAL eps_b,
				     const CCTK_REAL cs_a,
				     const CCTK_REAL cs_b,
				     const CCTK_REAL smooth_a,
				     const CCTK_REAL smooth_b,
				     const CCTK_REAL xij,
				     const CCTK_REAL yij,
				     const CCTK_REAL zij,
				     const CCTK_REAL vr_ij,
				     const CCTK_REAL detg_a,
				     const CCTK_REAL detg_b,
				     const CCTK_REAL velx_a,
				     const CCTK_REAL vely_a,
				     const CCTK_REAL velz_a,
				     const CCTK_REAL velx_b,
				     const CCTK_REAL vely_b,
				     const CCTK_REAL velz_b,
				     const CCTK_REAL MeanWij_x,
				     const CCTK_REAL MeanWij_y,
				     const CCTK_REAL MeanWij_z) {
    
    DECLARE_CCTK_PARAMETERS;       // see Siegler,Riffert (2000)
    const CCTK_REAL alpha = 1.0;   //fixed viscosity coefficient
    const CCTK_REAL beta = 2.0;    // beta = 2* alpha
    const CCTK_REAL epsilon = 0.1; 

    CCTK_REAL visc_a = 0.0;
    CCTK_REAL visc_b = 0.0;
    CCTK_REAL tauvisc = 0.0;
    const CCTK_REAL h_a = 1.0 + eps_a + press_a/rho_a;
    const CCTK_REAL h_b = 1.0 + eps_b + press_b/rho_b;
    const CCTK_REAL hab2 = pow( 0.5*(smooth_a+smooth_b), 2 );
    const CCTK_REAL rab2 = xij*xij + yij*yij + zij*zij;
    const CCTK_REAL divv = vr_ij / (rab2 + epsilon * hab2);
    
    if (divv < 0.0) {
      visc_a = rho_a * h_a * (-alpha*cs_a*smooth_a*divv + 
			      beta*smooth_a*smooth_a*divv*divv);
      visc_b = rho_b * h_b * (-alpha*cs_b*smooth_b*divv + 
			      beta*smooth_b*smooth_b*divv*divv);
      CCTK_REAL tausource_a = detg_a * visc_a / (rho_a*rho_a);
      CCTK_REAL tausource_b = detg_b * visc_b / (rho_b*rho_b);   
      tauvisc = (tausource_a * velx_b + tausource_b * velx_a) * MeanWij_x
	+ (tausource_a * vely_b + tausource_b * vely_a) * MeanWij_y
	+ (tausource_a * velz_b + tausource_b * velz_a) * MeanWij_z;
    } else {
      tauvisc = 0.0;
    }

    return tauvisc;
  }



  // kh: Arttificial viscosity terms for momentum and energy
  //     by Monaghan, 1997 (SPH and Riemann Solvers)
  static inline CCTK_REAL av_kh_mom(const CCTK_REAL rho_a,
				    const CCTK_REAL rho_b,
				    const CCTK_REAL press_a,
				    const CCTK_REAL press_b,
				    const CCTK_REAL eps_a,
				    const CCTK_REAL eps_b,
				    const CCTK_REAL cs_a,
				    const CCTK_REAL cs_b,
				    const CCTK_REAL smooth_a,
				    const CCTK_REAL smooth_b,
				    const CCTK_REAL xij,
				    const CCTK_REAL yij,
				    const CCTK_REAL zij,
				    const CCTK_REAL lowxij,
				    const CCTK_REAL lowyij,
				    const CCTK_REAL lowzij,
				    const CCTK_REAL vr_ij,
				    const CCTK_REAL detg_a,
				    const CCTK_REAL detg_b,
				    const CCTK_REAL visca_a,
				    const CCTK_REAL visca_b) {

    
    DECLARE_CCTK_PARAMETERS; // see Price, 2008
    CCTK_REAL momvisc = 0.0;
    const CCTK_REAL ceta = 2.0;

    const CCTK_REAL sqrij2= sqrt(lowxij*xij + lowyij*yij + lowzij*zij);
    const CCTK_REAL invdens_ij = 2.0 / (rho_a + rho_b);
    const CCTK_REAL vsig_ij = 0.5 * (cs_a + cs_b - ceta * vr_ij / sqrij2);

    momvisc = - 0.5 * (visca_a+visca_b) * vsig_ij * vr_ij * invdens_ij / sqrij2;
    return momvisc;
  }
  static inline CCTK_REAL av_kh_tau(const CCTK_REAL rho_a,
				    const CCTK_REAL rho_b,
				    const CCTK_REAL press_a,
				    const CCTK_REAL press_b,
				    const CCTK_REAL eps_a,
				    const CCTK_REAL eps_b,
				    const CCTK_REAL cs_a,
				    const CCTK_REAL cs_b,
				    const CCTK_REAL smooth_a,
				    const CCTK_REAL smooth_b,
				    const CCTK_REAL xij,
				    const CCTK_REAL yij,
				    const CCTK_REAL zij,
				    const CCTK_REAL lowxij,
				    const CCTK_REAL lowyij,
				    const CCTK_REAL lowzij,
				    const CCTK_REAL vr_ij,
				    const CCTK_REAL detg_a,
				    const CCTK_REAL detg_b,
				    const CCTK_REAL velx_a,
				    const CCTK_REAL vely_a,
				    const CCTK_REAL velz_a,
				    const CCTK_REAL velx_b,
				    const CCTK_REAL vely_b,
				    const CCTK_REAL velz_b,
				    const CCTK_REAL MeanWij_x,
				    const CCTK_REAL MeanWij_y,
				    const CCTK_REAL MeanWij_z,
                                    const CCTK_REAL visca_a,
                                    const CCTK_REAL visca_b) {
    
    DECLARE_CCTK_PARAMETERS; // see Price, 2008
    const CCTK_REAL ceta = 2.0;

    const CCTK_REAL invdens_ij = 2.0 / (rho_a + rho_b);
    const CCTK_REAL sqrij2= sqrt(lowxij*xij + lowyij*yij + lowzij*zij);
    const CCTK_REAL vsig_ij = 0.5 * (cs_a + cs_b - ceta * vr_ij / sqrij2);
    const CCTK_REAL vsigtau_ij = sqrt( sqrt( (press_a-press_b) * (press_a-press_b) ) / invdens_ij);

    const CCTK_REAL e_i = vsigtau_ij * eps_a
      + 0.5 * vsig_ij *  pow((lowxij * velx_a
			      + lowyij * vely_a
			      + lowzij * velz_a) / sqrij2 , 2);
    const CCTK_REAL e_j = vsigtau_ij * eps_b
      + 0.5 * vsig_ij *  pow((lowxij * velx_b
			      + lowyij * vely_b
			      + lowzij * velz_b) / sqrij2 , 2);
    const CCTK_REAL e_ij = e_i - e_j;
    const CCTK_REAL taukoeff = -0.5 * (visca_a+visca_b) * e_ij * invdens_ij / sqrij2;
    
    CCTK_REAL tauvisc = 0.0;
    tauvisc = taukoeff * xij * MeanWij_x +
      taukoeff * yij * MeanWij_y +
      taukoeff * zij * MeanWij_z;

    return tauvisc;
  }

  // relativistic: 
  static inline CCTK_REAL av_relativistic(const CCTK_REAL rho_a,
					      const CCTK_REAL rho_b,
					      const CCTK_REAL press_a,
					      const CCTK_REAL press_b,
					      const CCTK_REAL eps_a,
					      const CCTK_REAL eps_b,
					      const CCTK_REAL cs_a,
					      const CCTK_REAL cs_b,
					      const CCTK_REAL xij,
					      const CCTK_REAL yij,
					      const CCTK_REAL zij,
					      const CCTK_REAL velx_a,
					      const CCTK_REAL vely_a,
					      const CCTK_REAL velz_a,
					      const CCTK_REAL velx_b,
					      const CCTK_REAL vely_b,
					      const CCTK_REAL velz_b,
					      const CCTK_REAL gxxp_a,
					      const CCTK_REAL gxyp_a,
					      const CCTK_REAL gxzp_a,
					      const CCTK_REAL gyyp_a,
					      const CCTK_REAL gyzp_a,
					      const CCTK_REAL gzzp_a,
					      const CCTK_REAL gxxp_b,
					      const CCTK_REAL gxyp_b,
					      const CCTK_REAL gxzp_b,
					      const CCTK_REAL gyyp_b,
					      const CCTK_REAL gyzp_b,
					      const CCTK_REAL gzzp_b,
					      const CCTK_REAL visca_a,
					      const CCTK_REAL visca_b,
					      const CCTK_REAL MeanWij_x,
					      const CCTK_REAL MeanWij_y,
					      const CCTK_REAL MeanWij_z,
					      const bool mom) {

    
    DECLARE_CCTK_PARAMETERS;

    CCTK_REAL lowxij = gxxp_a*xij + gxyp_a*yij + gxzp_a*zij;
    CCTK_REAL lowyij = gxyp_a*xij + gyyp_a*yij + gyzp_a*zij;
    CCTK_REAL lowzij = gxzp_a*xij + gyzp_a*yij + gzzp_a*zij;
    const CCTK_REAL alpha_ab = 0.5*(visca_a+visca_b);
    const CCTK_REAL invrho_ab = 1/ (0.5*(rho_a+rho_b));
    const CCTK_REAL invsqxab2 = 1 / sqrt(lowxij*xij + lowyij*yij + lowzij*zij);

    const CCTK_REAL eab_x = xij * invsqxab2;
    const CCTK_REAL eab_y = yij * invsqxab2;
    const CCTK_REAL eab_z = zij * invsqxab2;

    CCTK_REAL lowvelx_a = gxxp_a*velx_a + gxyp_a*vely_a + gxzp_a*velz_a;
    CCTK_REAL lowvely_a = gxyp_a*velx_a + gyyp_a*vely_a + gyzp_a*velz_a;
    CCTK_REAL lowvelz_a = gxzp_a*velx_a + gyzp_a*vely_a + gzzp_a*velz_a;
    CCTK_REAL lowvelx_b = gxxp_b*velx_b + gxyp_b*vely_b + gxzp_b*velz_b;
    CCTK_REAL lowvely_b = gxyp_b*velx_b + gyyp_b*vely_b + gyzp_b*velz_b;
    CCTK_REAL lowvelz_b = gxzp_b*velx_b + gyzp_b*vely_b + gzzp_b*velz_b;


    const CCTK_REAL vele_a = lowvelx_a * eab_x + lowvely_a * eab_y + lowvelz_a * eab_z;
    const CCTK_REAL velparallel_ax = vele_a * eab_x;
    const CCTK_REAL velparallel_ay = vele_a * eab_y;
    const CCTK_REAL velparallel_az = vele_a * eab_z;
    const CCTK_REAL lowvelparallel_ax = gxxp_a*velparallel_ax + gxyp_a*velparallel_ay 
      + gxzp_a*velparallel_az;
    const CCTK_REAL lowvelparallel_ay = gxyp_a*velparallel_ax + gyyp_a*velparallel_ay 
      + gyzp_a*velparallel_az;
    const CCTK_REAL lowvelparallel_az = gxzp_a*velparallel_ax + gyzp_a*velparallel_ay 
      + gzzp_a*velparallel_az;
    const CCTK_REAL velparallel2_a = lowvelparallel_ax * velparallel_ax 
      + lowvelparallel_ay * velparallel_ay + lowvelparallel_az * velparallel_az;

    const CCTK_REAL vele_b = lowvelx_b * eab_x + lowvely_b * eab_y + lowvelz_b * eab_z;
    const CCTK_REAL velparallel_bx = vele_b * eab_x;
    const CCTK_REAL velparallel_by = vele_b * eab_y;
    const CCTK_REAL velparallel_bz = vele_b * eab_z;
    const CCTK_REAL lowvelparallel_bx = gxxp_b*velparallel_bx + gxyp_b*velparallel_by 
      + gxzp_b*velparallel_bz;
    const CCTK_REAL lowvelparallel_by = gxyp_b*velparallel_bx + gyyp_b*velparallel_by 
      + gyzp_b*velparallel_bz;
    const CCTK_REAL lowvelparallel_bz = gxzp_b*velparallel_bx + gyzp_b*velparallel_by 
      + gzzp_b*velparallel_bz;
    const CCTK_REAL velparallel2_b = lowvelparallel_bx * velparallel_bx 
      + lowvelparallel_by * velparallel_by + lowvelparallel_bz * velparallel_bz;

    const CCTK_REAL gamma_proj_a = 1/(sqrt(1-vele_a*vele_a)) ;
    const CCTK_REAL gamma_proj_b = 1/(sqrt(1-vele_b*vele_b)) ;
    
    const CCTK_REAL vel2_a = lowvelx_a * velx_a + lowvely_a * vely_a + lowvelz_a * velz_a; 
    const CCTK_REAL vel2_b = lowvelx_b * velx_b + lowvely_b * vely_b + lowvelz_b * velz_b; 

    const CCTK_REAL velorth_xa = velx_a - velparallel_ax; 
    const CCTK_REAL velorth_ya = vely_a - velparallel_ay; 
    const CCTK_REAL velorth_za = velz_a - velparallel_az; 
    const CCTK_REAL lowvelorth_xa = gxxp_a*velorth_xa + gxyp_a*velorth_ya + gxzp_a*velorth_za;
    const CCTK_REAL lowvelorth_ya = gxyp_a*velorth_xa + gyyp_a*velorth_ya + gyzp_a*velorth_za;
    const CCTK_REAL lowvelorth_za = gxzp_a*velorth_xa + gyzp_a*velorth_ya + gzzp_a*velorth_za;
    const CCTK_REAL velorth2_a = lowvelorth_xa * velorth_xa + lowvelorth_ya* velorth_ya + lowvelorth_za*velorth_za;

    const CCTK_REAL velorth_xb = velx_b - velparallel_bx; 
    const CCTK_REAL velorth_yb = vely_b - velparallel_by; 
    const CCTK_REAL velorth_zb = velz_b - velparallel_bz; 
    const CCTK_REAL lowvelorth_xb = gxxp_b*velorth_xb + gxyp_b*velorth_yb + gxzp_b*velorth_zb;
    const CCTK_REAL lowvelorth_yb = gxyp_b*velorth_xb + gyyp_b*velorth_yb + gyzp_b*velorth_zb;
    const CCTK_REAL lowvelorth_zb = gxzp_b*velorth_xb + gyzp_b*velorth_yb + gzzp_b*velorth_zb;
    const CCTK_REAL velorth2_b = lowvelorth_xb * velorth_xb + lowvelorth_yb* velorth_yb + lowvelorth_zb*velorth_zb;

    CCTK_REAL lambdasig_a = (sqrt(velparallel2_a) * (1 - cs_a * cs_a) 
      + cs_a * sqrt ( (1 - vel2_a) * (1 - velparallel2_a) - velorth2_a * cs_a * cs_a))
      /(1 - vel2_a*cs_a*cs_a);
    CCTK_REAL lambdasig_b = (sqrt(velparallel2_b) * (1 - cs_b * cs_b) 
      + cs_b * sqrt ( (1 - vel2_b) * (1 - velparallel2_b) - velorth2_b * cs_b * cs_b))
      /(1 - vel2_b*cs_b*cs_b);
    CCTK_REAL nlambdasig_a = (sqrt(velparallel2_a) * (1 - cs_a * cs_a) 
      - cs_a * sqrt ( (1 - vel2_a) * (1 - velparallel2_a) - velorth2_a * cs_a * cs_a))
      /(1 - vel2_a*cs_a*cs_a);
    CCTK_REAL nlambdasig_b = (sqrt(velparallel2_b) * (1 - cs_b * cs_b) 
      - cs_b * sqrt ( (1 - vel2_b) * (1 - velparallel2_b) - velorth2_b * cs_b * cs_b))
      /(1 - vel2_b*cs_b*cs_b);
   
    CCTK_REAL vsig_a = max (lambdasig_a,lambdasig_a);
    CCTK_REAL vsig_b = max (lambdasig_b,lambdasig_b);
    CCTK_REAL vsig = max(max(vsig_a,vsig_b),0.0);

    CCTK_REAL Sprojx_ab;
    CCTK_REAL Sprojy_ab;
    CCTK_REAL Sprojz_ab;
    CCTK_REAL Eproj_ab;
    CCTK_REAL visc;
    if (mom) {
      Sprojx_ab = gamma_proj_a * (1 + eps_a + press_a / rho_a) * lowvelx_a
      - gamma_proj_b * (1 + eps_b + press_b / rho_b) * lowvelx_b;
      Sprojy_ab = gamma_proj_a * (1 + eps_a + press_a / rho_a) * lowvely_a
      - gamma_proj_b * (1 + eps_b + press_b / rho_b) * lowvely_b;
      Sprojz_ab = gamma_proj_a * (1 + eps_a + press_a / rho_a) * lowvelz_a
      - gamma_proj_b * (1 + eps_b + press_b / rho_b) * lowvelz_b;
   
      visc = -alpha_ab * vsig * invrho_ab * 
      (Sprojx_ab * eab_x + Sprojy_ab * eab_y + Sprojz_ab * eab_z);
    }else {
      Eproj_ab = gamma_proj_a * (1 + eps_a + press_a / rho_a) 
	- press_a / (gamma_proj_a * rho_a)
	- gamma_proj_b * (1 + eps_b + press_b / rho_b) + press_b
	/ (gamma_proj_b * rho_b);

      visc = -alpha_ab * vsig * invrho_ab * Eproj_ab * 
      (MeanWij_x * eab_x + MeanWij_y * eab_y + MeanWij_z * eab_z);

    }
    return visc;
}
  /*
  static inline CCTK_REAL av_relativistic_tau(const CCTK_REAL rho_a,
					      const CCTK_REAL rho_b,
					      const CCTK_REAL press_a,
				    const CCTK_REAL press_b,
				    const CCTK_REAL eps_a,
				    const CCTK_REAL eps_b,
				    const CCTK_REAL cs_a,
				    const CCTK_REAL cs_b,
				    const CCTK_REAL xij,
				    const CCTK_REAL yij,
				    const CCTK_REAL zij,
				    const CCTK_REAL lowxij,
				    const CCTK_REAL lowyij,
				    const CCTK_REAL lowzij,
				    const CCTK_REAL velx_a,
				    const CCTK_REAL vely_a,
				    const CCTK_REAL velz_a,
				    const CCTK_REAL lowvelx_a,
				    const CCTK_REAL lowvely_a,
				    const CCTK_REAL lowvelz_a,
				    const CCTK_REAL velx_b,
				    const CCTK_REAL vely_b,
				    const CCTK_REAL velz_b,
				    const CCTK_REAL lowvelx_b,
				    const CCTK_REAL lowvely_b,
				    const CCTK_REAL lowvelz_b,
				    const CCTK_REAL MeanWij_x,
				    const CCTK_REAL MeanWij_y,
				    const CCTK_REAL MeanWij_z,
				    const CCTK_REAL visca_a,
				    const CCTK_REAL visca_b) {

    
    DECLARE_CCTK_PARAMETERS;

    const CCTK_REAL alpha_ab = 0.5*(visca_a+visca_b);
    const CCTK_REAL invrho_ab = 1/ (0.5*(rho_a+rho_b));
    const CCTK_REAL invsqxab2 = 1 / sqrt(lowxij*xij + lowyij*yij + lowzij*zij);
    const CCTK_REAL eab_x = xij * invsqxab2;
    const CCTK_REAL eab_y = yij * invsqxab2;
    const CCTK_REAL eab_z = zij * invsqxab2;

    const CCTK_REAL vele_a = lowvelx_a * eab_x + lowvely_a * eab_y + lowvelz_a * eab_z;
    const CCTK_REAL gamma_proj_a = 1/(sqrt(1-vele_a*vele_a)) ;
    const CCTK_REAL vele_b = lowvelx_b * eab_x + lowvely_b * eab_y + lowvelz_b * eab_z;
    const CCTK_REAL gamma_proj_b = 1/(sqrt(1-vele_b*vele_b)) ;
    

    CCTK_REAL Eproj_ab = gamma_proj_a * (1 + eps_a + press_a / rho_a) - press_a
      - gamma_proj_b * (1 + eps_b + press_b / rho_b) + press_b;
    
    const CCTK_REAL vel2_a = lowvelx_a * velx_a + lowvely_a * vely_a + lowvelz_a * velz_a; 
    const CCTK_REAL vel2_b = lowvelx_b * velx_b + lowvely_b * vely_b + lowvelz_b * velz_b; 
    const CCTK_REAL velpar_a = vele_a*eab_x;
const CCTK_REAL lowvelpar
    const CCTK_REAL velorth2_a = vel2_a-vele_a*vele_a; 
    const CCTK_REAL velorth2_b = vel2_b-vele_b*vele_b; 
    CCTK_REAL lambdasig_a = sqrt(vele2_a) * (1 - cs_a * cs_a) 
      + cs_a * sqrt ( (1 - vel2_a) * (1 - vele2_a) - velorth2_a * cs_a * cs_a);
    CCTK_REAL lambdasig_b = sqrt(vele2_b) * (1 - cs_b * cs_b) 
      + cs_b * sqrt ( (1 - vel2_b) * (1 - vele2_b) - velorth2_b * cs_b * cs_b);
    CCTK_REAL nlambdasig_a = sqrt(vele2_a) * (1 - cs_a * cs_a) 
      - cs_a * sqrt ( (1 - vel2_a) * (1 - vele2_a) - velorth2_a * cs_a * cs_a);
    CCTK_REAL nlambdasig_b = sqrt(vele2_b) * (1 - cs_b * cs_b) 
      - cs_b * sqrt ( (1 - vel2_b) * (1 - vele2_b) - velorth2_b * cs_b * cs_b);

    CCTK_REAL vsig_a = max (lambdasig_a,-nlambdasig_a);
    CCTK_REAL vsig_b = max (lambdasig_b,-nlambdasig_b);
    CCTK_REAL vsig = max(max(vsig_a,vsig_b),0.0);

    CCTK_REAL tauvisc = alpha_ab * vsig * invrho_ab * Eproj_ab * 
      (MeanWij_x * eab_x + MeanWij_y * eab_y + MeanWij_z * eab_z);

    return tauvisc;
}

  */

















  // old
#if 0
  inline CCTK_REAL artvisc(const CCTK_REAL rho,
			   const CCTK_REAL p, // press
			   const CCTK_REAL e, // eps
			   const CCTK_REAL a, // visca
			   const CCTK_REAL c, // speed of sound 
			   const CCTK_REAL h, // smoothing length
			   const CCTK_REAL h2_ab,
			   const CCTK_REAL r2_ab,
			   const CCTK_REAL rv) {

    DECLARE_CCTK_PARAMETERS;
    CCTK_REAL viscosity = 0.0;
    // calculating enthalpy
    const CCTK_REAL w = 1.0+p/rho+e;
    const CCTK_REAL divv =  rv / (r2_ab + 0.1 * h2_ab);

    // calculating artificial viscosity
    if (divv < 0.0){
      viscosity = rho * w * ( -a * c * h * divv + 2.0 * a * h * h *  divv * divv) ;
    } else {
      viscosity = 0.0;
    }
    //old, broken
    //if(-div_vij > 0.0){
    //  viscosity = rho*w*(-a*c*h*div_vij +
    //			 + 2.0*a*h*h*div_vij*div_vij);
    //} else {
    //  viscosity = 0.0;
    //}

    return viscosity;

  }
#endif

} 
#endif
