#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <omp.h>
#include "kernel.hh"
#include "interact.hh"
#include "dens_update.hh"

namespace GRSPH {

  extern "C" void GRSPH_initdens(CCTK_ARGUMENTS){
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    if(verbose)
      CCTK_VInfo(CCTK_THORNSTRING,"Start Init Density");
    
    vector<vector<int>> thread_js(omp_get_max_threads());

#pragma omp parallel 
    {
      auto &js = thread_js.at(omp_get_thread_num());

#pragma omp for schedule(dynamic, 1000)
    for (ptrdiff_t i = 0; i < *nparticles; ++i) {

      //      fprintf(stderr,"um: %d\n",(int)i);

#if 0
      const auto &restrict neighbours = interactions.particles_neighbours[i];
      const auto &restrict js = *neighbours.js;


      dens[i] = compute_dens((int*)js.data(),neighbours.jindmin,neighbours.jindmax,
			     i,posx,posy,posz,mass,smoothl[i],kerneldim[0],kerneldim[1]);
      dens0[i] = dens[i];
#endif

      // first get new interactions
      js.clear();
      for (ptrdiff_t j = 0; j < *n_g_particles; ++j) {
	if (j != i) {
	  CCTK_REAL dist = sqrt( (posx[i]-posx[j])*(posx[i]-posx[j]) +
				 (posy[i]-posy[j])*(posy[i]-posy[j]) +
				 (posz[i]-posz[j])*(posz[i]-posz[j]) );
	  if (dist <= neib_fac * smoothl[i]) {
	    js.push_back(j);
	  }
	}
      }
      const int jindmin = 0;
      const int jindmax = js.size();
      dens[i] = compute_dens((int*)js.data(),jindmin,jindmax,
			     i,posx,posy,posz,mass,smoothl[i],kerneldim[0],kerneldim[1]);

      dens0[i] = dens[i];

      // first lower velocity
      double vlowx = gxxp[i]*velx[i] + gxyp[i]*vely[i] + gxzp[i]*velz[i];
      double vlowy = gxyp[i]*velx[i] + gyyp[i]*vely[i] + gyzp[i]*velz[i];
      double vlowz = gxzp[i]*velx[i] + gyzp[i]*vely[i] + gzzp[i]*velz[i];
      W[i] = 1.0 / sqrt(1.0 - vlowx*velx[i] - vlowy*vely[i] - vlowz*velz[i]);

      // Calculate first rho
      rho[i] = dens[i] / (W[i] * sdetgp[i]) ;    
    
      // fprintf(stderr,"%6d %15.6E %15.6E %15.6E\n",i,smoothl[i],dens[i],mass[i]);

    } // loop over particles
    } // pragma omp

    if(verbose)
      CCTK_VInfo(CCTK_THORNSTRING,"End Init Density");


  } // GRSPH_initdens

				       
  extern "C" void GRSPH_initsmooth(CCTK_ARGUMENTS){
    // compute the smoothing length based on description in
    // Cossins PhD thesis, arXiv:1007.1245v2, section 3.5
    // 
    // NOTE: this routine is missing the grad h term Omega!
    // This must still be implemented
    //
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    // convergence accuracy
    const CCTK_REAL prec = prec_smooth;
    const CCTK_REAL eta = eta_smooth;
    const CCTK_INT countmax = 100;

    vector<vector<int>> thread_js(omp_get_max_threads());

#pragma omp parallel 
    {
      auto &js = thread_js.at(omp_get_thread_num());

#pragma omp for schedule(dynamic, 1000)
      //    for (ptrdiff_t i = 100; i < 101; ++i){
      for (ptrdiff_t i = 0; i < *nparticles; ++i){

      CCTK_REAL h = smoothl[i];      
      CCTK_REAL tempdens = 0.0;
      CCTK_INT count = 0;
      bool done = false;

      // Newton Raphson iteration to find smoothing length
      while(!done && count < countmax) {

	count++;

#if 0
	const auto &restrict neighbours = interactions.particles_neighbours[i];
        const auto &restrict jjs = *neighbours.js;

        tempdens = compute_dens((int*)jjs.data(),neighbours.jindmin,neighbours.jindmax,
                                i,posx,posy,posz,mass,h,kerneldim[0],kerneldim[1]);
#endif

	// first get new interactions
	js.clear();
	for (ptrdiff_t j = 0; j < *n_g_particles; ++j) {
	  if (j != i) {
	    CCTK_REAL dist = sqrt( (posx[i]-posx[j])*(posx[i]-posx[j]) +
				   (posy[i]-posy[j])*(posy[i]-posy[j]) +
				   (posz[i]-posz[j])*(posz[i]-posz[j]) );
	    if (dist <= neib_fac * h) {
	      js.push_back(j);
	    }
	  }
	}
	const int jindmin = 0;
	const int jindmax = js.size();
	tempdens = compute_dens((int*)js.data(),jindmin,jindmax,
				i,posx,posy,posz,mass,h,kerneldim[0],kerneldim[1]);

	// NOW the NR step
	CCTK_REAL F = mass[i] * pow(eta/h,3.0) - tempdens;
	CCTK_REAL dFdh = - 3.0 * tempdens / h;
	CCTK_REAL dh = - F / dFdh;
	CCTK_REAL hnew = h + dh;
	CCTK_REAL reldiff = abs(hnew-h)/h;
	
	done = ( reldiff < prec );
	h = hnew;
	//	  fprintf(stderr,"%6d %4d %15.6E %15.6E %15.6E %15.6E %15.6E\n",
	//	  i,count,h,tempdens,mass[i],reldiff,F);
      }
      // final recomputation of density
      js.clear();
      for (ptrdiff_t j = 0; j < *n_g_particles; ++j) {
	if (j != i) {
	  CCTK_REAL dist = sqrt( (posx[i]-posx[j])*(posx[i]-posx[j]) +
				 (posy[i]-posy[j])*(posy[i]-posy[j]) +
				 (posz[i]-posz[j])*(posz[i]-posz[j]) );
	  if (dist <= neib_fac * h) {
	    js.push_back(j);
	  }
	}
      }
      const int jindmin = 0;
      const int jindmax = js.size();
      tempdens = compute_dens((int*)js.data(),jindmin,jindmax,
			      i,posx,posy,posz,mass,h,kerneldim[0],kerneldim[1]);

      if (count >= countmax) {
	CCTK_WARN(0,"Did not converge in smoothing length calculation!");
      }

      smoothl[i] = h;
      dens[i] = tempdens;

      // fprintf(stderr,"%6d %15.6E %15.6E %15.6E\n",i,h,tempdens,mass[i]);

      //      if(i == 500) abort();

    } // loop over particles
    } // omp parallel


  } // GRSPH_initsmooth
  

} // namespace GRSPH

