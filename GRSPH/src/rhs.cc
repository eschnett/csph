#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>
#include <cstddef>

#include "interact.hh"
#include "kernel.hh"
#include "artificial.hh"
#include "eos.hh"
#include "calculate_divvel_gradh.hh"
#include "calculate_shockTriggerCoefficient.hh"

namespace GRSPH {
using namespace std;

  // This function is now templated to avoid if statements
  // in innermost loops. It is called from GRSPH_rhs_driver,
  // which is defined at the bottom of this source file.
template<const int avkey>
void GRSPH_rhs(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"Start RHS");

  // allocate arrays
  CCTK_REAL *vcordx,*vcordy,*vcordz,*div_vel,*gradh;
  vcordx = (CCTK_REAL*)malloc(*n_g_particles*sizeof(CCTK_REAL));
  vcordy = (CCTK_REAL*)malloc(*n_g_particles*sizeof(CCTK_REAL));
  vcordz = (CCTK_REAL*)malloc(*n_g_particles*sizeof(CCTK_REAL));
  div_vel = (CCTK_REAL*)malloc(*n_g_particles*sizeof(CCTK_REAL));
  gradh = (CCTK_REAL*)malloc(*n_g_particles*sizeof(CCTK_REAL));

// initialize RHS over maxnparticles and ghost+non-ghost particles
#pragma omp parallel for
  for (ptrdiff_t i = 0; i < maxnparticles; ++i) {
    posxrhs[i] = 0.0;
    posyrhs[i] = 0.0;
    poszrhs[i] = 0.0;

    sxrhs[i] = 0.0;
    syrhs[i] = 0.0;
    szrhs[i] = 0.0;

    taurhs[i] = 0.0;
    viscarhs[i] = 0.0;

    // Calculate pressure; we are doing this here, because it must be set
    // for all particles before the main RHS loop
    // initialize things to zero, calculate pressure, set up
    // coordinate frame velocity
    if (i < *n_g_particles) {
      press[i] = GRSPH_press(rho[i],eps[i]);
      cs2[i] = GRSPH_cs2(rho[i],eps[i]);
      vcordx[i] = alphap[i]*velx[i] - betaxp[i];
      vcordy[i] = alphap[i]*vely[i] - betayp[i];
      vcordz[i] = alphap[i]*velz[i] - betazp[i];
      gradh[i] = 1.0;
      div_vel[i] = 0.0;
    }
  }
  // Calculating helpful values needing
  // particle interaction, therefore loop over neighbours
  // 
  // Calculating gradh correction terms
  // and for artificial viscosity coefficient
  // divergence of velocity terms, div_vel, graddiv_vel  
/* ***************************** Loop over all ********************************* */
#pragma omp parallel   // create threads
  {
#pragma omp for // use threads
  for (ptrdiff_t i = 0; i < *n_g_particles; ++i) {
    /* ******************* Loop over neighbours ********************** */
    const auto &restrict neighbours = interactions.particles_neighbours[i];
    const auto &restrict js = *neighbours.js;
    for (ptrdiff_t jind = neighbours.jindmin; jind < neighbours.jindmax;
         ++jind) {
      const ptrdiff_t j = js[jind];
      const CCTK_REAL xij = posx[i] - posx[j];
      const CCTK_REAL yij = posy[i] - posy[j];
      const CCTK_REAL zij = posz[i] - posz[j];

      // Calculating Grad_Kernel function for smoothl(i)
      CCTK_REAL gradWij_x, gradWij_y, gradWij_z;
      grad_M4kernel(xij, yij, zij, gradWij_x, gradWij_y, gradWij_z,smoothl[i]
		    ,kerneldim[0],kerneldim[1]);
      
      //Calculate gradh
      if (varsmooth)
	gradh[i] -= GRSPH_gradh (mass[i],
				 mass[j],
				 dens[i],
				 smoothl[i],
				 sqrt(xij*xij + yij*yij + zij*zij),
				 kerneldim);
      // Calculate div_v
      div_vel[i] -= GRSPH_divvel(gxxp[j], gxyp[j],
				 gxzp[j], gyyp[j],
				 gyzp[j], gzzp[j],
				 vcordx[i]-vcordx[j],
				 vcordy[i]-vcordy[j],
				 vcordz[i]-vcordz[j],
				 mass[i],
				 dens[i],
				 gradWij_x,
				 gradWij_y,
				 gradWij_z);
    }// loop over neighbors
    if (varsmooth) //gradh selfinteraction
      gradh[i] -= GRSPH_gradh (mass[i],mass[i],dens[i],smoothl[i],0.0,kerneldim);
  }// loop over all    

/* ***************************** Loop over all ********************************* */
#pragma omp for // use threads
  for (ptrdiff_t i = 0; i < *n_g_particles; ++i) {
    if (avkey == 3) {
      const auto &restrict neighbours = interactions.particles_neighbours[i];
      const auto &restrict js = *neighbours.js;
      CCTK_REAL gradDivVel = GRSPH_calculate_gradDivVel ((int*)js.data(),
							 neighbours.jindmin,
							 neighbours.jindmax,
							 i,
							 posx, posy, posz,
							 mass, div_vel,
							 gradh, dens,
							 smoothl[i],
							 kerneldim,
							 gxxp[i], gxyp[i],
							 gxzp[i], gyyp[i],
							 gyzp[i], gzzp[i]);
      CCTK_REAL Coeff_shock = GRSPH_calculate_ShockCoeff(gradDivVel,
							 cs2[i],
							 div_vel[i],
							 smoothl[i]); 
      if (Coeff_shock > visca[i])
	visca[i] = Coeff_shock;
      else 
	viscarhs[i] = /*max(-div_vel[i],0.0)*/-
	  (visca[i]-0.2) * sqrt(cs2[i]) / (2.0*smoothl[i]); 
    }      
    // Calculating coefficient for artificial viscosity RHS
    // Only time-dependent shock trigger, Price 2008
    if (avkey == 2)
      viscarhs[i] = max(-div_vel[i],0.0)-
	(visca[i]-0.2) * sqrt(cs2[i]) / (2.0*smoothl[i]); 
  }// loop over all particles
} // pragma omp parallel 

//------------------RHS Values---------------------------- 
#pragma omp parallel for schedule(dynamic, 1000)
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {

    // d/dt x_i = V_i (coordinate frame velocity, not
    // Eulerian velocity because dt is our coordinate
    // frame time differential.
    posxrhs[i] = alphap[i]*velx[i] - betaxp[i];
    posyrhs[i] = alphap[i]*vely[i] - betayp[i];
    poszrhs[i] = alphap[i]*velz[i] - betazp[i];

    const auto &restrict neighbours = interactions.particles_neighbours[i];
    const auto &restrict js = *neighbours.js;
    for (ptrdiff_t jind = neighbours.jindmin; jind < neighbours.jindmax;
         ++jind) {
      const ptrdiff_t j = js[jind];

      // Calculating helpful values
      const CCTK_REAL sqcs2i = sqrt(cs2[i]);
      const CCTK_REAL sqcs2j = sqrt(cs2[j]);
      CCTK_REAL momvisc = 0.0;
      CCTK_REAL tauvisc = 0.0;
      const CCTK_REAL xij = posx[i] - posx[j];
      const CCTK_REAL yij = posy[i] - posy[j];
      const CCTK_REAL zij = posz[i] - posz[j];
      CCTK_REAL lowxij = gxxp[i]*xij + gxyp[i]*yij + gxzp[i]*zij;
      CCTK_REAL lowyij = gxyp[i]*xij + gyyp[i]*yij + gyzp[i]*zij;
      CCTK_REAL lowzij = gxzp[i]*xij + gyzp[i]*yij + gzzp[i]*zij;
      const CCTK_REAL vr_ij = lowxij * (vcordx[i]-vcordx[j]) 
	+ lowyij * (vcordy[i]-vcordy[j])
	+ lowzij * (vcordz[i]-vcordz[j]);
 
      // Calculating Grad_Kernel function for smoothl(i),(j)
      CCTK_REAL gradWij_x, gradWij_y, gradWij_z;
      CCTK_REAL gradWji_x, gradWji_y, gradWji_z;
      grad_M4kernel(xij, yij, zij, gradWij_x, gradWij_y,
		    gradWij_z,smoothl[i],kerneldim[0],kerneldim[1]);
      grad_M4kernel(xij, yij, zij, gradWji_x, gradWji_y,
		    gradWji_z,smoothl[j],kerneldim[0],kerneldim[1]);
      const CCTK_REAL MeanWij_x = 0.5 * (gradWij_x + gradWji_x);
      const CCTK_REAL MeanWij_y = 0.5 * (gradWij_y + gradWji_y);
      const CCTK_REAL MeanWij_z = 0.5 * (gradWij_z + gradWji_z);


      //-------------Artificial Viscosity-------------------
      // The selection of the AV used works via C++ templates
      // See the "driver" routine at the bottom of this file.
      // avkey = 0 -- no artificial viscosity
      // avkey = 1 -- standard
      // avkey = 2 -- Price08
      // avkey = 3 -- Rosswog 2010
      if (avkey == 0) {
	// none AV
	momvisc = 0.0;
	tauvisc = 0.0;
      } else if (avkey == 1) {
	// note that we are splitting the AV terms into momentum
	// and energy (tau) AV so that we have full freedom to 
	// use different ways of computing the energy AV terms,
	// which is a scalar and may be chosen differently from
	// the standard version of Monaghan and Gingold, 1983 (used
	// by Siegler & Riffert 2000).
	momvisc = av_std_mom(rho[i],rho[j],
			     press[i],press[j],
			     eps[i],eps[j],
			     sqcs2i,sqcs2j,
			     smoothl[i],smoothl[j],
			     xij,yij,zij,
			     vr_ij,
			     alphap[i]*sdetgp[i],
			     alphap[j]*sdetgp[j]);
	tauvisc = av_std_tau(rho[i],rho[j],
			     press[i],press[j],
			     eps[i],eps[j],
			     sqcs2i,sqcs2j,
			     smoothl[i],smoothl[j],
			     xij,yij,zij,
			     vr_ij,
			     alphap[i]*sdetgp[i],
			     alphap[j]*sdetgp[j],
			     vcordx[i],vcordy[i],vcordz[i],
			     vcordx[j],vcordy[j],vcordz[j],
			     MeanWij_x,MeanWij_y,MeanWij_z);
      }
      else if (avkey == 2){
	// Artificial viscosity, Price 08
        momvisc = av_kh_mom(rho[i],rho[j],
			    press[i],press[j],
			    eps[i],eps[j],
			    sqcs2i,sqcs2j,
			    smoothl[i],smoothl[j],
			    xij,yij,zij,
			    lowxij,lowyij,lowzij,
			    vr_ij,
			    alphap[i]*sdetgp[i],
			    alphap[j]*sdetgp[j],
			    visca[i],visca[j]);
	tauvisc = av_kh_tau(rho[i],rho[j],
			    press[i],press[j],
			    eps[i],eps[j],
			    sqcs2i,sqcs2j,
			    smoothl[i],smoothl[j],
			    xij,yij,zij,
			    lowxij,lowyij,lowzij,
			    vr_ij,
			    alphap[i]*sdetgp[i],
			    alphap[j]*sdetgp[j],
			    vcordx[i],vcordy[i],vcordz[i],
			    vcordx[j],vcordy[j],vcordz[j],
			    MeanWij_x,MeanWij_y,MeanWij_z,
			    visca[i],visca[j]);
			    }
      else if (avkey == 3){
	// special relativisitc artificial viscosity
	// Rosswog 2010, 2014 
        momvisc = av_relativistic(rho[i],rho[j],
				  press[i],press[j],
				  eps[i],eps[j],
				  sqcs2i,sqcs2j,
				  xij,yij,zij,
				  velx[i],vely[i],velz[i],
				  velx[j],vely[j],velz[j],
				  gxxp[i],gxyp[i],gxzp[i],
				  gyyp[i],gyzp[i],gzzp[i],
				  gxxp[j],gxyp[j],gxzp[j],
				  gyyp[j],gyzp[j],gzzp[j],
				  visca[i],visca[j],
				  MeanWij_x,MeanWij_y,MeanWij_z,
				  true);
	tauvisc = av_relativistic(rho[i],rho[j],
				  press[i],press[j],
				  eps[i],eps[j],
				  sqcs2i,sqcs2j,
				  xij,yij,zij,
				  velx[i],vely[i],velz[i],
				  velx[j],vely[j],velz[j],
				  gxxp[i],gxyp[i],gxzp[i],
				  gyyp[i],gyzp[i],gzzp[i],
				  gxxp[j],gxyp[j],gxzp[j],
				  gyyp[j],gyzp[j],gzzp[j],
				  visca[i],visca[j],
				  MeanWij_x,MeanWij_y,MeanWij_z,
				  false);
      }

      // Calculating generalized momentum RHS
      // no source term
      const CCTK_REAL invghdens2_i = 1.0/(gradh[i]*dens[i]*dens[i]);
      const CCTK_REAL invghdens2_j = 1.0/(gradh[j]*dens[j]*dens[j]);
      const CCTK_REAL momsource_i = alphap[i] * sdetgp[i] *  press[i] * invghdens2_i; 
      const CCTK_REAL momsource_j = alphap[j] * sdetgp[j] *  press[j] * invghdens2_j;
      
     
      sxrhs[i] -= mass[j] * (momsource_i * gradWij_x 
			     + momsource_j * gradWji_x 
			     + momvisc * MeanWij_x);

      syrhs[i] -= mass[j] * (momsource_i * gradWij_y 
			     + momsource_j * gradWji_y 
			     + momvisc * MeanWij_y);
      
      szrhs[i] -= mass[j] * (momsource_i * gradWij_z 
			     + momsource_j * gradWji_z 
			     + momvisc * MeanWij_z);

      // Calculating generalized energy RHS
      // no source term 
      const CCTK_REAL tausource_i = alphap[i] * sdetgp[i] * press[i] * invghdens2_i;
      const CCTK_REAL tausource_j = alphap[j] * sdetgp[j] * press[j] * invghdens2_j;

                                // x
      taurhs[i] -= mass[j] * (tausource_i * velx[j] * gradWij_x 
			      + tausource_j * velx[i] * gradWji_x
			      + // y
			      tausource_i * vely[j] * gradWij_y 
			      + tausource_j * vely[i] * gradWji_y
			      + // z
			      tausource_i * velz[j] * gradWij_z 
			      + tausource_j * velz[i] * gradWji_z
			      + // viscosity
			      tauvisc); 

    } // loop over friends
      // Gravity source
      sxrhs[i] += NewtAccelx[i];
      syrhs[i] += NewtAccely[i];
      szrhs[i] += NewtAccelz[i];
      //      taus- dens[i] * (velx[i]*NewtAccelx[i]
      //				+ vely[i]*NewtAccely[i]
      //				+ velz[i]*NewtAccelz[i]);


  } // main loop
  free(vcordx);
  free(vcordy);
  free(vcordz);

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"End RHS");

}

extern "C" void GRSPH_rhs_driver(CCTK_ARGUMENTS) {

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // Using C++ templates to switch between types of artificial
  // viscosity.
  // avkey = 0 -- no artificial viscosity
  // avkey = 1 -- standard
  // avkey = 2 -- Price08
  // avkey = 3 -- Rosswog 2010
  if(CCTK_Equals(artificial_viscosity,"none")) {
    GRSPH_rhs<0>(CCTK_PASS_CTOC);
  } else if(CCTK_Equals(artificial_viscosity,"standard")) {
    GRSPH_rhs<1>(CCTK_PASS_CTOC);
  } else if(CCTK_Equals(artificial_viscosity,"Price08")) {
    GRSPH_rhs<2>(CCTK_PASS_CTOC);
  } else if(CCTK_Equals(artificial_viscosity,"Rosswog")) {
    GRSPH_rhs<3>(CCTK_PASS_CTOC);
  } else {
    CCTK_WARN(0,"Artificial viscosity choice unknown!");
  }
    


}


} // end namespace GRSPH
