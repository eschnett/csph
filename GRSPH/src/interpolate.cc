#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cmath>
#include <cstddef>

#include "kernel.hh"

namespace GRSPH {
using namespace std;

extern "C" void GRSPH_interpolate(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // Grid size
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];

  // Global domain
  const CCTK_REAL x0 = CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL y0 = CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL z0 = CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  // Local domain
  const CCTK_REAL xmin = x0 + cctk_lbnd[0] * dx;
  const CCTK_REAL ymin = y0 + cctk_lbnd[1] * dy;
  const CCTK_REAL zmin = z0 + cctk_lbnd[2] * dz;

  // Maximum neighbour distance
  const ptrdiff_t hi = lrint(ceil(hsml / dx));
  const ptrdiff_t hj = lrint(ceil(hsml / dy));
  const ptrdiff_t hk = lrint(ceil(hsml / dz));

// Initialize grid quantities
#pragma omp parallel for simd collapse(3)
  for (ptrdiff_t gk = 0; gk < nk; ++gk) {
    for (ptrdiff_t gj = 0; gj < nj; ++gj) {
      for (ptrdiff_t gi = 0; gi < ni; ++gi) {
        ptrdiff_t gidx = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
        grid_rho[gidx] = 0.0;
        grid_velx[gidx] = 0.0;
        grid_vely[gidx] = 0.0;
        grid_velz[gidx] = 0.0;
        grid_eps[gidx] = 0.0;
      }
    }
  }

// Interpolate from particles to grid
#pragma omp parallel for
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    ptrdiff_t gi = lrint((posx[i] - xmin) / dx);
    ptrdiff_t gj = lrint((posy[i] - ymin) / dy);
    ptrdiff_t gk = lrint((posz[i] - zmin) / dz);
    if (gi < 0 || gi >= ni || gj < 0 || gj >= nj || gk < 0 || gk >= nk)
      CCTK_ERROR("Particle outside the grid; domain decomposition failed");

    ptrdiff_t simin = max(ptrdiff_t(0), gi - hi);
    ptrdiff_t sjmin = max(ptrdiff_t(0), gj - hj);
    ptrdiff_t skmin = max(ptrdiff_t(0), gk - hk);
    ptrdiff_t simax = min(ni, gi + hi + 1);
    ptrdiff_t sjmax = min(nj, gj + hj + 1);
    ptrdiff_t skmax = min(nk, gk + hk + 1);
    for (ptrdiff_t sk = skmin; sk < skmax; ++sk) {
      for (ptrdiff_t sj = sjmin; sj < sjmax; ++sj) {
        for (ptrdiff_t si = simin; si < simax; ++si) {
          ptrdiff_t sidx = CCTK_GFINDEX3D(cctkGH, si, sj, sk);
          CCTK_REAL sx = xmin + si * dx;
          CCTK_REAL sy = ymin + sj * dy;
          CCTK_REAL sz = zmin + sk * dz;

          CCTK_REAL xij = posx[i] - sx;
          CCTK_REAL yij = posy[i] - sy;
          CCTK_REAL zij = posz[i] - sz;
          CCTK_REAL Wij = kernel(xij, yij, zij, smoothl[i]);

#pragma omp atomic update
          grid_rho[sidx] += mass[i] * Wij;

#pragma omp atomic update
          grid_velx[sidx] += mass[i] / rho[i] * velx[i] * Wij;
#pragma omp atomic update
          grid_vely[sidx] += mass[i] / rho[i] * vely[i] * Wij;
#pragma omp atomic update
          grid_velz[sidx] += mass[i] / rho[i] * velz[i] * Wij;

#pragma omp atomic update
          grid_eps[sidx] += mass[i] * tau[i] * Wij;
        }
      }
    }
  }
}
}
