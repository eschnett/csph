#ifndef P2C_C2P_HH
#define P2C_C2P_HH

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

namespace GRSPH {

static inline __attribute__((always_inline))
void upper_metric(const double gxx, const double gxy,
		 const double gxz, const double gyy,
		 const double gyz, const double gzz,
		 const double sdetg, 
		 double* uxx, double* uxy, double* uxz,
		 double* uyy, double* uyz, double* uzz){

  double invdet = 1.0 / (sdetg*sdetg);

  *uxx = (-gyz*gyz + gyy*gzz)*invdet;
  *uxy = (gxz*gyz - gxy*gzz) *invdet;
  *uxz = (-gxz*gyy + gxy*gyz)*invdet;
  *uyy = (-gxz*gxz + gxx*gzz)*invdet;
  *uyz = (gxy*gxz - gxx*gyz) *invdet;
  *uzz = (-gxy*gxy + gxx*gyy)*invdet;

}

static inline __attribute__((always_inline))
CCTK_REAL calc_sdetg(const CCTK_REAL gxx,
		     const CCTK_REAL gxy,
		     const CCTK_REAL gxz,
		     const CCTK_REAL gyy,
		     const CCTK_REAL gyz,
		     const CCTK_REAL gzz) {

  return sqrt(-gxz*gxz*gyy
	      + 2.0*gxy*gxz*gyz
	      - gxx*gyz*gyz
	      - gxy*gxy*gzz
	      + gxx*gyy*gzz);

}


}
#endif // #ifndef P2C_C2P_HH
