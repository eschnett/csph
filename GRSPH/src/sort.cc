#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <vector>

namespace GRSPH {
using namespace std;

namespace {
template <typename T>
void reorder(T *restrict data, T *restrict tmpdata, ptrdiff_t ndata,
             const vector<int> &perm) {
  for (ptrdiff_t i = 0; i < ndata; ++i)
    tmpdata[i] = data[perm[i]];
  for (ptrdiff_t i = 0; i < ndata; ++i)
    data[i] = tmpdata[i];
}
}

extern "C" void GRSPH_sort(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"Start Sort");

  const auto &MoL_Intermediate_Step = *static_cast<const CCTK_INT *>(
	     CCTK_VarDataPtr(cctkGH, 0, "MoL::MoL_Intermediate_Step"));
  if (cctk_iteration != 0 && MoL_Intermediate_Step != 0)
    return;

  // Grid size
  const ptrdiff_t ni = cctk_lsh[0];
  const ptrdiff_t nj = cctk_lsh[1];
  const ptrdiff_t nk = cctk_lsh[2];

  // Global domain
  const CCTK_REAL x0 = CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL y0 = CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL z0 = CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  // Local domain
  const CCTK_REAL xmin = x0 + cctk_lbnd[0] * dx;
  const CCTK_REAL ymin = y0 + cctk_lbnd[1] * dy;
  const CCTK_REAL zmin = z0 + cctk_lbnd[2] * dz;

  // Calculate grid cell index
  vector<int> grididx(*nparticles);
  //#pragma omp parallel for
  for (ptrdiff_t i = 0; i < *nparticles; ++i) {
    ptrdiff_t gi = lrint((posx[i] - xmin) / dx);
    ptrdiff_t gj = lrint((posy[i] - ymin) / dy);
    ptrdiff_t gk = lrint((posz[i] - zmin) / dz);
    if (gi < 0 || gi >= ni || gj < 0 || gj >= nj || gk < 0 || gk >= nk)
      CCTK_ERROR("Particle outside the grid; domain decomposition failed");
    grididx[i] = CCTK_GFINDEX3D(cctkGH, gi, gj, gk);
  }

  vector<int> perm(*nparticles);
  for (ptrdiff_t i = 0; i < *nparticles; ++i)
    perm[i] = i;
  sort(perm.begin(), perm.end(), [&](ptrdiff_t permi, ptrdiff_t permj) {
    if (grididx[permi] == grididx[permj])
      return id[permi] < id[permj];
    return grididx[permi] < grididx[permj];
  });

  vector<CCTK_REAL> tmpdata1(*nparticles);
  CCTK_REAL *tmpdata = tmpdata1.data();


  reorder(id, tmpdata, *nparticles, perm);
  reorder(mass, tmpdata, *nparticles, perm);
  reorder(eps, tmpdata, *nparticles, perm);
  reorder(cs2, tmpdata, *nparticles, perm);
  reorder(smoothl, tmpdata, *nparticles, perm);
  reorder(press, tmpdata, *nparticles, perm);
  reorder(rho, tmpdata, *nparticles, perm);

  // Gravitational
  reorder(alphap, tmpdata, *nparticles, perm);
  reorder(sdetgp, tmpdata, *nparticles, perm);
  reorder(W, tmpdata, *nparticles, perm);
  reorder(betaxp, tmpdata, *nparticles, perm);
  reorder(betayp, tmpdata, *nparticles, perm);
  reorder(betazp, tmpdata, *nparticles, perm);
  reorder(gxxp, tmpdata, *nparticles, perm);
  reorder(gxyp, tmpdata, *nparticles, perm);
  reorder(gxzp, tmpdata, *nparticles, perm);
  reorder(gyyp, tmpdata, *nparticles, perm);
  reorder(gyzp, tmpdata, *nparticles, perm);
  reorder(gzzp, tmpdata, *nparticles, perm);
  reorder(NewtAccelx, tmpdata, *nparticles, perm);
  reorder(NewtAccely, tmpdata, *nparticles, perm);
  reorder(NewtAccelz, tmpdata, *nparticles, perm);
 
  // RHS + values
  reorder(posx, tmpdata, *nparticles, perm);
  reorder(posy, tmpdata, *nparticles, perm);
  reorder(posz, tmpdata, *nparticles, perm);
  reorder(velx, tmpdata, *nparticles, perm);
  reorder(vely, tmpdata, *nparticles, perm);
  reorder(velz, tmpdata, *nparticles, perm);
  reorder(dens, tmpdata, *nparticles, perm);
  reorder(tau, tmpdata, *nparticles, perm);
  reorder(visca, tmpdata, *nparticles, perm);
  reorder(sx, tmpdata, *nparticles, perm);
  reorder(sy, tmpdata, *nparticles, perm);
  reorder(sz, tmpdata, *nparticles, perm);

  /*
  reorder(posxrhs, tmpdata, *nparticles, perm);
  reorder(posyrhs, tmpdata, *nparticles, perm);
  reorder(poszrhs, tmpdata, *nparticles, perm);
  reorder(velxrhs, tmpdata, *nparticles, perm);
  reorder(velyrhs, tmpdata, *nparticles, perm);
  reorder(velzrhs, tmpdata, *nparticles, perm);
  reorder(sxrhs, tmpdata, *nparticles, perm);
  reorder(syrhs, tmpdata, *nparticles, perm);
  reorder(szrhs, tmpdata, *nparticles, perm);
  reorder(taurhs, tmpdata, *nparticles, perm);
  reorder(viscarhs, tmpdata, *nparticles, perm);
  */

  if(verbose)
    CCTK_VInfo(CCTK_THORNSTRING,"End Sort");

}
}
