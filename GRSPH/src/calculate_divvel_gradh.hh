#ifndef CALCULATE_DIVVEL_GRADH_HH
#define CALCULATE_DIVVEL_GRADH_HH
                                                                                          
#include <cctk.h>
#include <cctk_Parameters.h>
#include <cmath>

namespace GRSPH {

  static inline CCTK_REAL GRSPH_divvel (const CCTK_REAL gxxp_j,
					const CCTK_REAL gxyp_j,
					const CCTK_REAL gxzp_j,
					const CCTK_REAL gyyp_j,
					const CCTK_REAL gyzp_j,
					const CCTK_REAL gzzp_j,
					const CCTK_REAL dVcordX_ij,
					const CCTK_REAL dVcordY_ij,
					const CCTK_REAL dVcordZ_ij,
					const CCTK_REAL mass,
					const CCTK_REAL dens,
					const CCTK_REAL gradWij_x,
					const CCTK_REAL gradWij_y,
					const CCTK_REAL gradWij_z) {
    // Calculate div_v                                                                     
    CCTK_REAL lowDVX = gxxp_j*(dVcordX_ij)
      + gxyp_j*(dVcordY_ij)
      + gxzp_j*(dVcordZ_ij);
    CCTK_REAL lowDVY = gxyp_j*(dVcordX_ij)
      + gyyp_j*(dVcordY_ij)
      + gyzp_j*(dVcordZ_ij);
    CCTK_REAL lowDVZ = gxzp_j*(dVcordX_ij)
      + gyzp_j*(dVcordY_ij)
      + gzzp_j*(dVcordZ_ij);
    return mass / dens * (lowDVX * gradWij_x + lowDVY * gradWij_y
			  + lowDVZ * gradWij_z);
  }

  static inline CCTK_REAL GRSPH_gradh (const CCTK_REAL mass_i,
				       const CCTK_REAL mass_j,
				       const CCTK_REAL dens_i,
				       const CCTK_REAL smoothl_i,
				       CCTK_REAL q,
				       CCTK_REAL* kerneldim) {
    DECLARE_CCTK_PARAMETERS;
    CCTK_REAL dh_drho = -eta_smooth * pow(mass_i/dens_i,kerneldim[0]) / dens_i;
    CCTK_REAL gradKoeff = kerneldim[1] / pow(smoothl_i,kerneldim[0] + 1.0);

    CCTK_REAL gradH = 0;
    q = q / smoothl_i;
    
    if (q < 1.0){
      gradH = dh_drho * mass_j * gradKoeff *
	( -kerneldim[0] + 1.5*q*q*(kerneldim[0]+2.0)
	  - 0.75*q*q*q*(kerneldim[0]+3.0) );
    }
    else if (q < 2.0){
      gradH = dh_drho * mass_j * gradKoeff *
	( -2.0*kerneldim[0] + 3.0*q*(kerneldim[0]+1.0)
	  - 1.5*q*q*(kerneldim[0]+2.0) + 0.25*q*q*q*(kerneldim[0]+3.0) );
    }
    return gradH;
  }
}

#endif
