# Schedule definitions for thorn GRSPH

STORAGE: nparticles,n_g_particles
STORAGE: id mass smoothl kerneldim boundaryval
STORAGE: pos[2] dens dens0
STORAGE: scon[2] tau[2] visca[2]
STORAGE: press W rho eps cs2 vel
STORAGE: posrhs velrhs 
STORAGE: sconrhs taurhs viscarhs 
STORAGE: gravity visc NewtAccel

STORAGE: grid_rho grid_vel grid_eps
STORAGE: grid_mass grid_mom grid_eint
STORAGE: grid_sdetg
STORAGE: grid_Newtonian

# uncomment for con2prim testing
###SCHEDULE GRSPH_con2prim_test AT CCTK_BASEGRID
###{
###  LANG: C
###} "Test Con2Prim"

SCHEDULE GRSPH_MoL IN MoL_Register
{
  LANG: C
} "Register evolved groups"

SCHEDULE GRSPH_init_to_zero AT CCTK_BASEGRID
{
  LANG: C
} "Initialize all particles variables to 0"

#########################################################################
# GROUP GRSPH_Initial

SCHEDULE GROUP GRSPH_Initial at CCTK_INITIAL
{
} "Do special things for setting up the simulation"

SCHEDULE GRSPH_init IN GRSPH_Initial
{
  LANG: C
} "Initialize particles"

SCHEDULE GRSPH_CalcSDetG IN GRSPH_Initial AFTER GRSPH_init
{
  LANG: C
} "compute sdetg on the 3D grid"

if(do_gravity_source) {
SCHEDULE GRSPH_sources_grid IN GRSPH_Initial AFTER GRSPH_CalcSDetG
{
  LANG: C
} "Initialize grid gravity acceleration"
}

SCHEDULE GRSPH_interpolate_to_particles IN GRSPH_Initial AFTER \
	 (GRSPH_sources_grid, GRSPH_sources_grid)
{
  LANG: C
} "interpolate gravity grid variables to particles"

SCHEDULE GRSPH_sort IN GRSPH_Initial AFTER GRSPH_interpolate_to_particles
{
  LANG: C
} "Sort particles at INITIAL"

SCHEDULE GRSPH_setghostparticles IN GRSPH_Initial AFTER GRSPH_sort
{
  LANG: C
} "Set ghost particles"

SCHEDULE GRSPH_interact IN GRSPH_Initial AFTER GRSPH_setghostparticles
{
  LANG: C
} "Find interactions"

SCHEDULE GRSPH_initdens IN GRSPH_Initial AFTER GRSPH_interact
{
  LANG: C
} "Initialize particle conserved density,rho"

SCHEDULE GRSPH_prim2con IN GRSPH_Initial AFTER GRSPH_initdens
{
  LANG: C
} "Initial Prim->Con"

####################################################
####### MoL Step

SCHEDULE GRSPH_setghostparticles IN MoL_Step BEFORE (GRSPH_CalcSDetG,GRSPH_interact)
{
  LANG: C
} "Set ghost particles after density calculation (initial/update)"

SCHEDULE GRSPH_interact IN MoL_Step AFTER GRSPH_setghostparticles BEFORE GRSPH_CalcSDetG
{
  LANG: C
} "Find interactions, after MoL for density update"


SCHEDULE GRSPH_CalcSDetG IN MoL_Step
{
  LANG: C
} "compute sdetg on the 3D grid"

if(do_gravity_source) {
SCHEDULE GRSPH_sources_grid IN MoL_Step AFTER GRSPH_CalcSDetG 
{
  LANG: C
} "compute the gravity source terms on the grid"
}

SCHEDULE GRSPH_interpolate_to_particles IN MoL_Step AFTER \
	 (GRSPH_sources_grid, GRSPH_sources_grid)
{
  LANG: C
} "interpolate gravity grid variables to particles"

SCHEDULE GRSPH_rhs_driver IN MoL_Step AFTER GRSPH_interpolate_to_particles
{
  LANG: C
} "Calculate RHS"

########################### PostStep stuff

SCHEDULE GRSPH_sort IN MoL_PostStepModify 
{
  LANG: C
} "Sort particles after MoL"


SCHEDULE GRSPH_setghostparticles IN MoL_PostStep AFTER GRSPH_sort
{
  LANG: C
} "Set ghost particles for new interaction after MoL"

SCHEDULE GRSPH_interact IN MoL_PostStep AFTER GRSPH_setghostparticles
{
  LANG: C
} "Find interactions, after MoL for density update"

if(varsmooth) {
SCHEDULE GRSPH_dens_update_var_smoothl IN MoL_PostStep AFTER GRSPH_interact BEFORE GRSPH_con2prim
{
  LANG: C
} "Update density"
}
else{
SCHEDULE GRSPH_dens_update_fixed_smoothl IN MoL_PostStep AFTER GRSPH_interact BEFORE GRSPH_con2prim
{
  LANG: C
} "Update density"
}

SCHEDULE GRSPH_con2prim IN MoL_PostStep 
{
  LANG: C
} "Convert back to primitive variables: Con->Prim"

############################### analysis stuff

SCHEDULE GRSPH_timestep_diagnostic AT analysis
{
  LANG: C
} "Calculate timestep diagnostics"


SCHEDULE GRSPH_conserved AT analysis
{
  LANG: C
} "Calculate conserved quantities"

SCHEDULE GRSPH_interpolate AT analysis
{
  LANG: C
} "Interpolate quantities to grid"

SCHEDULE GRSPH_distribute AT analysis
{
  LANG: C
} "Distribute conserved quantities onto grid"
